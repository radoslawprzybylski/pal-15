﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.7.2.98?>
<SwConfiguration CpuAddress="SL1" xmlns="http://br-automation.co.at/AS/SwConfiguration">
  <TaskClass Name="Cyclic#1">
    <Task Name="MainMaster" Source="Sources.Master.MainMaster.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Main_Trans" Source="Sources.01_Transport.Main_Transport.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Main_Balko" Source="Sources.03_Balkon.Main_Balkon.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Main_Magaz" Source="Sources.04_Magazyn_Palet.Main_Magazyn.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Main_Owija" Source="Sources.05_Owijarka.Main_Owijarka.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="inv_p66_10" Source="Sources.01_Transport.inv_transport.inv_p66_10.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="inv_p66_11" Source="Sources.01_Transport.inv_transport.inv_p66_11.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="inv_p66_01" Source="Sources.01_Transport.inv_transport.inv_p66_01.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="inv_p66_02" Source="Sources.01_Transport.inv_transport.inv_p66_02.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="inv_p66_03" Source="Sources.01_Transport.inv_transport.inv_p66_03_RolkaKrotka.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="inv_p66_1" Source="Sources.01_Transport.inv_transport.inv_p66_03_RolkaDluga.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="inv_p66_04" Source="Sources.03_Balkon.inv_balkon.inv_p66_04_Balkon.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="inv_p66_05" Source="Sources.03_Balkon.inv_balkon.inv_p66_05_Slupy.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="inv_p66_08" Source="Sources.05_Owijarka.inv_owijarka.inv_p66_08_RolkowyDuzy.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="inv_p66_09" Source="Sources.05_Owijarka.inv_owijarka.inv_p66_09_RolkowyOwijarka.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="CM_Transpo" Source="Sources.01_Transport.CM_Transport.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="CM_Balkon" Source="Sources.03_Balkon.CM_Balkon.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="CM_Magazyn" Source="Sources.04_Magazyn_Palet.CM_Magazyn.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="MainRecept" Source="Sources.Receptury.MainReceptury.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Retains" Source="Sources.Retains.Retains.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Vizu" Source="Wizualizacja.Vizu.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#2" />
  <TaskClass Name="Cyclic#3" />
  <TaskClass Name="Cyclic#4" />
  <TaskClass Name="Cyclic#5" />
  <TaskClass Name="Cyclic#6" />
  <TaskClass Name="Cyclic#7" />
  <TaskClass Name="Cyclic#8" />
  <DataObjects>
    <DataObject Name="assl1" Source="" Memory="UserROM" Language="Binary" />
  </DataObjects>
  <VcDataObjects>
    <VcDataObject Name="Visu" Source="Wizualizacja.Visu.dob" Memory="UserROM" Language="Vc" WarningLevel="2" />
  </VcDataObjects>
  <Binaries>
    <BinaryObject Name="vcfile" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arialbd" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcdsloc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcbclass" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcfntttf" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu01" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu03" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcshared" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcrt" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="TCData" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="vcmgr" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vctcal" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcalarm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdsw" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="FWRules" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcdsint" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arial" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccovl" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="udbdef" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcgclass" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu02" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcctext" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbtn" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccnum" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbmp" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccline" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbar" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdi855" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpk" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcptelo" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdihd" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdi815" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpfppc7" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccdt" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcclbox" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccalarm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcchspot" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcnet" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdvnc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpfar00" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccstr" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccpwd" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Role" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arconfig" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="User" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="asfw" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="ashwd" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="ashwac" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="sysconf" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="iomap" Source="" Memory="UserROM" Language="Binary" />
  </Binaries>
  <Libraries>
    <LibraryObject Name="AsSafety" Source="Libraries.AsSafety.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="FileIO" Source="Libraries.FileIO.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="DataObj" Source="Libraries.DataObj.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsXml" Source="Libraries.AsXml.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="standard" Source="Libraries.standard.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="runtime" Source="Libraries.runtime.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="astime" Source="Libraries.astime.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="ArEco" Source="Libraries.ArEco.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="brsystem" Source="Libraries.brsystem.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsEPL" Source="Libraries.AsEPL.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="IecCheck" Source="Libraries.IecCheck.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="sys_lib" Source="Libraries.sys_lib.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsBrStr" Source="Libraries.AsBrStr.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="asstring" Source="Libraries.asstring.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="CM_Lib" Source="Libraries.CM_Lib.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="asieccon" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="vcresman" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
  </Libraries>
</SwConfiguration>