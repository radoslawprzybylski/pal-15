
PROGRAM _INIT
	//dwa rzeczywiste czujniki
	gDATA.Transport.Z1_Silownik_Spych1.PAR.HasCloseFB := TRUE;
	gDATA.Transport.Z1_Silownik_Spych1.PAR.HasOpenFB := TRUE;
//	gDATA.Transport.Z1_Silownik_Spych1.PAR.TimerStsClose_Preset := T#3s;
//	gDATA.Transport.Z1_Silownik_Spych1.PAR.TimerStsOpen_Preset := T#3s;
	gDATA.Transport.Z1_Silownik_Spych1.PAR.TimerAlm_Preset := T#3s;
	 
	//dwa rzeczywiste czujniki
	gDATA.Transport.Z2_Silownik_Spych2.PAR.HasCloseFB := TRUE;
	gDATA.Transport.Z2_Silownik_Spych2.PAR.HasOpenFB := TRUE;
	//gDATA.Transport.Z2_Silownik_Spych2.PAR.TimerStsClose_Preset := T#3s;
//	gDATA.Transport.Z2_Silownik_Spych2.PAR.TimerStsOpen_Preset := T#3s;
	gDATA.Transport.Z2_Silownik_Spych2.PAR.TimerAlm_Preset := T#3s;
	
	//dwa rzeczywiste czujniki
	gDATA.Transport.Z3_BlokadaZgrzewki.PAR.HasCloseFB := TRUE;
	gDATA.Transport.Z3_BlokadaZgrzewki.PAR.HasOpenFB := TRUE;
//	gDATA.Transport.Z3_BlokadaZgrzewki.PAR.TimerStsClose_Preset := T#3s;
//	gDATA.Transport.Z3_BlokadaZgrzewki.PAR.TimerStsOpen_Preset := T#3s;
	gDATA.Transport.Z3_BlokadaZgrzewki.PAR.TimerAlm_Preset := T#2s;
	
	//dwa rzeczywiste czujniki
	gDATA.Transport.Z10_Obrotnica.PAR.HasCloseFB := TRUE;
	gDATA.Transport.Z10_Obrotnica.PAR.HasOpenFB := TRUE;
//	gDATA.Transport.Z10_Obrotnica.PAR.TimerStsClose_Preset := T#3s;
//	gDATA.Transport.Z10_Obrotnica.PAR.TimerStsOpen_Preset := T#3s;
	gDATA.Transport.Z10_Obrotnica.PAR.TimerAlm_Preset := T#2s;
END_PROGRAM

PROGRAM _CYCLIC
	gCM.Transport.Z1_Silownik_Spych1(DATA := gDATA.Transport.Z1_Silownik_Spych1);
	gCM.Transport.Z2_Silownik_Spych2(DATA := gDATA.Transport.Z2_Silownik_Spych2);
	gCM.Transport.Z3_BlokadaZgrzewki(DATA := gDATA.Transport.Z3_BlokadaZgrzewki);
	gCM.Transport.Z10_Obrotnica(DATA := gDATA.Transport.Z10_Obrotnica);
	
	LicznikWarstw_Visu := Licznik_Warstw + 1;
	
	TON_Timer_Obrotnica01();
	TON_Timer_Obrotnica02();
	TON_Timer_Spych01();
	TON_Timer_Spych02();
	TON_Timer_Spych03();
	TON_Timer_Spych04();
	TON_Timer_Spych05();
	TON_Timer_Spych06();
	TON_CzasDojazduZgrzewki5();
	TON_Timer_Obrotnica_BrakObrotu();
	TON_Timer_Blokada();
	TON_Timer_BrakBlokady();
	TON_Timer_Opoznienie_Spychu01();
	TON_Timer_Opoznienie_Spychu02();
	TON_Timer_Opoznienie_Spychu03();
	TON_Timer_Opoznienie_Spychu04();
	TON_Timer_Opoznienie_Spychu05();
	TON_Timer_Opoznienie_Spychu06();
	TP_PrzeladowanieZgrzewkami();
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

