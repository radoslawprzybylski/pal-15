(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: plnkP84ctrl
 * File: plnkP84ctrl.typ
 * Author: popekw
 * Created: January 21, 2010
 ********************************************************************
 * Local data types of program plnkP84ctrl
 ********************************************************************)

TYPE
	IO_P66_typ : 	STRUCT 
		moduleOK : BOOL; (*ModuleOK if TRUE module present*)
		statusWord : UINT; (*Status Word*)
		commandWord : UINT; (*Command Word*)
		setRPM : INT; (*set speed RPM*)
		outRPM : INT; (*output speed RPM*)
		errorCode : UINT; (*Last Fault detected*)
	END_STRUCT;
END_TYPE
