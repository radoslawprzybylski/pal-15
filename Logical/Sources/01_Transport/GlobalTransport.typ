
TYPE
	Transport_typ : 	STRUCT 
		Cmd : Transport_Cmd_typ;
		Par : Transport_Par_typ;
		Sts : Transport_Sts_typ;
	END_STRUCT;
	Transport_Cmd_typ : 	STRUCT 
		Start : BOOL;
		Stop : BOOL;
		Wstrzymaj : BOOL;
		Reset : BOOL;
		ErrorReset : BOOL;
		KoniecPalety : BOOL;
	END_STRUCT;
	Transport_Par_typ : 	STRUCT 
		Predkosci : PredkosciTransportery_typ;
		Czasy : Czasy_typ;
	END_STRUCT;
	Transport_Sts_typ : 	STRUCT 
		Step : TransportStep_enum;
		Gotowy : BOOL;
		Error : BOOL;
		Zresetowano : BOOL;
		PoZatrzymaniu : BOOL;
		WarstwaGotowa : BOOL;
		WarstwaNaPlatformie : BOOL;
	END_STRUCT;
	TransportStep_enum : 
		(
		TRANSPORT_CZEKANIE := 0,
		TRANSPORT_URUCHAMIANIE := 1,
		TRANSPORT_PRACA := 2,
		TRANSPORT_WSTRZYMYWANIE,
		TRANSPORT_WSTRZYMANY,
		TRANSPORT_ZATRZYMYWANIE,
		TRANSPORT_ZATRZYMANY,
		TRANSPORT_RESETOWANIE,
		TRANSPORT_ERROR,
		TRANSPORT_PO_ZATRZYMANIU
		);
	Czasy_typ : 	STRUCT 
		Czas_Opoznienie01 : TIME;
		Czas_Opoznienie02 : TIME;
		Czas_Opoznienie03 : TIME;
		Czas_Opoznienie04 : TIME;
		Czas_Opoznienie05 : TIME;
		Czas_Opoznienie06 : TIME;
		Czas_Opoznienie07 : TIME;
		Czas_BrakObrotu : TIME;
		Czas_Obrot : TIME;
		Czas_Spych01 : TIME;
		Czas_Spych02 : TIME;
		Czas_Spych03 : TIME;
		Czas_Spych04 : TIME;
		Czas_Spych05 : TIME;
		Czas_Spych06 : TIME;
		Czas_Blokada : TIME;
		Czas_BrakBlokady : TIME;
		Czas_PrzeladowanieZgrzewkami : TIME;
	END_STRUCT;
	PredkosciTransportery_typ : 	STRUCT 
		MT1_10_RPM_Praca : INT;
		MT2_11_RPM_Praca : INT;
		MT3_01_RPM_Praca : INT;
		MT4_02_RPM_Praca : INT;
		MT5_02_RPM_Praca : INT;
		MT6_03_RPM_Praca : INT;
		MT1_10_RPM_Zwalniania : INT;
		MT2_11_RPM_Zwalniania : INT;
		MT3_01_RPM_Zwalniania : INT;
		MT4_02_RPM_Zwalniania : INT;
		MT5_02_RPM_Zwalniania : INT;
		MT6_03_RPM_Zwalniania : INT;
	END_STRUCT;
END_TYPE
