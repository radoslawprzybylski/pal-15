
TYPE
	Master_typ : 	STRUCT 
		Cmd : Master_Cmd_typ;
		Par : Master_Par_typ;
		Sts : Master_Sts_typ;
	END_STRUCT;
	Master_Cmd_typ : 	STRUCT 
		Start : BOOL;
		Stop : BOOL;
		Wstrzymaj : BOOL;
		Reset : BOOL;
		ErrorReset : BOOL;
		KoniecPalety : BOOL;
	END_STRUCT;
	Master_Par_typ : 	STRUCT 
		PracaAlarmTimeout : TIME;
	END_STRUCT;
	Master_Sts_typ : 	STRUCT 
		Step : MasterStep_enum;
		Gotowy : BOOL;
		Error : BOOL;
		Zresetowano : BOOL;
	END_STRUCT;
	MasterStep_enum : 
		(
		MASTER_CZEKANIE,
		MASTER_URUCHAMIANIE,
		MASTER_PRACA,
		MASTER_ZATRZYMYWANIE,
		MASTER_ZATRZYMANY,
		MASTER_RESETOWANIE,
		MASTER_WSTRZYMANY,
		MASTER_ERROR
		);
END_TYPE
