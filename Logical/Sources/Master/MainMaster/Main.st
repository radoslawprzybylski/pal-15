
PROGRAM _INIT
	(* Insert code here *)
	gMaster.Par.PracaAlarmTimeout := T#60s;
	TON_TimeoutPraca.PT := gMaster.Par.PracaAlarmTimeout;
	
	TON_StartPage.PT := T#15s;
	TON_StartPage.IN := TRUE;
	
	TON_ResetSafety.PT := T#35s;
	TON_ResetSafety.IN := TRUE;
	
	// Ustawienie statusu do kontynuacji pracy po odlaczeniu zasilania
	gTransport.Sts.PoZatrzymaniu := TRUE;
	gBalkon.Sts.PoZatrzymaniu := TRUE;
	gMagazyn.Sts.PoZatrzymaniu := TRUE;
	gOwijarka.Sts.PoZatrzymaniu := TRUE;
	
END_PROGRAM

PROGRAM _CYCLIC
	
	(*Wyswietlenie strony startowej*)
	IF TON_StartPage.Q THEN
		IF Wizu_CurrentPage = 0 THEN
			Wizu_ChangePage := 1;
		END_IF
	END_IF
	TON_StartPage();
	VisProgressBar := TIME_TO_REAL(TON_StartPage.ET) / TIME_TO_REAL(TON_StartPage.PT) * 100;
	
	
	(****************************************************)
	(*				SPRAWDZENIE STATUSOW				*)
	(****************************************************)
	(******************Status bledu**********************)
	// error handler MASTER
	IF NOT gCM.Master.di_CtrlFaz OR
		NOT gCM.Master.dis_Grzyb OR
		NOT gCM.Master.di_CzujnikPowietrza
		THEN
		gMaster.Sts.Error := TRUE;
	END_IF
	
	IF  gBalkon.Sts.Error 	 OR
		gTransport.Sts.Error OR
		gMagazyn.Sts.Error 	 OR 
		gOwijarka.Sts.Error  OR
		TON_TimeoutPraca.Q   
		THEN
		gMaster.Sts.Error := TRUE;
	END_IF
	TON_ResetSafety();
	
	// wejscie w stan bledu
	IF gMaster.Sts.Error THEN
		gMaster.Sts.Step := MASTER_ERROR;
	END_IF
	
	
	(****************Fizyczne guziki i LEDy*********************)
	IF gCM.Master.di_StartAutomat > di_StartAutomatOld THEN
		gMaster.Cmd.Start := TRUE;
	END_IF
	
	IF gCM.Master.di_StopAutomat > di_StopAutomatOld THEN
		gMaster.Cmd.Stop := TRUE;
	END_IF
	
	IF gCM.Master.di_Pauza > di_PauzaOld AND gMaster.Sts.Step <> MASTER_WSTRZYMANY THEN
		gMaster.Cmd.Wstrzymaj := TRUE;
	END_IF
	
	IF gMaster.Sts.Step = MASTER_PRACA OR gMaster.Sts.Step = MASTER_WSTRZYMANY THEN
		gCM.Master.do_StartAutomat := TRUE;
	ELSE
		gCM.Master.do_StartAutomat := FALSE;
	END_IF
	
	IF gMaster.Sts.Step = MASTER_CZEKANIE OR gMaster.Sts.Step = MASTER_ZATRZYMANY OR gMaster.Sts.Step = MASTER_ERROR THEN
		gCM.Master.do_StopAutomat := TRUE;
	ELSE
		gCM.Master.do_StopAutomat := FALSE;
	END_IF	
	
	IF gMaster.Sts.Step = MASTER_WSTRZYMANY THEN
		gCM.Master.do_Pauza := TRUE;
	ELSE
		gCM.Master.do_Pauza := FALSE;
	END_IF		
	
	//gdy wszystkie z ponizszych sa na 0, to git
	TON_SafetyLED(IN := gCM.Master.var_SafetyOK, PT := T#5s);
	IF gCM.Master.var_SafetyOK AND TON_SafetyLED.Q THEN
		gCM.Master.do_ResetSafety := NOT(TON_SafetyLED.Q);
	ELSIF gCM.Master.var_SafetyOK THEN
		gCM.Master.do_ResetSafety := TP_0.Q;
		TOF_0.IN := TP_0.IN;  
		TP_0.IN := NOT(TOF_0.Q);
	ELSIF TON_ResetSafety.Q THEN
		gCM.Master.do_ResetSafety := TRUE;
	END_IF
		
	TOF_0(PT := T#1000ms);
	TP_0(PT := T#500ms);
		
	
	(****************Status gotowosci********************)
	
	gMaster.Sts.Gotowy := NOT gMaster.Sts.Error;
	
	(****************Reset statusow**********************)
	IF NOT gMaster.Cmd.Reset THEN
		gMaster.Sts.Zresetowano := FALSE;
	END_IF
	

	(****************************************************)
	(*					MASZYNA STANOW					*)
	(****************************************************)
	CASE gMaster.Sts.Step OF
		(********************************************)
		(*			WAIT - Czekaj na komendy		*)
		(********************************************)
		MASTER_CZEKANIE:
			TON_TimeoutPraca.IN := FALSE;
			
			IF gMaster.Cmd.Start > CmdStartOld THEN
				gMaster.Sts.Step := MASTER_URUCHAMIANIE;
			ELSIF ((gMaster.Cmd.Reset > CmdResetOld) AND NOT gMaster.Sts.Zresetowano) THEN
				gMaster.Sts.Step := MASTER_RESETOWANIE;
			END_IF
			
			(*******Zatrzymywanie procesu po wcisnieci stopu*****)
			IF gMaster.Cmd.Stop THEN
				IF gMaster.Sts.Step<>MASTER_ZATRZYMANY THEN
					gMaster.Sts.Step := MASTER_ZATRZYMYWANIE;
					Praca_step := 99;
				END_IF
			END_IF
					
			(********************************************)
			(*	STARTING- Startowanie modulow maszyny	*)
			(********************************************)
		MASTER_URUCHAMIANIE:
			// Ustawienie wszystkich modulow w tryb pracy
			IF NOT gMaster.Cmd.Stop THEN
				gTransport.Cmd.Start:= TRUE;
				gBalkon.Cmd.Start  	:= TRUE;
				gMagazyn.Cmd.Start  := TRUE;
				gOwijarka.Cmd.Start := TRUE;
			END_IF
			
			TON_TimeoutPraca.IN := TRUE;
			IF  gTransport.Sts.Step = TRANSPORT_PRACA AND
				gBalkon.Sts.Step = BALKON_PRACA AND
				gMagazyn.Sts.Step = MAGAZYN_PRACA AND
				gOwijarka.Sts.Step = OWIJARKA_PRACA
				THEN
				gMaster.Sts.Step := MASTER_PRACA;
				TON_TimeoutPraca.IN := FALSE;
			END_IF
			
			//Zatrzymanie procesu
			IF gMaster.Cmd.Stop THEN
				gMaster.Sts.Step := MASTER_ZATRZYMYWANIE;
			END_IF	
			
			IF gMaster.Cmd.Wstrzymaj THEN
				gMaster.Sts.Step := MASTER_WSTRZYMANY;
				gTransport.Cmd.Wstrzymaj:= TRUE;
				gBalkon.Cmd.Wstrzymaj 	:= TRUE;
				gMagazyn.Cmd.Wstrzymaj 	:= TRUE;
				gOwijarka.Cmd.Wstrzymaj := TRUE;
			END_IF
			
			(********************************************)
			(*			   RUN - Stan pracy				*)
			(********************************************)
		MASTER_PRACA:
			
			IF gMaster.Cmd.Stop THEN
				gMaster.Sts.Step := MASTER_ZATRZYMYWANIE;
			END_IF
			
			IF gMaster.Cmd.KoniecPalety > CmdKoniecPaletyOld THEN
				gTransport.Cmd.KoniecPalety := TRUE;
				gBalkon.Cmd.KoniecPalety := TRUE;
				gMagazyn.Cmd.KoniecPalety := TRUE;
				gOwijarka.Cmd.KoniecPalety := TRUE;
			ELSIF NOT(gMaster.Cmd.KoniecPalety) THEN
				gTransport.Cmd.KoniecPalety := FALSE;
				gBalkon.Cmd.KoniecPalety := FALSE;
				gMagazyn.Cmd.KoniecPalety := FALSE;
				gOwijarka.Cmd.KoniecPalety := FALSE;
			END_IF
			
			IF gMaster.Cmd.Wstrzymaj THEN
				gMaster.Sts.Step := MASTER_WSTRZYMANY;
				gTransport.Cmd.Wstrzymaj:= TRUE;
				gBalkon.Cmd.Wstrzymaj 	:= TRUE;
				gMagazyn.Cmd.Wstrzymaj 	:= TRUE;
				gOwijarka.Cmd.Wstrzymaj := TRUE;
			END_IF
			
			(********************************************)
			(*			   PAUSE - Stan pauzy			*)
			(********************************************)
		MASTER_WSTRZYMANY:
			IF NOT(gMaster.Cmd.Wstrzymaj) OR gCM.Master.di_Pauza > di_PauzaOld THEN
				gMaster.Sts.Step := MASTER_PRACA;
				gMaster.Cmd.Wstrzymaj := FALSE;
			ELSIF gMaster.Cmd.Stop THEN
				gMaster.Sts.Step := MASTER_ZATRZYMYWANIE;
			END_IF
					
			IF NOT(gMaster.Cmd.Wstrzymaj) THEN
				gTransport.Cmd.Wstrzymaj:= FALSE;
				gBalkon.Cmd.Wstrzymaj 	:= FALSE;
				gMagazyn.Cmd.Wstrzymaj 	:= FALSE;
				gOwijarka.Cmd.Wstrzymaj := FALSE;
			END_IF
			(********************************************)
			(*		   STOPPING - Stan zatrzymywania	*)
			(********************************************)
		MASTER_ZATRZYMYWANIE:
			// reset wstrzymania
			gTransport.Cmd.Wstrzymaj:= FALSE;
			gBalkon.Cmd.Wstrzymaj 	:= FALSE;
			gMagazyn.Cmd.Wstrzymaj 	:= FALSE;
			gOwijarka.Cmd.Wstrzymaj := FALSE;

			//reset startu
			gTransport.Cmd.Start:= FALSE;
			gBalkon.Cmd.Start  	:= FALSE;
			gMagazyn.Cmd.Start  := FALSE;
			gOwijarka.Cmd.Start := FALSE;

			//reset resetu :)
			gTransport.Cmd.Reset:= FALSE;
			gBalkon.Cmd.Reset  	:= FALSE;
			gMagazyn.Cmd.Reset  := FALSE;
			gOwijarka.Cmd.Reset := FALSE;

			// Stop wszystkich modulow
			gTransport.Cmd.Stop:= TRUE;
			gBalkon.Cmd.Stop  	:= TRUE;
			gMagazyn.Cmd.Stop  := TRUE;
			gOwijarka.Cmd.Stop := TRUE;

			gMaster.Sts.Step := MASTER_ZATRZYMANY;

			(********************************************)
			(*		STOPPED - Stan modul zatrzymany		*)
			(********************************************)
		MASTER_ZATRZYMANY:
			gMaster.Cmd.Start := FALSE;
			IF NOT gMaster.Cmd.Stop THEN
				gTransport.Cmd.Stop:= FALSE;
				gBalkon.Cmd.Stop  	:= FALSE;
				gMagazyn.Cmd.Stop  := FALSE;
				gOwijarka.Cmd.Stop := FALSE;
				gMaster.Sts.Step := MASTER_CZEKANIE;
			END_IF		

			(********************************************)
			(*		  RESETTING - Stan resetowania		*)
			(********************************************)
		MASTER_RESETOWANIE:
			gMaster.Cmd.Wstrzymaj := FALSE;
			gTransport.Cmd.Wstrzymaj:= FALSE;
			gBalkon.Cmd.Wstrzymaj 	:= FALSE;
			gMagazyn.Cmd.Wstrzymaj 	:= FALSE;
			gOwijarka.Cmd.Wstrzymaj := FALSE;
			
			gBalkon.Sts.PoZatrzymaniu  	:= FALSE;
			gMagazyn.Sts.PoZatrzymaniu  := FALSE;
			gOwijarka.Sts.PoZatrzymaniu := FALSE;
			gTransport.Sts.PoZatrzymaniu := FALSE;
			
			// Reset wszystkich modulow	
			CASE Master_Reset_Step OF
				0:
					gTransport.Cmd.Reset:= TRUE;
					gBalkon.Cmd.Reset  	:= TRUE;
					gMagazyn.Cmd.Reset  := TRUE;
					gOwijarka.Cmd.Reset := TRUE;
					Master_Reset_Step := 1;
				1:
					IF gTransport.Sts.Zresetowano AND
						gBalkon.Sts.Zresetowano AND
						gMagazyn.Sts.Zresetowano AND
						gOwijarka.Sts.Zresetowano 
						THEN
						gTransport.Cmd.Reset:= FALSE;
						gBalkon.Cmd.Reset  	:= FALSE;
						gMagazyn.Cmd.Reset  := FALSE;
						gOwijarka.Cmd.Reset := FALSE;
						gMaster.Sts.Zresetowano  := TRUE;
	
						//Warunek akcji konczacej, czy start czy reset
						IF gMaster.Sts.Zresetowano AND gMaster.Cmd.Start THEN 
							gMaster.Sts.Step := MASTER_URUCHAMIANIE;
						ELSIF gMaster.Sts.Zresetowano THEN
							gMaster.Sts.Step := MASTER_CZEKANIE;
						END_IF
						
						gMaster.Cmd.Reset := FALSE;
					END_IF
					Master_Reset_Step := 0;
			END_CASE

			IF gMaster.Cmd.Stop THEN
				gMaster.Sts.Step := MASTER_ZATRZYMYWANIE;
			END_IF
			
			(********************************************)
			(*				ERROR - Stan bledu			*)
			(********************************************)
		MASTER_ERROR:
			// reset wstrzymania
			gTransport.Cmd.Wstrzymaj:= FALSE;
			gBalkon.Cmd.Wstrzymaj 	:= FALSE;
			gMagazyn.Cmd.Wstrzymaj 	:= FALSE;
			gOwijarka.Cmd.Wstrzymaj := FALSE;

			//reset startu
			gTransport.Cmd.Start:= FALSE;
			gBalkon.Cmd.Start  	:= FALSE;
			gMagazyn.Cmd.Start  := FALSE;
			gOwijarka.Cmd.Start := FALSE;
			gMaster.Cmd.Start 	:= FALSE;

			//reset resetu :)
			gTransport.Cmd.Reset:= FALSE;
			gBalkon.Cmd.Reset  	:= FALSE;
			gMagazyn.Cmd.Reset  := FALSE;
			gOwijarka.Cmd.Reset := FALSE;

			// Stop wszystkich modulow
			gTransport.Cmd.Stop:= TRUE;
			gBalkon.Cmd.Stop  	:= TRUE;
			gMagazyn.Cmd.Stop  := TRUE;
			gOwijarka.Cmd.Stop := TRUE;
			
			gBalkon.Sts.PoZatrzymaniu  	:= TRUE;
			gMagazyn.Sts.PoZatrzymaniu  := TRUE;
			gOwijarka.Sts.PoZatrzymaniu := TRUE;
			gTransport.Sts.PoZatrzymaniu := TRUE;
		
			IF NOT gMaster.Sts.Error THEN
				gTransport.Cmd.Stop:= FALSE;
				gBalkon.Cmd.Stop  	:= FALSE;
				gMagazyn.Cmd.Stop  := FALSE;
				gOwijarka.Cmd.Stop := FALSE;
			
				gTransport.Cmd.ErrorReset:= FALSE;
				gBalkon.Cmd.ErrorReset  	:= FALSE;
				gMagazyn.Cmd.ErrorReset  := FALSE;
				gOwijarka.Cmd.ErrorReset := FALSE;
		
				gMaster.Sts.Step := MASTER_CZEKANIE;
			END_IF
	
			IF gMaster.Cmd.ErrorReset THEN
				gMaster.Cmd.ErrorReset 		:= FALSE;
				gTransport.Cmd.ErrorReset	:= TRUE;
				gBalkon.Cmd.ErrorReset  	:= TRUE;
				gMagazyn.Cmd.ErrorReset  	:= TRUE;
				gOwijarka.Cmd.ErrorReset 	:= TRUE;
				gMaster.Sts.Error			:= FALSE;
			END_IF
	END_CASE
			
	///////oldy
	CmdStartOld := gMaster.Cmd.Start;
	CmdResetOld := gMaster.Cmd.Reset;
	CmdKoniecPaletyOld := gMaster.Cmd.KoniecPalety;
	///////oldy fizyczne przyciski
	di_StartAutomatOld := gCM.Master.di_StartAutomat;
	di_StopAutomatOld := gCM.Master.di_StopAutomat;
	di_PauzaOld := gCM.Master.di_Pauza;
	di_ResetOld := gCM.Master.di_ResetSafety;
	
	
	(***********Przypisanie alarmow do tablicy***********)	
	//blad od inicjalizacji PRACA
	IF TON_TimeoutPraca.Q THEN
		Alarmy_Master[0] := TRUE;
	ELSE 
		Alarmy_Master[0] := FALSE;
	END_IF
	
	IF NOT gCM.Master.dis_Grzyb THEN
		Alarmy_Master[1] := TRUE;
	ELSE
		Alarmy_Master[1] := FALSE;
	END_IF
	
	IF NOT gCM.Master.di_CtrlFaz THEN
		Alarmy_Master[2] := TRUE;
	ELSE
		Alarmy_Master[2] := FALSE;
	END_IF
	
	IF NOT  gCM.Master.di_CzujnikPowietrza THEN
		Alarmy_Master[3] := TRUE;
	ELSE
		Alarmy_Master[3] := FALSE;
	END_IF
	
END_PROGRAM

PROGRAM _EXIT
(* Insert code here *)

END_PROGRAM