
TYPE
	Retain_typ : 	STRUCT 
		Transport : RetainTransport_typ;
		Balkon : RetainBalkon_typ;
		Magazyn : RetainMagazyn_typ;
		Owijarka : RetainOwijarka_typ;
		Produkty : Produkty_typ;
		Czasy : Czasy_typ;
		Predkosci : Retain_Predkosci_typ;
	END_STRUCT;
	RetainTransport_typ : 	STRUCT 
		Ruch_Transport10 : BOOL;
		Ruch_Transport11 : BOOL;
		Ruch_Transport01 : BOOL;
		Ruch_Transport02Dlugi : BOOL;
		Ruch_Transport02Krotki : BOOL;
		Ruch_Transport03 : BOOL;
		Sts_WarstwaGotowa : BOOL;
	END_STRUCT;
	RetainBalkon_typ : 	STRUCT 
		Praca_step_Balkon : INT;
		RuchBalkonForward : BOOL;
		RuchBalkonReverse : BOOL;
		RuchSlupyForward : BOOL;
		RuchSlupyReverse : BOOL;
		StatusUkladanieWarstwy : BOOL;
		PaletaGotowa : BOOL;
		Enkoder : DINT;
		PredkoscSlupy : INT;
		PredkoscBalkon : INT;
		Silownik_Zderzak : BOOL;
		Silownik_Warstwa : BOOL;
	END_STRUCT;
	RetainMagazyn_typ : 	STRUCT 
		Praca_step_Magazyn : INT;
		RuchLancucha : BOOL;
		RuchSilownikDown : BOOL;
		RuchSilownikUp : BOOL;
		StatusPaletaZabrana : BOOL;
		Silownik_BlokPal : BOOL;
		Silownik_Lanuchowy1 : BOOL;
	END_STRUCT;
	RetainOwijarka_typ : 	STRUCT 
		Praca_step_Owijarka : INT;
		RuchRolkiMagazyn : BOOL;
		RuchRolkiOwijarka : BOOL;
		LiczbaPalet : INT;
	END_STRUCT;
	Produkty_typ : 	STRUCT 
		WysokosciProduktow : ARRAY[0..4]OF INT;
		ListaProduktow : ARRAY[0..4]OF STRING[20];
	END_STRUCT;
	Retain_Predkosci_typ : 	STRUCT 
		Balkon : PredkoscBalkon_typ;
		Slupy : PredkoscSlupy_typ;
		Transport : PredkosciTransportery_typ;
		Owijarka : PredkosciOwijarka_typ;
	END_STRUCT;
	WartosciDomyslne_typ : 	STRUCT 
		Predkosci : Retain_Predkosci_typ;
		Czasy : Czasy_typ;
	END_STRUCT;
END_TYPE
