
PROGRAM _INIT
	
	// di_S16_PozycjaBalkonu_1 - zwalnianie przy dojezdzie do pozycji bazowej, di_S17_PozycjaBalkonu_2 - pozycja bazowa
	// di_S18_PozycjaBalkonu_3 - pozycja koncowa, di_S19_PozycjaBalkonu_4 - zwalnianie do pozycji koncowej (zaladunku)
	
	gBalkon.Par.PredkoscBalkon := gRetain.Predkosci.Balkon;
	gBalkon.Par.PredkoscSlupy := gRetain.Predkosci.Slupy;
	
	CzasZalaczeniaHamulca := T#1s;

END_PROGRAM

PROGRAM _CYCLIC
	(****************************************************)
	(*				SPRAWDZENIE STATUSOW				*)
	(****************************************************)
	
	IF gBalkon.Cmd.ErrorReset AND gBalkon.Sts.Step <> BALKON_ERROR THEN
		gBalkon.Cmd.ErrorReset := FALSE;
	END_IF
	
	(******************Status bledu**********************)
	//wywolanie bledu od napedow balkonu *****************************************************************
	IF gDATA.Balkon.MT7_04Balkon.Status.errorCode <> 0 OR
		gDATA.Balkon.MT8_05Slupy.Status.errorCode <> 0 OR
		gCM.Balkon.di_S26_Polozenie_Slup_2 > Balkon_di_S26_PolozenieSlup2_Old OR
		(gCM.Balkon.di_S25_Polozenie_Slup_1 > Balkon_di_S25_PolozenieSlup1_Old AND gBalkon.Sts.Step <> BALKON_RESETOWANIE) OR
		gDATA.Balkon.Z5_Silownik_Zderzak.STS.ALM.InAlm OR
		gDATA.Balkon.Z4_Silownik_Warstwa.STS.ALM.InAlm OR
		alarm_BalkonZaNisko
		THEN
		gBalkon.Sts.Error := TRUE;
	END_IF
	
	// Zablokowanie mozliwosci ruchu nad krancowke, rownoczesnie dopuszczenie ruchu, ktory pozwoli na zjechanie z tej krancowki
	IF gCM.Balkon.di_S25_Polozenie_Slup_1 THEN
		gDATA.Balkon.MT8_05Slupy.Command.reverse := FALSE;
	ELSIF gCM.Balkon.di_S26_Polozenie_Slup_2 THEN
		gDATA.Balkon.MT8_05Slupy.Command.forward := FALSE;
	END_IF
	
	IF gCM.Balkon.di_S17_PozycjaBalkonu_2 THEN
		gDATA.Balkon.MT7_04Balkon.Command.forward := FALSE;
	ELSIF gCM.Balkon.di_S18_PozycjaBalkonu_3 THEN
		gDATA.Balkon.MT7_04Balkon.Command.reverse := FALSE;
	END_IF
	
	IF (gDATA.Balkon.MT8_05Slupy.Command.forward OR gDATA.Balkon.MT8_05Slupy.Command.reverse) THEN
		TON_Hamulec.IN := FALSE;
		gCM.Balkon.P2_Hamulec := TRUE;
	ELSE
		TON_Hamulec();
		TON_Hamulec.PT := CzasZalaczeniaHamulca;
		TON_Hamulec.IN := TRUE;
		IF TON_Hamulec.Q THEN
			TON_Hamulec.IN := FALSE;
			gCM.Balkon.P2_Hamulec := FALSE;
		END_IF
	END_IF
	
	//wejscie w stan bledu 
	IF gBalkon.Sts.Error THEN 
		gBalkon.Sts.Step := BALKON_ERROR;
	END_IF
	
	// Zapisanie ostatniego stanu pracy przed bledem
	IF Praca_step <> 99 AND gBalkon.Sts.Step = BALKON_PRACA THEN
		gRetain.Balkon.Praca_step_Balkon := Praca_step;
		gRetain.Balkon.PaletaGotowa 			:= gBalkon.Sts.PaletaGotowa;
		gRetain.Balkon.StatusUkladanieWarstwy 	:= gBalkon.Sts.MozliweUkladanieWarstwy;
		gRetain.Balkon.RuchSlupyForward 		:= gDATA.Balkon.MT8_05Slupy.Command.forward;
		gRetain.Balkon.RuchSlupyReverse 		:= gDATA.Balkon.MT8_05Slupy.Command.reverse;
		gRetain.Balkon.RuchBalkonForward 		:= gDATA.Balkon.MT7_04Balkon.Command.forward;
		gRetain.Balkon.RuchBalkonReverse 		:= gDATA.Balkon.MT7_04Balkon.Command.reverse;
		gRetain.Balkon.PredkoscBalkon 			:= gDATA.Balkon.MT7_04Balkon.Set.setRPM;
		gRetain.Balkon.PredkoscSlupy 			:= gDATA.Balkon.MT8_05Slupy.Set.setRPM;
		gRetain.Balkon.Silownik_Warstwa 		:= gDATA.Balkon.Z4_Silownik_Warstwa.CS.Control;
		gRetain.Balkon.Silownik_Zderzak 		:= gDATA.Balkon.Z5_Silownik_Zderzak.CS.Control;
	END_IF

	(****************Status gotowosci********************)
	
	gBalkon.Sts.Gotowy := NOT gBalkon.Sts.Error;

	(****************Reset statusow**********************)
	IF (gBalkon.Cmd.Reset > cmdReset_old) OR (gBalkon.Sts.Step = BALKON_PRACA) THEN
		gBalkon.Sts.Zresetowano := FALSE;
	END_IF
	
	(****************************************************)
	(*					MASZYNA STANOW					*)
	(****************************************************)
	CASE gBalkon.Sts.Step OF
		(********************************************)
		(*			WAIT - Czekaj na komendy		*)
		(********************************************)
		BALKON_CZEKANIE:
			//Przejscie do procedury startu, pierwszys start zaczunamy od resetu
			IF (gBalkon.Cmd.Reset AND NOT gBalkon.Sts.Zresetowano) THEN
				gBalkon.Sts.Step := BALKON_RESETOWANIE;
			ELSIF gBalkon.Cmd.Start > cmdStart_old AND gBalkon.Sts.PoZatrzymaniu THEN
				gBalkon.Sts.Step := BALKON_PO_ZATRZYMANIU;
			ELSIF (gBalkon.Cmd.Start > cmdStart_old AND NOT(gBalkon.Sts.PoZatrzymaniu)) THEN
				gBalkon.Sts.Step := BALKON_PO_ZATRZYMANIU;
			END_IF
			
			Reset_step := 0;
			
			//Nacisniecie Stopu
			IF gBalkon.Cmd.Stop THEN 
				gBalkon.Sts.Step := BALKON_ZATRZYMYWANIE;
			END_IF
			
			(********************************************)
			(*			 STARTING - Stan startu			*)
			(********************************************)
		BALKON_URUCHAMIANIE:
			//zatrzymanie maszyny 
			IF NOT gBalkon.Cmd.Start OR gBalkon.Cmd.Stop THEN
				gBalkon.Sts.Step := BALKON_ZATRZYMYWANIE;
			END_IF
			
			IF gBalkon.Cmd.Wstrzymaj THEN
				gBalkon.Sts.Step := BALKON_WSTRZYMYWANIE;
			END_IF
			
			gBalkon.Sts.MozliweUkladanieWarstwy := TRUE;

			gBalkon.Sts.Step := BALKON_PRACA;
			Praca_step := 0;
			
			(********************************************)
			(*			   RUN - Stan pracy				*)
			(********************************************)
		BALKON_PRACA:
			gBalkon.Sts.PoZatrzymaniu := FALSE;
			
			//zatrzymanie maszyny 
			IF NOT gBalkon.Cmd.Start OR gBalkon.Cmd.Stop THEN
				gBalkon.Sts.Step := BALKON_ZATRZYMYWANIE;
				Praca_step := 99;
			END_IF
			
			IF Praca_step > 1 THEN
				gTransport.Sts.WarstwaGotowa := FALSE;
			END_IF
			
			//praca procesowa	
			CASE Praca_step OF
				0:
					IF gBalkon.Cmd.Wstrzymaj THEN
						gBalkon.Sts.Step := BALKON_WSTRZYMYWANIE;
					ELSE
						gDATA.Balkon.Z4_Silownik_Warstwa.CS.Control := TRUE; // Schowanie blokady (0 = Close, 1 = Open)
						Praca_step := 1;
					END_IF
				1:
					// Oczekiwanie na sygnal o ulozeniu calej warstwy i rozpoczeciu pracy
					// Zmienna statusowa gBalkon.Sts.MozliweUkladanieWarstwy sluzy do komunikacji z modulem spychacza.
					// Informuje o tym, ze nie mozna w tej chwili zepchnac kolejnego rzedu, bo platforma/balkon jest w trakcie odkladania warstwy/jest w innej pozycji.
					IF gTransport.Sts.WarstwaGotowa THEN
						gBalkon.Sts.MozliweUkladanieWarstwy := FALSE;
						gTransport.Sts.WarstwaGotowa := FALSE;
						Praca_step := 2;
					END_IF	
				2:
					// 1. Gdy pojawi sie sygnal o ulozeniu calej warstwy na platformie balkon podnosi sie na wysokosc zadana z receptury. Wysokosc okreslana jest na podstawie enkodera.
					// Trzeba sprawdzic czy ruch powinien byc w gore, czy w dol, czyli czy dana wysokosc jest powyzej czy ponizej pozycji, na ktorej obecnie jest platforma
					// Przyjeto w calym programie, ze dla slupow: REVERSE - RUCH W GORE /\, FORWARD - DOL \/  -> do sprawdzenia i ew. zmiany.
					IF Enkoder < gBalkon.Par.EnkPozycja.WysokosciWarstw[nr_warstwy] THEN
						gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.PracyPoczatkowa;
						gDATA.Balkon.MT8_05Slupy.Command.reverse := TRUE;
						Praca_step := 3;
					ELSE
						gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.PracyPoczatkowa;
						gDATA.Balkon.MT8_05Slupy.Command.forward := TRUE;
						IF gCM.Balkon.di_S24_Balkon_nisko THEN
							gDATA.Balkon.MT7_04Balkon.Set.setRPM := gBalkon.Par.PredkoscBalkon.PrzodPoczatkowa;
							gDATA.Balkon.MT7_04Balkon.Command.reverse := TRUE;
							Praca_step := 4;
						ELSE
							alarm_BalkonZaNisko := TRUE;
						END_IF
					END_IF
				3:
					// 2.	Jesli osiagnelismy odpowiednia wysokosc balkon zatrzymuje sie.
					IF Enkoder >= gBalkon.Par.EnkPozycja.WysokosciWarstw[nr_warstwy] THEN
						gDATA.Balkon.MT8_05Slupy.Command.reverse := FALSE;
						Praca_step := 5;
					END_IF
				4:
					// 2.	Jesli osiagnelismy odpowiednia wysokosc balkon zatrzymuje sie.
					IF Enkoder <= gBalkon.Par.EnkPozycja.WysokosciWarstw[nr_warstwy] THEN
						gDATA.Balkon.MT8_05Slupy.Command.forward := FALSE;
						Praca_step := 6;
					END_IF
					// Zmniejszenie predkosci dojazdu do pozycji
					IF gCM.Balkon.di_S19_PozycjaBalkonu_4 THEN
						gDATA.Balkon.MT7_04Balkon.Set.setRPM := gBalkon.Par.PredkoscBalkon.PrzodKoncowa;
					END_IF
				5:
					gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.PracyDojazd;
					gDATA.Balkon.MT8_05Slupy.Command.forward := TRUE;
					IF Enkoder <= gBalkon.Par.EnkPozycja.WysokosciWarstw[nr_warstwy] THEN
						gDATA.Balkon.MT8_05Slupy.Command.forward := FALSE;
						Praca_step := 55;
					END_IF
				55:
					// 3.	Ruch rozpoczyna platforma poruszajac sie w strone palety
					// Przyjeto w calym projekcie, ze dla platformy: REVERSE ruch w strone czujnika I, FORWARD ruch do czujnika IV (powrotny)
					// W tym miejscu sprawdzany jest odczyt z czujnika 'Czujnik balkon za nisko'
					IF gCM.Balkon.di_S24_Balkon_nisko THEN
						gDATA.Balkon.MT7_04Balkon.Set.setRPM := gBalkon.Par.PredkoscBalkon.PrzodPoczatkowa;
						gDATA.Balkon.MT7_04Balkon.Command.reverse := TRUE;
						Praca_step := 6;
					ELSE
						alarm_BalkonZaNisko := TRUE;
					END_IF
				6:
					gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.PracyDojazd;
					gDATA.Balkon.MT8_05Slupy.Command.reverse := TRUE;
					IF Enkoder >= gBalkon.Par.EnkPozycja.WysokosciWarstw[nr_warstwy] THEN
						gDATA.Balkon.MT8_05Slupy.Command.reverse := FALSE;
						Praca_step := 66;
					END_IF
					// Zmniejszenie predkosci dojazdu do pozycji
					IF gCM.Balkon.di_S19_PozycjaBalkonu_4 THEN
						gDATA.Balkon.MT7_04Balkon.Set.setRPM := gBalkon.Par.PredkoscBalkon.PrzodKoncowa;
					END_IF
				66:
					// Zmniejszenie predkosci dojazdu do pozycji
					IF gCM.Balkon.di_S24_Balkon_nisko THEN
						IF gCM.Balkon.di_S19_PozycjaBalkonu_4 THEN
							gDATA.Balkon.MT7_04Balkon.Set.setRPM := gBalkon.Par.PredkoscBalkon.PrzodKoncowa;
							Praca_step := 7;
						ELSIF gCM.Balkon.di_S19_PozycjaBalkonu_4 THEN
							Praca_step := 7;
						END_IF
					ELSE
						alarm_BalkonZaNisko := TRUE;
					END_IF	
				7:
					// Zatrzymanie balkonu jesli osiagnal pozycje koncowa
					IF gCM.Balkon.di_S18_PozycjaBalkonu_3 THEN
						gDATA.Balkon.MT7_04Balkon.Command.reverse := FALSE;
						Praca_step := 8;
					END_IF
				8:
					// Wysuwane sa blokady warstwy
					// Przyjeto, ze gdy blokady sa WYSUNIETE na wyjsciu silownika jest TRUE, SCHOWANE - FALSE
					gDATA.Balkon.Z5_Silownik_Zderzak.CS.Control := TRUE;
					Praca_step := 9;
				9:
					// Jesli blokada jest wysunieta to wysuwamy zderzak
					// Przyjeto, ze gdy zderzak jest WYSUNIETY na wyjsciu silownika jest TRUE, SCHOWANY - FALSE
					IF gDATA.Balkon.Z5_Silownik_Zderzak.STS.IsOpened THEN
						gDATA.Balkon.Z4_Silownik_Warstwa.CS.Control := FALSE; // (0 = Close, 1 = Open)
						Praca_step := 10;
					END_IF
				10:
					// Jesli zderzak jest wysuniety rozpoczynamy ruch powrotny platformy/balkonu
					IF gDATA.Balkon.Z4_Silownik_Warstwa.STS.IsClosed THEN
						gDATA.Balkon.MT7_04Balkon.Set.setRPM := gBalkon.Par.PredkoscBalkon.PowrotPoczatkowa;
						gDATA.Balkon.MT7_04Balkon.Command.forward := TRUE;
						Praca_step := 11;
					END_IF
				11:
					// Zmniejszenie predkosci po dojechaniu do czujnika IV
					IF gCM.Balkon.di_S16_PozycjaBalkonu_1 THEN
						gDATA.Balkon.MT7_04Balkon.Set.setRPM := gBalkon.Par.PredkoscBalkon.PowrotKoncowa;
						Praca_step := 12;
					END_IF
				12:
					// Zatrzymanie platformy/balkonu po dojechaniu na pozycje poczatkowa, czujnik I
					IF gCM.Balkon.di_S17_PozycjaBalkonu_2 THEN
						gDATA.Balkon.MT7_04Balkon.Command.forward := FALSE;
						Praca_step := 13;
					END_IF
				13:
					// Schowanie zderzaka i blokad
					gDATA.Balkon.Z4_Silownik_Warstwa.CS.Control := TRUE; // (0 = Close, 1 = Open)
					gDATA.Balkon.Z5_Silownik_Zderzak.CS.Control := FALSE;
					Praca_step := 14;
				14:	
					// Jesli blokady i zderzak sa schowane to sprawdzamy czy zostaly ulozone wszystkie warstwy czy nie
					IF gDATA.Balkon.Z5_Silownik_Zderzak.STS.IsClosed THEN
						IF nr_warstwy = (Receptura.LiczbaWarstw - 1) OR gBalkon.Cmd.KoniecPalety THEN // numeracja warstw rozpoczyna sie od 0, dlatego musimy porownywac do liczby warstw o 1 mniejszej
							Statystyki.Partia.Warstw := Statystyki.Partia.Warstw + 1;
							Statystyki.Zycie.Warstw  := Statystyki.Zycie.Warstw + 1;
							
							gBalkon.Cmd.KoniecPalety := FALSE;
							nr_warstwy := 0;
							Praca_step := 15;
						ELSE
							Statystyki.Partia.Warstw := Statystyki.Partia.Warstw + 1;
							Statystyki.Zycie.Warstw  := Statystyki.Zycie.Warstw + 1;
							
							nr_warstwy := nr_warstwy + 1; // inkrementacja numeru warstwy, zmienna sluzy do przesuwania sie po tablicy wysokosci kolejnych warstw
							Praca_step := 18;
						END_IF	
					END_IF
				15:
					// Odjazd balkonu na sama g�re, aby umozliwic wyjazd pelnej palety
					gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.PracyPoczatkowa;
					gDATA.Balkon.MT8_05Slupy.Command.reverse := TRUE;
					Praca_step := 16;
				16:
					// Zatrzymanie balkonu/slupow i wyslanie sygnalu do owijarki, ze paleta moze zostac zabrana
					IF Enkoder >= gBalkon.Par.EnkPozycja.WyjazdPalety THEN
						gDATA.Balkon.MT8_05Slupy.Command.reverse := FALSE;
						gBalkon.Sts.PaletaGotowa := TRUE;
						Praca_step := 17;
					END_IF
				17:
					// Jesli otrzymamy sygnal od owijarki o zabraniu palety na owijarke jedziemy na pozycje pozwalajaca ukladac kolejne warstwy
					// Zresetowanie statusu PaletaGotowa
					IF gMagazyn.Sts.PaletaZabrana THEN
						gBalkon.Sts.PaletaGotowa := FALSE;
						Praca_step := 18;
					END_IF
				18:
					// Powrot balkonu/slupow na pozycje pozwalajaca ukladac kolejne warstwy
					// Sprawdzenie czy trzeba wykonac ruch do gory czy w dol
					IF gBalkon.Cmd.Wstrzymaj THEN
						gBalkon.Sts.Step := BALKON_WSTRZYMYWANIE;
					ELSE
						IF Enkoder > gBalkon.Par.EnkPozycja.UkladanieWarstwy THEN
							IF Enkoder > gBalkon.Par.EnkPozycja.WyjazdPalety THEN
								gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.PracyOdjazdDol;
							ELSE
								gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.PracyPoczatkowa;
							END_IF
							gDATA.Balkon.MT8_05Slupy.Command.forward := TRUE;
							Praca_step := 19;
						ELSE
							gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.PracyPoczatkowa;
							gDATA.Balkon.MT8_05Slupy.Command.reverse := TRUE;
							Praca_step := 20;
						END_IF
					END_IF
				19:
					// Zatrzymanie balkonu/slupow po osiagnieciu odpowiedniej wysokosci
					IF Enkoder <= gBalkon.Par.EnkPozycja.UkladanieWarstwy THEN
						gDATA.Balkon.MT8_05Slupy.Command.forward := FALSE;
						Praca_step := 199;
					END_IF
				199:
					gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.PracyDojazd;
					gDATA.Balkon.MT8_05Slupy.Command.reverse := TRUE;
					IF Enkoder >= gBalkon.Par.EnkPozycja.UkladanieWarstwy THEN
						gDATA.Balkon.MT8_05Slupy.Command.reverse := FALSE;
						Praca_step := 22;
					END_IF
				20:
					// Zatrzymanie balkonu/slupow po osiagnieciu odpowiedniej wysokosci
					IF Enkoder >= gBalkon.Par.EnkPozycja.UkladanieWarstwy THEN
						gDATA.Balkon.MT8_05Slupy.Command.reverse := FALSE;
						Praca_step := 21;
					END_IF
				21:
					gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.PracyDojazd;
					gDATA.Balkon.MT8_05Slupy.Command.forward := TRUE;
					IF Enkoder <= gBalkon.Par.EnkPozycja.UkladanieWarstwy THEN
						gDATA.Balkon.MT8_05Slupy.Command.forward := FALSE;
						Praca_step := 22;
					END_IF
					
				22:
					IF gDATA.Balkon.Z4_Silownik_Warstwa.STS.IsOpened THEN
					// Wyslanie sygnalu, potwierdzenia do modulu spychacza, ze mozna ukladac kolejna warstwe
					gBalkon.Sts.MozliweUkladanieWarstwy := TRUE;
					Praca_step := 0;
					END_IF
				99:
				//blad
			END_CASE
		
			(********************************************)
			(*		   WSTRZYMAJ - Stan wstrzymywania	*)
			(********************************************)	
		BALKON_WSTRZYMYWANIE:
			
			//kod ktory pozwoli ten modul wstrzymac
			gBalkon.Sts.Step := BALKON_WSTRZYMANY;
			
			(********************************************)
			(*		   WSTRZYMANY - Stan wstrzymania	*)
			(********************************************)	
		BALKON_WSTRZYMANY:
			
			IF NOT(gBalkon.Cmd.Wstrzymaj) THEN
				gBalkon.Sts.Step := BALKON_PRACA;
			END_IF
			
			IF gBalkon.Cmd.Stop THEN
				gBalkon.Sts.Step := BALKON_ZATRZYMYWANIE;
			END_IF


			(********************************************)
			(*		   STOPPING - Stan zatrzymywania	*)
			(********************************************)
		BALKON_ZATRZYMYWANIE:
			
			gDATA.Balkon.MT7_04Balkon.Command.forward := FALSE;
			gDATA.Balkon.MT7_04Balkon.Command.reverse := FALSE;
			
			gDATA.Balkon.MT8_05Slupy.Command.forward := FALSE;
			gDATA.Balkon.MT8_05Slupy.Command.reverse := FALSE;
			
			//przejscie do stanu oczekiwania w zatrzymaniu 
			gBalkon.Sts.Step := BALKON_ZATRZYMANY;
			
			(********************************************)
			(*		STOPPED - Stan modul zatrzymany		*)
			(********************************************)
		BALKON_ZATRZYMANY:
			
			//o ile komenda stop nie jest aktywna to wchodzimy w aktywne czekanie na inna komende
			IF NOT gBalkon.Cmd.Stop THEN 
				gBalkon.Sts.Step := BALKON_CZEKANIE;
			END_IF
			
			(********************************************)
			(*		  RESETTING - Stan resetowania		*)
			(********************************************)
		BALKON_RESETOWANIE:
			
			Praca_step := 0;
			nr_warstwy := 0;
			
			gRetain.Balkon.Praca_step_Balkon 		:= 0;
			gRetain.Balkon.PaletaGotowa 			:= FALSE;
			gRetain.Balkon.StatusUkladanieWarstwy 	:= TRUE;
			gRetain.Balkon.RuchSlupyForward 		:= FALSE;
			gRetain.Balkon.RuchSlupyReverse			:= FALSE;
			gRetain.Balkon.RuchBalkonForward 		:= FALSE;
			gRetain.Balkon.RuchBalkonReverse 		:= FALSE;
			gRetain.Balkon.PredkoscBalkon 			:= FALSE;
			gRetain.Balkon.Silownik_Warstwa			:= FALSE;
			gRetain.Balkon.Silownik_Zderzak			:= TRUE;
			
			CASE Reset_step OF
				0:
					Reset_step := 1;
				1:
					// Jesli nie ma warstwy to resetujemy blokady warstwy i zderzak
					gDATA.Balkon.Z4_Silownik_Warstwa.CS.Control := TRUE; // Schowanie blokady (0 = Close, 1 = Open)
					gDATA.Balkon.Z5_Silownik_Zderzak.CS.Control := FALSE; // Schowanie zderzaka (0 = Close, 1 = Open)
					Reset_step := 2;
				2:
					// Jesli blokady schowane to resetujemy platforme -> przesuwamy sie na pozycje bazowa.
					IF gDATA.Balkon.Z4_Silownik_Warstwa.STS.IsOpened AND gDATA.Balkon.Z5_Silownik_Zderzak.STS.IsClosed THEN
						Reset_step := 3;
					END_IF
				3:
					IF gCM.Balkon.di_S17_PozycjaBalkonu_2 THEN
						gDATA.Balkon.MT7_04Balkon.Set.setRPM := gBalkon.Par.PredkoscBalkon.Reset;
						gDATA.Balkon.MT7_04Balkon.Command.reverse := TRUE;
						Reset_step := 4;
					ELSE
						gDATA.Balkon.MT7_04Balkon.Set.setRPM := gBalkon.Par.PredkoscBalkon.Reset;
						gDATA.Balkon.MT7_04Balkon.Command.forward := TRUE;
						Reset_step := 5;
					END_IF
				4:
					// Jesli zjechalismy z krancowki to zatrzymaj sie i wroc na krancowke
					IF gCM.Balkon.di_S17_PozycjaBalkonu_2 < Balkon_diS17_PozycjaBalkonu2_Old THEN
						gDATA.Balkon.MT7_04Balkon.Command.reverse := FALSE;
						gDATA.Balkon.MT7_04Balkon.Command.forward := TRUE;
						Reset_step := 5;
					END_IF
				5:
					// Jesli platforma na pozycji bazowej (1) to zatrzymaj sie
					IF gCM.Balkon.di_S17_PozycjaBalkonu_2 > Balkon_diS17_PozycjaBalkonu2_Old THEN  //zakomentowane, bo czujnik nie przykrecony - odkomentowac pozniej
						gDATA.Balkon.MT7_04Balkon.Command.forward := FALSE;
						Reset_step := 55;
					END_IF
				55:
					gDATA.Balkon.MT7_04Balkon.Set.setRPM := gBalkon.Par.PredkoscBalkon.ResetDojazd;
					gDATA.Balkon.MT7_04Balkon.Command.reverse := TRUE;
					IF gCM.Balkon.di_S17_PozycjaBalkonu_2 < Balkon_diS17_PozycjaBalkonu2_Old THEN  //zakomentowane, bo czujnik nie przykrecony - odkomentowac pozniej
						gDATA.Balkon.MT7_04Balkon.Command.reverse := FALSE;
						Reset_step := 6;
					END_IF
				6:
					// Sprawdzenie czy jestesmy na gornej krancowce, jesli tak to jedziemy w dol az z niej zjedziemy
					IF gCM.Balkon.di_S25_Polozenie_Slup_1 THEN
						gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.ResetStart;
						gDATA.Balkon.MT8_05Slupy.Command.forward := TRUE;
						Reset_step := 7;
					ELSE
						gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.ResetStart;
						gDATA.Balkon.MT8_05Slupy.Command.reverse := TRUE;
						Reset_step := 8;	
					END_IF
				7:
					// Jesli zjechalismy z krancowki to zatrzymujemy sie i rozpoczynamy ruch w druga strone
					IF gCM.Balkon.di_S25_Polozenie_Slup_1 < Balkon_di_S25_PolozenieSlup1_Old THEN
						gDATA.Balkon.MT8_05Slupy.Command.forward := FALSE;
						gDATA.Balkon.MT8_05Slupy.Command.reverse := TRUE;
						Reset_step := 8;
					END_IF
				8:	
					// Jesli najechalismy na gorna krancowke balkonu to reset enkodera.
					IF gCM.Balkon.di_S25_Polozenie_Slup_1 > Balkon_di_S25_PolozenieSlup1_Old THEN
						gDATA.Balkon.MT8_05Slupy.Command.reverse := FALSE;
						gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.ResetStop;
						gDATA.Balkon.MT8_05Slupy.Command.forward := TRUE;
						Reset_step := 88;
					END_IF
				88:
					IF gCM.Balkon.di_S25_Polozenie_Slup_1 < Balkon_di_S25_PolozenieSlup1_Old THEN		
						gDATA.Balkon.MT8_05Slupy.Command.forward := FALSE;
						di_Enkoder_reset := TRUE;
						Reset_step := 9;
					END_IF
				9:
					// Powrot na dol, aby mozliwe bylo ukladanie zgrzewek
					di_Enkoder_reset := FALSE;
					IF NOT(gCM.Balkon.P2_Hamulec) THEN
						Reset_step := 10;
					END_IF
				10:
					gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.ResetStart;
					gDATA.Balkon.MT8_05Slupy.Command.forward := TRUE;
					IF Enkoder <= gBalkon.Par.EnkPozycja.UkladanieWarstwy THEN
						gDATA.Balkon.MT8_05Slupy.Command.forward := FALSE;
						Reset_step := 11;
					END_IF
				11:
					gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.PracyDojazd;
					gDATA.Balkon.MT8_05Slupy.Command.reverse := TRUE;
					IF Enkoder >= gBalkon.Par.EnkPozycja.UkladanieWarstwy THEN
						gDATA.Balkon.MT8_05Slupy.Command.reverse := FALSE;
						Reset_step := 12;
					END_IF
				12:
					//jezeli z sukcesem wykonane powyzsze to Zresetowano := TRUE;
					gBalkon.Sts.Zresetowano := TRUE;
					Reset_step := 0;
					
			END_CASE
			
			//Stop lub zdjecie Startu
			IF gBalkon.Cmd.Stop THEN 
				gBalkon.Sts.Step := BALKON_ZATRZYMYWANIE;
			END_IF
			
			//Warunek akcji konczacej, czy start czy reset
			IF gBalkon.Sts.Zresetowano AND gBalkon.Cmd.Start THEN 
				gBalkon.Sts.Step := BALKON_URUCHAMIANIE;
			ELSIF gBalkon.Sts.Zresetowano THEN
				gBalkon.Sts.Step := BALKON_CZEKANIE;
			END_IF
					
			(********************************************)
			(*				ERROR - Stan bledu			*)
			(********************************************)
		BALKON_ERROR:
			//Wylaczenie wszystkich naped�w
			gDATA.Balkon.MT7_04Balkon.Command.forward := FALSE;
			gDATA.Balkon.MT7_04Balkon.Command.reverse := FALSE;
			
			gDATA.Balkon.MT8_05Slupy.Command.forward := FALSE;
			gDATA.Balkon.MT8_05Slupy.Command.reverse := FALSE;
			
			//wyjscie z bledu 
			IF NOT gBalkon.Sts.Error THEN 
				//przejscie do opowiedniego kroku
				gBalkon.Sts.Step := BALKON_CZEKANIE;
			END_IF
			
			IF gBalkon.Cmd.ErrorReset THEN
				gDATA.Balkon.MT7_04Balkon.Command.resetFault := TRUE;
				gDATA.Balkon.MT8_05Slupy.Command.resetFault := TRUE;
				gDATA.Balkon.Z4_Silownik_Warstwa.CS.Reset := TRUE;
				gDATA.Balkon.Z5_Silownik_Zderzak.CS.Reset := TRUE;
				
				// Reset bledu od balkonu opuszczonego za nisko. Resetujemy go bez sprawdzania czujnika, aby umozliwic manualne podniesienie balkonu.
				// Jesli operator bedzie chcial wlaczyc tryb automatyczny bez podniesienia balkonu to blad wystapi ponownie
				alarm_BalkonZaNisko := FALSE;
				
				//procedura resetu error�w 
				gBalkon.Sts.Error := FALSE;
				gBalkon.Cmd.ErrorReset := FALSE;
			END_IF
			
			
			(********************************************)
			(*		PO_ZATRZYMANIU - Stan mini_resetu	*)
			(********************************************)
		
		BALKON_PO_ZATRZYMANIU:
		
			IF gBalkon.Cmd.Stop THEN 
				gBalkon.Sts.Step := BALKON_ZATRZYMYWANIE;
			END_IF
			// dodane w tym miejscu, bo po wylaczeniu zasilania i wznowieniu pracy nie wchodzimy w uruchamianie
			gDATA.Balkon.MT8_05Slupy.Set.setRPM := gBalkon.Par.PredkoscSlupy.PracyPoczatkowa;
			
			gBalkon.Sts.PaletaGotowa 					:= gRetain.Balkon.PaletaGotowa;
			gBalkon.Sts.MozliweUkladanieWarstwy 		:= gRetain.Balkon.StatusUkladanieWarstwy;
			gDATA.Balkon.MT7_04Balkon.Set.setRPM 		:= gRetain.Balkon.PredkoscBalkon;
			gDATA.Balkon.MT8_05Slupy.Set.setRPM 		:= gRetain.Balkon.PredkoscSlupy;
			gDATA.Balkon.MT7_04Balkon.Command.forward 	:= gRetain.Balkon.RuchBalkonForward;
			gDATA.Balkon.MT7_04Balkon.Command.reverse 	:= gRetain.Balkon.RuchBalkonReverse;
			gDATA.Balkon.MT8_05Slupy.Command.forward 	:= gRetain.Balkon.RuchSlupyForward;
			gDATA.Balkon.MT8_05Slupy.Command.reverse 	:= gRetain.Balkon.RuchSlupyReverse;
			gDATA.Balkon.Z4_Silownik_Warstwa.CS.Control := gRetain.Balkon.Silownik_Warstwa;
			gDATA.Balkon.Z5_Silownik_Zderzak.CS.Control := gRetain.Balkon.Silownik_Zderzak;
			
			Praca_step := gRetain.Balkon.Praca_step_Balkon;
			
			gBalkon.Sts.Step := BALKON_PRACA;			
	
	END_CASE
	
	(****************Przypisywanie zmiennych do zmiennych z poprzedniego cyklu**********************)
	cmdStart_old := gBalkon.Cmd.Start;
	cmdReset_old := gBalkon.Cmd.Reset;
	
	Balkon_di_S25_PolozenieSlup1_Old := gCM.Balkon.di_S25_Polozenie_Slup_1;
	Balkon_di_S26_PolozenieSlup2_Old := gCM.Balkon.di_S26_Polozenie_Slup_2;
						
	Balkon_diS16_PozycjaBalkonu1_Old := gCM.Balkon.di_S16_PozycjaBalkonu_1;
	Balkon_diS17_PozycjaBalkonu2_Old := gCM.Balkon.di_S17_PozycjaBalkonu_2;
	Balkon_diS18_PozycjaBalkonu3_Old := gCM.Balkon.di_S18_PozycjaBalkonu_3;
	Balkon_diS19_PozycjaBalkonu4_Old := gCM.Balkon.di_S19_PozycjaBalkonu_4;						
	
	(****************Tabele Alarm�w**********************)

	IF gDATA.Balkon.MT7_04Balkon.Status.errorCode <> 0 THEN
		Alarmy_Balkon[0] := TRUE;
	ELSE
		Alarmy_Balkon[0] := FALSE;
	END_IF
	
	IF gDATA.Balkon.MT8_05Slupy.Status.errorCode <> 0 THEN
		Alarmy_Balkon[1] := TRUE;
	ELSE
		Alarmy_Balkon[1] := FALSE;
	END_IF
	
	IF gDATA.Balkon.Z4_Silownik_Warstwa.STS.ALM.InAlm THEN
		Alarmy_Balkon[2] := TRUE;
	ELSE
		Alarmy_Balkon[2] := FALSE;
	END_IF
	
	IF gDATA.Balkon.Z5_Silownik_Zderzak.STS.ALM.InAlm THEN
		Alarmy_Balkon[3] := TRUE;
	ELSE
		Alarmy_Balkon[3] := FALSE;
	END_IF
	
	IF gCM.Balkon.di_S25_Polozenie_Slup_1 THEN
		Alarmy_Balkon[4] := TRUE;
	ELSE
		Alarmy_Balkon[4] := FALSE;
	END_IF
	
	IF gCM.Balkon.di_S26_Polozenie_Slup_2 THEN
		Alarmy_Balkon[5] := TRUE;
	ELSE
		Alarmy_Balkon[5] := FALSE;
	END_IF
	
	IF alarm_BalkonZaNisko THEN
		Alarmy_Balkon[6] := TRUE;
	ELSE
		Alarmy_Balkon[6] := FALSE;
	END_IF
	
END_PROGRAM
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

