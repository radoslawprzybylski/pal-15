
PROGRAM _INIT
	//dwa rzeczywiste czujniki
	gDATA.Balkon.Z4_Silownik_Warstwa.PAR.HasCloseFB := TRUE;
	gDATA.Balkon.Z4_Silownik_Warstwa.PAR.HasOpenFB := TRUE;
//	gDATA.Balkon.Z4_Silownik_Warstwa.PAR.TimerStsClose_Preset := T#3s;
//	gDATA.Balkon.Z4_Silownik_Warstwa.PAR.TimerStsOpen_Preset := T#3s;
	gDATA.Balkon.Z4_Silownik_Warstwa.PAR.TimerAlm_Preset := T#6s;
	 
	//dwa rzeczywiste czujniki2552
	gDATA.Balkon.Z5_Silownik_Zderzak.PAR.HasCloseFB := TRUE;
	gDATA.Balkon.Z5_Silownik_Zderzak.PAR.HasOpenFB := TRUE;
//	gDATA.Balkon.Z5_Silownik_Zderzak.PAR.TimerStsClose_Preset := T#3s;
//	gDATA.Balkon.Z5_Silownik_Zderzak.PAR.TimerStsOpen_Preset := T#3s;
	gDATA.Balkon.Z5_Silownik_Zderzak.PAR.TimerAlm_Preset := T#6s;
	
	gBalkon.Par.EnkReset := 11788;
	gBalkon.Par.EnkSkalowanie := 7.19;
	
	EnkoderOffset := gRetain.Balkon.Enkoder;
END_PROGRAM

PROGRAM _CYCLIC
	gCM.Balkon.Z4_Silownik_Warstwa(DATA := gDATA.Balkon.Z4_Silownik_Warstwa);
	gCM.Balkon.Z5_Silownik_Zderzak(DATA := gDATA.Balkon.Z5_Silownik_Zderzak);
	
	// Przepisywanie odczytu enkodera do zmiennej utrzymywanej bateryjnie
	gRetain.Balkon.Enkoder := Enkoder;
	Enkoder := gCM.Balkon.en_ENK1 + EnkoderOffset;
	
	IF di_Enkoder_reset THEN
		EnkoderOffset := gBalkon.Par.EnkReset;
	END_IF
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

