
TYPE
	Balkon_typ : 	STRUCT 
		Cmd : Balkon_Cmd_typ;
		Par : Balkon_Par_typ;
		Sts : Balkon_Sts_typ;
	END_STRUCT;
	Balkon_Cmd_typ : 	STRUCT 
		Start : BOOL;
		Stop : BOOL;
		Wstrzymaj : BOOL;
		Reset : BOOL;
		ErrorReset : BOOL;
		KoniecPalety : BOOL;
	END_STRUCT;
	Balkon_Par_typ : 	STRUCT 
		EnkPozycja : EnkPozycja_typ;
		EnkSkalowanie : REAL;
		EnkReset : DINT;
		PredkoscSlupy : PredkoscSlupy_typ;
		PredkoscBalkon : PredkoscBalkon_typ;
	END_STRUCT;
	Balkon_Sts_typ : 	STRUCT 
		Step : BalkonStep_enum;
		Gotowy : BOOL;
		Error : BOOL;
		Zresetowano : BOOL;
		PaletaGotowa : BOOL;
		MozliweUkladanieWarstwy : BOOL;
		PoZatrzymaniu : BOOL;
	END_STRUCT;
	BalkonStep_enum : 
		(
		BALKON_CZEKANIE := 0,
		BALKON_URUCHAMIANIE := 1,
		BALKON_PRACA := 2,
		BALKON_WSTRZYMYWANIE := 3,
		BALKON_WSTRZYMANY := 4,
		BALKON_ZATRZYMYWANIE := 5,
		BALKON_ZATRZYMANY := 6,
		BALKON_RESETOWANIE := 7,
		BALKON_ERROR := 8,
		BALKON_PO_ZATRZYMANIU := 9
		);
	PredkoscSlupy_typ : 	STRUCT 
		ResetStop : INT;
		ResetStart : INT;
		PracyDojazd : INT;
		PracyPoczatkowa : INT;
		PracyOdjazdDol : INT;
	END_STRUCT;
	PredkoscBalkon_typ : 	STRUCT 
		PrzodPoczatkowa : INT;
		PrzodKoncowa : INT;
		PowrotPoczatkowa : INT;
		PowrotKoncowa : INT;
		ResetDojazd : INT;
		Reset : INT;
	END_STRUCT;
	EnkPozycja_typ : 	STRUCT 
		UkladanieWarstwy : DINT;
		WysokosciWarstw : ARRAY[0..20]OF DINT;
		WyjazdPalety : DINT;
		PierwszejWarstwy : DINT;
	END_STRUCT;
END_TYPE
