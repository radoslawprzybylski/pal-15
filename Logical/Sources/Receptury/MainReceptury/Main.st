
PROGRAM _INIT
	(* Insert code here *)
	Receptura.LiczbaZgrzewekWarstwa := 9;
	gBalkon.Par.EnkSkalowanie := 7.04;
	
	WysokoscUkladania := 380;
	WysokoscWyjazdu := 1600;
	Receptura.LiczbaWarstw := 8;
	WysokosciWarstw[1] := 315;
	WysokoscPierwszejWarstwy := 145;
	
	gRetain.Produkty.ListaProduktow[0] := 'Puszki 165 mm';
	gRetain.Produkty.WysokosciProduktow[0] := 172;
	
	
	TON_STS_Warstwa_Gotowa.PT := T#2s;
	
	
END_PROGRAM

PROGRAM _CYCLIC
	
	// Zamiana wysokosci warstw w mm na impulsy enkodera
	FOR i := 0 TO Receptura.LiczbaWarstw DO
		gBalkon.Par.EnkPozycja.WysokosciWarstw[i] := REAL_TO_DINT(WysokosciWarstw[i] * gBalkon.Par.EnkSkalowanie) ;
		gBalkon.Par.EnkPozycja.UkladanieWarstwy := REAL_TO_DINT(WysokoscUkladania * gBalkon.Par.EnkSkalowanie) ;
		gBalkon.Par.EnkPozycja.WyjazdPalety := REAL_TO_DINT(WysokoscWyjazdu * gBalkon.Par.EnkSkalowanie) ;
		gBalkon.Par.EnkPozycja.PierwszejWarstwy := REAL_TO_DINT(WysokoscPierwszejWarstwy * gBalkon.Par.EnkSkalowanie) ;
	END_FOR;
	
	IF ZmianaProduktu THEN
		FOR i := 0 TO 13 DO
			IF i = 0 THEN
				WysokosciWarstw[i] := WysokoscPierwszejWarstwy;
			ELSIF (i <= Receptura.LiczbaWarstw - 1) THEN
				WysokosciWarstw[i] := WysokosciWarstw[i-1] + gRetain.Produkty.WysokosciProduktow[ID_Produkt];
			ELSE
				WysokosciWarstw[i] := 0;
			END_IF
		END_FOR
	END_IF
	
	IF Licznik_Warstw MOD 2 = 0 THEN
		Warstwa_Prawa := TRUE;
	END_IF
	
	IF Licznik_Warstw MOD 2 <> 0 THEN
		Warstwa_Prawa := FALSE;
	END_IF		
	
	
	IF  Warstwa_Prawa = TRUE THEN
		
		Receptura_Obrot_Warstwa_Prawa := TRUE;
			Receptura_Obrot_Warstwa_Lewa := FALSE;
		END_IF
		
	//IF  Warstwa_Prawa = TRUE AND (Licznik_Zgrzewek = 1 OR Licznik_Zgrzewek =2 OR Licznik_Zgrzewek =3 OR Licznik_Zgrzewek =6 OR Licznik_Zgrzewek =7 OR Licznik_Zgrzewek =8) THEN
		
	//	Receptura_Obrot_Warstwa_Prawa := FALSE;
		
	
	
    IF  Warstwa_Prawa = FALSE THEN
		
		Receptura_Obrot_Warstwa_Lewa := TRUE;
		Receptura_Obrot_Warstwa_Prawa := FALSE;
    END_IF
	
//	IF  Warstwa_Prawa = FALSE //AND (Licznik_Zgrzewek = 0 OR Licznik_Zgrzewek =1 OR Licznik_Zgrzewek =2 OR Licznik_Zgrzewek =5 OR Licznik_Zgrzewek =6 OR Licznik_Zgrzewek =7 )THEN
		
	//	Receptura_Obrot_Warstwa_Lewa := FALSE;
		
//	END_IF
	
	//IF Warstwa_Prawa = TRUE AND Licznik_Zgrzewek = 5 THEN
		
	//	Receptura_Blokada_Warstwa_Prawa := TRUE;
		
//	END_IF
	
//	IF Warstwa_Prawa = TRUE AND Licznik_Zgrzewek <>5 THEN
		
	//	Receptura_Blokada_Warstwa_Prawa := TRUE;
		
//	END_IF
	
	//IF  Warstwa_Prawa = FALSE AND Licznik_Zgrzewek = 4 THEN
		
//		Receptura_Blokada_Warstwa_Lewa := TRUE;
		
//	END_IF
	
//	IF  Warstwa_Prawa = FALSE AND Licznik_Zgrzewek <> 4 THEN
		
	//	Receptura_Blokada_Warstwa_Lewa := FALSE;
		
//	END_IF
	
	
	
	



	
	 	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

