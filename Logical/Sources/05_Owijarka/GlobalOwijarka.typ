
TYPE
	Owijarka_typ : 	STRUCT 
		Cmd : Owijarka_Cmd_typ;
		Par : Owijarka_Par_typ;
		Sts : Owijarka_Sts_typ;
	END_STRUCT;
	Owijarka_Cmd_typ : 	STRUCT 
		Start : BOOL;
		Stop : BOOL;
		Wstrzymaj : BOOL;
		Reset : BOOL;
		ErrorReset : BOOL;
		KoniecPalety : BOOL;
	END_STRUCT;
	Owijarka_Par_typ : 	STRUCT 
		Predkosci : PredkosciOwijarka_typ;
	END_STRUCT;
	Owijarka_Sts_typ : 	STRUCT 
		Step : OwijarkaStep_enum;
		Gotowy : BOOL;
		Error : BOOL;
		Zresetowano : BOOL;
		PoZatrzymaniu : BOOL;
	END_STRUCT;
	OwijarkaStep_enum : 
		(
		OWIJARKA_CZEKANIE := 0,
		OWIJARKA_URUCHAMIANIE := 1,
		OWIJARKA_PRACA := 2,
		OWIJARKA_WSTRZYMYWANIE,
		OWIJARKA_WSTRZYMANY,
		OWIJARKA_ZATRZYMYWANIE,
		OWIJARKA_ZATRZYMANY,
		OWIJARKA_RESETOWANIE,
		OWIJARKA_ERROR,
		OWIJARKA_PO_ZATRZYMANIU
		);
	PredkosciOwijarka_typ : 	STRUCT 
		MagazynRolka : INT;
		OwijarkaRolka : INT;
	END_STRUCT;
END_TYPE
