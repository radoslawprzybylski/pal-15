(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: plnkP66ctrl
 * File: plnkP66ctrl.st
 * Author: popekw
 * Created: January 21, 2010
 ********************************************************************
 *
 * 25.01.2010	Wojciech Popek			-	dodanie obs�ugi rejestr�w z parametrami rampy (ACC,DEC)
 * 25.10.2010	Wojciech Popek			-	dodanie obs�ugi b��d�w, modyfikacja funkcji STOP(HALT)
 * 24.11.2010	Wojciech Popek			-	zmiana typ�w zmiennych steruj�cych, ujednolicenie interfejsu z falownikiem X64
 * 18.10.2012	Marcin Kotlarek			-	uzalezniedzie wykonywania operacji acyklicznych od zmiennej moduleOk falownika 
 *
 *
 * Implementation of program plnkP66ctrl
 *
 * Obs�uga falownika ACOPOSinverter P66 z interfejsem POWERLINK
 *
 * Interfejs
 * 
 * gInverter1.Command.forward				-	START - ruch do przodu
 * gInverter1.Command.reverse				-	START - ruch do ty�u
 * gInverter1.Command.stop					-	STOP
 * gInverter1.Command.quickStop				-	QUICK STOP
 * gInverter1.Command.disableVoltage		-	zdj�cie napi�cia z silnika
 * gInverter1.Command.resetFault			-	kasuj b��d
 * gInverter1.Set.setRPM					-	zadana warto�� pr�dko�ci obrotowej RPM [obr./min.]
 * gInverter1.Set.setACC					-	czas rampy przyspieszania [0.1s] (np. 50 = 5s)
 * gInverter1.Set.setDEC					-	czas rampy zwalniania [0.1s]
 * gInverter1.Actual.outRPM					-	aktualna warto�� pr�dko�ci obrotowej RPM [obr./min.]
 * gInverter1.Actual.outACC					-	aktualny ustawiony czas rampy przyspieszania [0.1s]
 * gInverter1.Actual.outDEC					-	aktualny ustawiony czas rampy hamowania [0.1s]
 * gInverter1.Status.ready					-	flaga gotowo�ci falownika do wykonywania komend
 * gInverter1.Status.strState				-	opis aktualnego stanu falownika (CiA402)
 * gInverter1.Status.acyclicStatus			-	status komunikacji niecyklicznej 
 *												BIT_0 -> status zapisu parametru "gInverter1.Set.setACC" 0=OK, 1=ERROR
 *												BIT_1 -> status zapisu parametru "gInverter1.Set.setDEC" 0=OK, 1=ERROR
 *												BIT_2 -> status odczytu parametru "gInverter1.Set.outACC" 0=OK, 1=ERROR
 *												BIT_3 -> status odczytu parametru "gInverter1.Set.outACC" 0=OK, 1=ERROR
 * gInverter1.Status.errorCode				-	numer b��du
 * gInverter1.Status.strError				-	opis b��du
 * gInverter1.Config.deviceName				-	�cie�ka dost�pu do interfejsu POWERLINK (np. "IF3")
 * gInverter1.Config.stationNo				-	numer stacji POWERLINK (node number)
 *   
 ********************************************************************)
 
PROGRAM _INIT
(*	**************************************************************************	*)
(*	INICJALIZACJA PODSTAWOWYCH ZMIENNYCH KONFIGURACYJNYCH						*)
(*	**************************************************************************	*)
(*	**************************************************************************	*)
	
	(*	inicjalizacja globalnej zmiennej steruj�cej dla falownika	*)
	invP66ctrl ACCESS ADR(gDATA.Owijarka.MT9_08RolkowyDuzy);	(*	nazwa interfejsu POWERLINK	*)
	invP66ctrl.Config.deviceName := 'IF1';	(*	np. IF3 dla X20PCU 				*)
	
	(*	numer node falownika (Powerlink Node number)	*)
	invP66ctrl.Config.stationNo := 7;
	
	(*	inicjalizacja parametr�w rampy	*)
	invP66ctrl.Set.setRPM := 1500;			(*	pr�dko�� [obr/min]				*)
	invP66ctrl.Set.setACC := 1;			(*	czas przyspieszania [0.1s]		*)
	invP66ctrl.Set.setDEC := 1;			(*	czas hamowania [0.1s]			*)
	
	(*	**************************************************************************	*)
	(*	**************************************************************************	*)

	(*	inicjalizacja blok�w funkcyjnych dost�pu do rejestr�w konfiguracyjnych rampy	*)
	(*	****************************************************************************	*)
	acyclicEnable := TRUE;	//zezwolenie na dostep niecykliczny
	eplReadACC.enable := acyclicEnable;
	eplReadACC.pDevice := ADR(invP66ctrl.Config.deviceName);
	eplReadACC.node := invP66ctrl.Config.stationNo;
	eplReadACC.index := IDX_P66_ACC;
	eplReadACC.subindex := IDX_P66_ACC_SUB;
	eplReadACC.pData := ADR(invP66ctrl.Actual.outACC);
	eplReadACC.datalen := SIZEOF(invP66ctrl.Actual.outACC);
	
	eplWriteACC.enable := acyclicEnable;
	eplWriteACC.pDevice := ADR(invP66ctrl.Config.deviceName);
	eplWriteACC.node := invP66ctrl.Config.stationNo;
	eplWriteACC.index := IDX_P66_ACC;
	eplWriteACC.subindex := IDX_P66_ACC_SUB;
	eplWriteACC.pData := ADR(invP66ctrl.Set.setACC);
	eplWriteACC.datalen := SIZEOF(invP66ctrl.Set.setACC);
	
	eplReadDEC.enable := acyclicEnable;
	eplReadDEC.pDevice := ADR(invP66ctrl.Config.deviceName);
	eplReadDEC.node := invP66ctrl.Config.stationNo;
	eplReadDEC.index := IDX_P66_ACC;
	eplReadDEC.subindex := IDX_P66_DEC_SUB;
	eplReadDEC.pData := ADR(invP66ctrl.Actual.outDEC);
	eplReadDEC.datalen := SIZEOF(invP66ctrl.Actual.outDEC);
	
	eplWriteDEC.enable := acyclicEnable;
	eplWriteDEC.pDevice := ADR(invP66ctrl.Config.deviceName);
	eplWriteDEC.node := invP66ctrl.Config.stationNo;
	eplWriteDEC.index := IDX_P66_ACC;
	eplWriteDEC.subindex := IDX_P66_DEC_SUB;
	eplWriteDEC.pData := ADR(invP66ctrl.Set.setDEC);
	eplWriteDEC.datalen := SIZEOF(invP66ctrl.Set.setDEC);
	(*	****************************************************************************	*)



END_PROGRAM


PROGRAM _CYCLIC
	
	
	(*	ustawienie maski statusu falownika i statusu b��du falownika	*)
	invStat := ioP66.statusWord AND MSK_STAT;
	errStat := ioP66.statusWord AND MSK_ERR;
	
	(*	przej�cie do stanu b��du	*)
	IF (errStat = P66_STAT_ERROR) OR (ioP66.moduleOK = FALSE) THEN
		invP66ctrl.Status.errorCode := ioP66.errorCode;
		step := P66_FAULT;
		VC4ShowError := 0;
	ELSE
		invP66ctrl.Status.errorCode := 0;
		invP66ctrl.Status.strError := '';
		VC4ShowError := 1;
	END_IF

	(* ************************************************************************************	*)
	(* program steruj�cy zgodnie z diagramem stan�w CiA402 									*)

	CASE step OF
		(*	falownik wy��czony	*)
		P66_SWON_DISABLED :
			IF invStat = P66_STAT_SWON_DIS THEN
				ioP66.commandWord := CMD_SHUTDOWN;
				step := P66_RDY_SWON;
			END_IF
			(*	oczekiwanie na gotowo�� falownika	*)
		P66_RDY_SWON :			
			IF invStat = P66_STAT_RDY_SWON THEN
				ioP66.commandWord := CMD_SWON;
				step := P66_SWON;
			END_IF
			(*	falownik w��czony - oczekiwanie na polecenie	*)
		P66_SWON :	
			IF invP66ctrl.Command.forward THEN
				//	invP66ctrl.Command.forward := FALSE;
				ioP66.commandWord := CMD_FORWARD;
			ELSIF invP66ctrl.Command.reverse THEN
				//	invP66ctrl.Command.reverse := FALSE;
				ioP66.commandWord := CMD_REVERSE;
			ELSE
				ioP66.commandWord := ioP66.commandWord OR CMD_STOP;	//set HALT bit for STOP
			END_IF
			
			IF invP66ctrl.Command.quickStop THEN
				invP66ctrl.Command.quickStop := FALSE;
				ioP66.commandWord := CMD_QSTOP;
				step := P66_QSTOP;
			ELSIF invP66ctrl.Command.disableVoltage THEN
				ioP66.commandWord := CMD_DSBL_VOLT;
				step := P66_DISABLE_VOLTAGE;
			END_IF
			IF invStat = P66_STAT_SWON_DIS THEN
				step := P66_SWON_DISABLED;
			END_IF;
			(*	szybkie zatrzymanie	*)
		P66_QSTOP:
			IF invP66ctrl.Command.disableVoltage THEN
				invP66ctrl.Command.disableVoltage := FALSE;
				ioP66.commandWord := CMD_DSBL_VOLT;
				step := P66_SWON_DISABLED;
			END_IF
			(*	od��czenie zasilania silnika	*)
		P66_DISABLE_VOLTAGE:
			IF invP66ctrl.Command.disableVoltage = FALSE THEN
				step := P66_SWON_DISABLED;
			END_IF
			(*	falownik w stanie b��du	*)
		P66_FAULT:
			IF invP66ctrl.Command.resetFault THEN
				invP66ctrl.Command.resetFault := FALSE;
				ioP66.commandWord := CMD_RESET;
			END_IF
			IF invStat = P66_STAT_SWON_DIS THEN
				step := P66_SWON_DISABLED;
			END_IF;
	
		ELSE
	
	END_CASE;
	invP66ctrl.Command.resetFault := FALSE;
	(* ********************************************************************************** *)
	(* zapisanie stanu falownika do zmiennej tekstowej statusu *)
	CASE invStat OF
		P66_STAT_SWON_DIS:			invP66ctrl.Status.strState := 'State: Switch On Disabled'; 
		P66_STAT_RDY_SWON:			invP66ctrl.Status.strState := 'State: Ready To Switch On';
		P66_STAT_SWITCHED_ON:		invP66ctrl.Status.strState := 'State: Switched On';
		P66_STAT_OPERATION_ENABLE:	invP66ctrl.Status.strState := 'State: Operation Enabled';
		P66_STAT_QSTOP_ACTIVE:		invP66ctrl.Status.strState := 'State: Quick Stop Active';
		P66_STAT_QSTOP_ERROR:		invP66ctrl.Status.strState := 'State: Fault';
		P66_STAT_ERROR:				invP66ctrl.Status.strState := 'State: Fault';
	END_CASE;
	
	(* ********************************************************************************** *)
	(*	aktualizacja struktury globalnej	*)
	ioP66.setRPM := invP66ctrl.Set.setRPM;
	invP66ctrl.Actual.outRPM := ioP66.outRPM;
	(*	aktualizacja statusu gotowo�ci do pracy	*)
	IF ((ioP66.moduleOK = TRUE) AND ((invStat = P66_STAT_SWITCHED_ON) OR (invStat = P66_STAT_OPERATION_ENABLE))) THEN
		invP66ctrl.Status.ready := TRUE;
	ELSE
		invP66ctrl.Status.ready := FALSE;
	END_IF;
	
	(* ********************************************************************************** *)
	(*	asynchroniczny dost�p do rejestr�w	*)
	(*	odczyt/zapis parametr�w rampy		*)
	(*	**********************************	*)
	IF ((invP66ctrl.Actual.outACC <> invP66ctrl.Set.setACC) AND (invP66ctrl.Status.ready = TRUE)) THEN
		eplWriteACC.enable := acyclicEnable;
	END_IF
	IF ((invP66ctrl.Actual.outDEC <> invP66ctrl.Set.setDEC) AND (invP66ctrl.Status.ready = TRUE)) THEN
		eplWriteDEC.enable := acyclicEnable;
	END_IF
	
	eplWriteACC();
	IF eplWriteACC.status = ERR_OK THEN
		invP66ctrl.Status.acyclicStatus := invP66ctrl.Status.acyclicStatus AND 16#FE;	(*	reset acyclic write ACC status	*)
		eplWriteACC.enable := FALSE;
	ELSIF eplWriteACC.status = ERR_FUB_ENABLE_FALSE THEN
	ELSIF eplWriteACC.status <> ERR_FUB_BUSY THEN
		invP66ctrl.Status.acyclicStatus := invP66ctrl.Status.acyclicStatus OR 16#01;	(*	set acyclic write ACC status	*)
		eplWriteACC.enable := FALSE;
	END_IF;
	eplWriteDEC();
	IF eplWriteDEC.status = ERR_OK THEN
		invP66ctrl.Status.acyclicStatus := invP66ctrl.Status.acyclicStatus AND 16#FD;	(*	reset acyclic write DEC status	*)
		eplWriteDEC.enable := FALSE;
	ELSIF eplWriteDEC.status = ERR_FUB_ENABLE_FALSE THEN
	ELSIF eplWriteDEC.status <> ERR_FUB_BUSY THEN
		invP66ctrl.Status.acyclicStatus := invP66ctrl.Status.acyclicStatus OR 16#02;	(*	set acyclic write DEC status	*)
		eplWriteDEC.enable := FALSE;
	END_IF;
	eplReadACC.enable := invP66ctrl.Status.ready;
	eplReadACC();
	IF eplReadACC.status = ERR_OK THEN
		invP66ctrl.Status.acyclicStatus := invP66ctrl.Status.acyclicStatus AND 16#FB;	(*	reset acyclic read ACC status	*)
	ELSIF eplReadACC.status = ERR_FUB_ENABLE_FALSE THEN
	ELSIF eplReadACC.status <> ERR_FUB_BUSY THEN
		invP66ctrl.Status.acyclicStatus := invP66ctrl.Status.acyclicStatus OR 16#04;	(*	set acyclic reade ACC error status	*)
	END_IF;
	eplReadDEC.enable := invP66ctrl.Status.ready;
	eplReadDEC();
	IF eplReadDEC.status = ERR_OK THEN
		invP66ctrl.Status.acyclicStatus := invP66ctrl.Status.acyclicStatus AND 16#F7;	(*	reset acyclic read DEC status	*)
	ELSIF eplReadDEC.status = ERR_FUB_ENABLE_FALSE THEN
	ELSIF eplReadDEC.status <> ERR_FUB_BUSY THEN
		invP66ctrl.Status.acyclicStatus := invP66ctrl.Status.acyclicStatus OR 16#08;	(*	set acyclic read DEC error status	*)
	END_IF;
	(*	**********************************	*)
	
	(* ********************************************************************************** *)
	(*	Error description	*)
	(*	**********************************	*)
	CASE invP66ctrl.Status.errorCode OF
		0:	invP66ctrl.Status.strError := 'noF - NO ERROR';
		1:	invP66ctrl.Status.strError := '-';
		2:	invP66ctrl.Status.strError := '[Control EEprom](EEF1)';
		3:	invP66ctrl.Status.strError := '[Incorrect config.](CFF)';
		4:	invP66ctrl.Status.strError := '[Invalid config.](CFI)';
		5:	invP66ctrl.Status.strError := '[Modbus com.](SLF1)';
		6:	invP66ctrl.Status.strError := '[internal com. link](ILF)';
		7:	invP66ctrl.Status.strError := '[Com. network](CnF)';
		8:	invP66ctrl.Status.strError := '[External flt-LI/Bit](EPF1)';
		9:	invP66ctrl.Status.strError := '[Overcurrent](OCF)';
		10:	invP66ctrl.Status.strError := '[Precharge](CrF1)';
		11:	invP66ctrl.Status.strError := '[Speed fdback loss](SPF)';
		12:	invP66ctrl.Status.strError := '[Load slipping](AnF)';
		13:	invP66ctrl.Status.strError := '[AI2 4-20 mA loss] (LFF2)';
		14:	invP66ctrl.Status.strError := '[PTC1 probe](PtF1)';
		15:	invP66ctrl.Status.strError := '[PTC1 overheat](OtF1)';
		16:	invP66ctrl.Status.strError := '[Drive overheat](OHF)';
		17:	invP66ctrl.Status.strError := '[Motor overload](OLF)';
		18:	invP66ctrl.Status.strError := '[Overbraking](ObF)';
		19:	invP66ctrl.Status.strError := '[Mains overvoltage](OSF)';
		20:	invP66ctrl.Status.strError := '[1 motor phase loss](OPF1)';
		21:	invP66ctrl.Status.strError := '[Input phase loss](PHF)';
		22:	invP66ctrl.Status.strError := '[Undervoltage](USF)';
		23:	invP66ctrl.Status.strError := '[Motor short-circuit](SCF1)';
		24:	invP66ctrl.Status.strError := '[Overspeed](SOF)';
		25:	invP66ctrl.Status.strError := '[Auto-tuning](tnF)';
		26:	invP66ctrl.Status.strError := '[Rating error](InF1)';
		27:	invP66ctrl.Status.strError := '[Incompatible PB](InF2)';
		28:	invP66ctrl.Status.strError := '[Internal serial link](InF3)(OSF)';
		29:	invP66ctrl.Status.strError := '[Internal MFG area](InF4)';
		30:	invP66ctrl.Status.strError := '[Power EEprom](EEF2)';
		31:	invP66ctrl.Status.strError := '[Impedant sh. circuit](SCF2)';
		32:	invP66ctrl.Status.strError := '[Ground short circuit](SCF3)';
		33:	invP66ctrl.Status.strError := '[3 output phase loss](OPF2)';
		34:	invP66ctrl.Status.strError := '[CANopen com.](COF)';
		35:	invP66ctrl.Status.strError := '[Brake control](bLF)';
		36:	invP66ctrl.Status.strError := '-';
		37:	invP66ctrl.Status.strError := '[Internal - hard init.](InF7)';
		38:	invP66ctrl.Status.strError := '[External fault com.](EPF2)';
		39:	invP66ctrl.Status.strError := '[Application fault](APF)';
		40:	invP66ctrl.Status.strError := '[Internal-ctrl supply](InF8)';
		41:	invP66ctrl.Status.strError := '[Brake feedback](brF)';
		42:	invP66ctrl.Status.strError := '[PowerSuite com.](SLF2)';
		43:	invP66ctrl.Status.strError := '[Encoder coupling](ECF)';
		44:	invP66ctrl.Status.strError := '[Torque/current lim](SSF)';
		45:	invP66ctrl.Status.strError := '[HMI com.](SLF3)';
		46:	invP66ctrl.Status.strError := '[Power removal](PrF)';
		47:	invP66ctrl.Status.strError := '[PTC2 probe](PtF2)';
		48:	invP66ctrl.Status.strError := '[PTC2 overheat](OtF2)';
		49:	invP66ctrl.Status.strError := '[LI6=PTC probe](PtFL)';
		50:	invP66ctrl.Status.strError := '[LI6=PTC overheat](OtFL)';
		51:	invP66ctrl.Status.strError := '[Internal- I measure](InF9)';
		52:	invP66ctrl.Status.strError := '[Internal-mains circuit](InFA)';
		53:	invP66ctrl.Status.strError := '[Internal-th. sensor](InFb)';
		54:	invP66ctrl.Status.strError := '[IGBT overheat](tJF)';
		55:	invP66ctrl.Status.strError := '[IGBT short circuit](SCF4)';
		56:	invP66ctrl.Status.strError := '[Motor short circuit](SCF5)';
		57:	invP66ctrl.Status.strError := '[Torque time-out](SrF)';
		58:	invP66ctrl.Status.strError := '[Out. contact. stuck](FCF1)';
		59:	invP66ctrl.Status.strError := '[Out. contact. open](FCF2)';
		60:	invP66ctrl.Status.strError := '[Internal-time meas.](InFC)';
		61:	invP66ctrl.Status.strError := '[AI2 input](AI2F)';
		62:	invP66ctrl.Status.strError := '[Encoder](EnF)';
		63:	invP66ctrl.Status.strError := '[Thyr. soft charge](CrF2)';
		64:	invP66ctrl.Status.strError := '[input contactor](LCF)';
		65:	invP66ctrl.Status.strError := '[DB unit sh. circuit](bUF)';
		66:	invP66ctrl.Status.strError := '-';
		67:	invP66ctrl.Status.strError := '[IGBT desaturation](HdF)';
		68:	invP66ctrl.Status.strError := '[Internal-option](InF6)';
		69:	invP66ctrl.Status.strError := '[Internal- CPU](InFE)';
		70:	invP66ctrl.Status.strError := '[DBR overload](bOF)';
		71:	invP66ctrl.Status.strError := '[AI3 4-20 mA loss](LFF3)';
		72:	invP66ctrl.Status.strError := '[AI4 4-20 mA loss](LFF4)';
		73:	invP66ctrl.Status.strError := '[Cards pairing](HCF)';
		76:	invP66ctrl.Status.strError := '[Dynamic load fault](dLF)';
		ELSE		invP66ctrl.Status.strError := 'error';
	END_CASE;
	(*	**********************************	*)


END_PROGRAM

