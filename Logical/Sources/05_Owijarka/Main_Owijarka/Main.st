
PROGRAM _INIT
	// Do ustawienia:
	gOwijarka.Par.Predkosci.OwijarkaRolka := gRetain.Predkosci.Owijarka.OwijarkaRolka; // 1400;
	gOwijarka.Par.Predkosci.MagazynRolka  := gRetain.Predkosci.Owijarka.MagazynRolka;  // 1400;
	 
END_PROGRAM

PROGRAM _CYCLIC
	(****************************************************)
	(*				SPRAWDZENIE STATUSOW				*)
	(****************************************************)
	
	IF gOwijarka.Cmd.ErrorReset AND gOwijarka.Sts.Step <> OWIJARKA_ERROR THEN
		gOwijarka.Cmd.ErrorReset := FALSE;
	END_IF
	
	(******************Status bledu**********************)
	//wywolanie bledu od napedow OWIJARKA *****************************************************************
	IF gDATA.Owijarka.MT10_09RolkowyOwijarka.Status.errorCode <> 0 OR
		gDATA.Owijarka.MT9_08RolkowyDuzy.Status.errorCode <> 0 OR	
		alarm_OwijarkaPelna
		THEN
		gOwijarka.Sts.Error := TRUE;
	END_IF
	
	//wejscie w stan bledu 
	IF gOwijarka.Sts.Error THEN 
		gOwijarka.Sts.Step := OWIJARKA_ERROR;
	END_IF
	
	// Zapisanie ostatniego stanu pracy przed bledem
	IF Praca_step <> 99 AND gOwijarka.Sts.Step = OWIJARKA_PRACA THEN
		gRetain.Owijarka.Praca_step_Owijarka 	:= Praca_step;
		gRetain.Owijarka.RuchRolkiMagazyn 		:= gDATA.Owijarka.MT9_08RolkowyDuzy.Command.forward;
		gRetain.Owijarka.RuchRolkiOwijarka 		:= gDATA.Owijarka.MT10_09RolkowyOwijarka.Command.reverse;
	END_IF

	(****************Status gotowosci********************)
	
	gOwijarka.Sts.Gotowy := NOT gOwijarka.Sts.Error;

	(****************Reset statusow**********************)
	IF (gOwijarka.Cmd.Reset > cmdReset_old) OR (gOwijarka.Sts.Step = OWIJARKA_PRACA) THEN
		gOwijarka.Sts.Zresetowano := FALSE;
	END_IF
	
	(****************************************************)
	(*					MASZYNA STANOW					*)
	(****************************************************)
	CASE gOwijarka.Sts.Step OF
		(********************************************)
		(*			WAIT - Czekaj na komendy		*)
		(********************************************)
		OWIJARKA_CZEKANIE:
			//Przejscie do procedury startu, pierwszys start zaczunamy od resetu
			IF (gOwijarka.Cmd.Reset AND NOT gOwijarka.Sts.Zresetowano) THEN
				gOwijarka.Sts.Step := OWIJARKA_RESETOWANIE;
			ELSIF gOwijarka.Cmd.Start > cmdStart_old AND gOwijarka.Sts.PoZatrzymaniu THEN
				gOwijarka.Sts.Step := OWIJARKA_PO_ZATRZYMANIU;
			ELSIF (gOwijarka.Cmd.Start > cmdStart_old AND NOT(gOwijarka.Sts.PoZatrzymaniu)) THEN
				gOwijarka.Sts.Step := OWIJARKA_PO_ZATRZYMANIU;
			END_IF
			
			Reset_step := 0;
			
			//Nacisniecie Stopu
			IF gOwijarka.Cmd.Stop THEN 
				gOwijarka.Sts.Step := OWIJARKA_ZATRZYMYWANIE;
			END_IF
			
			//Nacisniecie przycisku wyjazdu palety
			IF gCM.Owijarka.di_S40_Przycisk_Paleta > di_S40_Przycisk_Paleta_Old THEN
				gDATA.Owijarka.MT10_09RolkowyOwijarka.Set.setRPM := gOwijarka.Par.Predkosci.OwijarkaRolka;			
				gDATA.Owijarka.MT9_08RolkowyDuzy.Set.setRPM := gOwijarka.Par.Predkosci.MagazynRolka;
				gDATA.Owijarka.MT9_08RolkowyDuzy.Command.forward := TRUE;
				gDATA.Owijarka.MT10_09RolkowyOwijarka.Command.reverse := TRUE;
			ELSIF gCM.Owijarka.di_S40_Przycisk_Paleta < di_S40_Przycisk_Paleta_Old THEN
				gDATA.Owijarka.MT9_08RolkowyDuzy.Command.forward := FALSE;
				gDATA.Owijarka.MT10_09RolkowyOwijarka.Command.reverse := FALSE;
			END_IF
			
			(********************************************)
			(*			 STARTING - Stan startu			*)
			(********************************************)
		OWIJARKA_URUCHAMIANIE:
			//zatrzymanie maszyny 
			IF NOT gOwijarka.Cmd.Start OR gOwijarka.Cmd.Stop THEN
				gOwijarka.Sts.Step := OWIJARKA_ZATRZYMYWANIE;
			END_IF
			
			IF gOwijarka.Cmd.Wstrzymaj THEN
				gOwijarka.Sts.Step := OWIJARKA_WSTRZYMYWANIE;
			END_IF

			gDATA.Owijarka.MT10_09RolkowyOwijarka.Set.setRPM := gOwijarka.Par.Predkosci.OwijarkaRolka;			
			gDATA.Owijarka.MT9_08RolkowyDuzy.Set.setRPM := gOwijarka.Par.Predkosci.MagazynRolka;
			
			gOwijarka.Sts.Step := OWIJARKA_PRACA;
			
			(********************************************)
			(*			   RUN - Stan pracy				*)
			(********************************************)
		OWIJARKA_PRACA:
			gOwijarka.Sts.PoZatrzymaniu := FALSE;
			
			//zatrzymanie maszyny 
			IF NOT gOwijarka.Cmd.Start OR gOwijarka.Cmd.Stop THEN
				gOwijarka.Sts.Step := OWIJARKA_ZATRZYMYWANIE;
				Praca_step := 99;
			END_IF
			
			//praca procesowa	
			CASE Praca_step OF
				0:
					IF gOwijarka.Cmd.Wstrzymaj THEN
						gOwijarka.Sts.Step := OWIJARKA_WSTRZYMYWANIE;
					ELSE
						IF gBalkon.Sts.PaletaGotowa THEN
							Praca_step := 1;
						END_IF
					END_IF
				1:
					IF gCM.Owijarka.di_S38_BrakPalety_owijarka THEN
						gDATA.Owijarka.MT9_08RolkowyDuzy.Set.setRPM := gOwijarka.Par.Predkosci.OwijarkaRolka;
						gDATA.Owijarka.MT9_08RolkowyDuzy.Command.forward := TRUE;
						
						gDATA.Owijarka.MT10_09RolkowyOwijarka.Set.setRPM := gOwijarka.Par.Predkosci.OwijarkaRolka;
						gDATA.Owijarka.MT10_09RolkowyOwijarka.Command.reverse := TRUE;
						gCM.Owijarka.do_BlokadaOwijarki_przekaznik := TRUE;
						Praca_step := 3;
					ELSE
						// Alarm informujacy, ze operator nie zabral poprzedniej palety z owijarki
						alarm_OwijarkaPelna := TRUE;
					END_IF
//				2:
//					IF gCM.Owijarka.di_S37_ObecnoscPalety_owijarka THEN
//						gDATA.Owijarka.MT10_09RolkowyOwijarka.Set.setRPM := gOwijarka.Par.Predkosci.OwijarkaRolka;
//						gDATA.Owijarka.MT10_09RolkowyOwijarka.Command.reverse := TRUE;
//						Praca_step := 3;
//					END_IF
				3:
					IF gCM.Owijarka.di_S37_ObecnoscPalety_owijarka THEN
						Praca_step := 4;
					END_IF
				4:
					IF NOT(gCM.Owijarka.di_S38_BrakPalety_owijarka) THEN
						gDATA.Owijarka.MT9_08RolkowyDuzy.Command.forward := FALSE;
						gDATA.Owijarka.MT10_09RolkowyOwijarka.Command.reverse := FALSE;
						Praca_step := 5;
					END_IF
				5:
					Statystyki.Partia.Palet := Statystyki.Partia.Palet + 1;
					Statystyki.Zycie.Palet 	:= Statystyki.Zycie.Palet + 1;
					
					gCM.Owijarka.do_BlokadaOwijarki_przekaznik := FALSE;
					Praca_step := 0;	
				
				99:
				//blad
			END_CASE
		
			(********************************************)
			(*		   WSTRZYMAJ - Stan wstrzymywania	*)
			(********************************************)	
		OWIJARKA_WSTRZYMYWANIE:
			
			//kod ktory pozwoli ten modul wstrzymac
			gOwijarka.Sts.Step := OWIJARKA_WSTRZYMANY;
			
			(********************************************)
			(*		   WSTRZYMANY - Stan wstrzymania	*)
			(********************************************)	
		OWIJARKA_WSTRZYMANY:
			
			IF NOT(gOwijarka.Cmd.Wstrzymaj) THEN
				gOwijarka.Sts.Step := OWIJARKA_PRACA;
			END_IF
			
			IF gOwijarka.Cmd.Stop THEN
				gOwijarka.Sts.Step := OWIJARKA_ZATRZYMYWANIE;
			END_IF

			(********************************************)
			(*		   STOPPING - Stan zatrzymywania	*)
			(********************************************)
		OWIJARKA_ZATRZYMYWANIE:
			//Wylaczenie wszystkich naped�w
			gDATA.Owijarka.MT10_09RolkowyOwijarka.Command.forward := FALSE;
			gDATA.Owijarka.MT10_09RolkowyOwijarka.Command.reverse := FALSE;
			gDATA.Owijarka.MT9_08RolkowyDuzy.Command.forward := FALSE;
			gDATA.Owijarka.MT9_08RolkowyDuzy.Command.reverse := FALSE;
			
			//przejscie do stanu oczekiwania w zatrzymaniu 
			gOwijarka.Sts.Step := OWIJARKA_ZATRZYMANY;
			
			(********************************************)
			(*		STOPPED - Stan modul zatrzymany		*)
			(********************************************)
		OWIJARKA_ZATRZYMANY:
			//o ile komenda stop nie jest aktywna to wchodzimy w aktywne czekanie na inna komende
			IF NOT gOwijarka.Cmd.Stop THEN 
				gOwijarka.Sts.Step := OWIJARKA_CZEKANIE;
			END_IF
			
			(********************************************)
			(*		  RESETTING - Stan resetowania		*)
			(********************************************)
		OWIJARKA_RESETOWANIE:
			
			Praca_step := 0;
			
			gRetain.Owijarka.Praca_step_Owijarka := 0;
			gRetain.Owijarka.RuchRolkiMagazyn 	 := FALSE;
			gRetain.Owijarka.RuchRolkiOwijarka 	 := FALSE;
			
			gCM.Owijarka.do_BlokadaOwijarki_przekaznik := FALSE;
			
			gDATA.Owijarka.MT10_09RolkowyOwijarka.Set.setRPM := gOwijarka.Par.Predkosci.OwijarkaRolka;
			gDATA.Owijarka.MT9_08RolkowyDuzy.Set.setRPM := gOwijarka.Par.Predkosci.MagazynRolka;
			
			//jezeli z sukcesem wykonae powyzsze to Zresetowano := TRUE;
			gOwijarka.Sts.Zresetowano := TRUE;
			
			//Stop lub zdjecie Startu
			IF gOwijarka.Cmd.Stop THEN 
				gOwijarka.Sts.Step := OWIJARKA_ZATRZYMYWANIE;
			END_IF
			
			//Warunek akcji konczacej, czy start czy reset
			IF gOwijarka.Sts.Zresetowano AND gOwijarka.Cmd.Start THEN 
				gOwijarka.Sts.Step := OWIJARKA_URUCHAMIANIE;
			ELSIF gOwijarka.Sts.Zresetowano THEN
				gOwijarka.Sts.Step := OWIJARKA_CZEKANIE;
			END_IF
					
			(********************************************)
			(*				ERROR - Stan bledu			*)
			(********************************************)
		OWIJARKA_ERROR:
			//Wylaczenie wszystkich naped�w
			gDATA.Owijarka.MT10_09RolkowyOwijarka.Command.forward := FALSE;
			gDATA.Owijarka.MT10_09RolkowyOwijarka.Command.reverse := FALSE;
			gDATA.Owijarka.MT9_08RolkowyDuzy.Command.forward := FALSE;
			gDATA.Owijarka.MT9_08RolkowyDuzy.Command.reverse := FALSE;
			
			//wyjscie z bledu 
			IF NOT gOwijarka.Sts.Error THEN 
				//przejscie do opowiedniego kroku
				gOwijarka.Sts.Step := OWIJARKA_CZEKANIE;
			END_IF
			
			IF gOwijarka.Cmd.ErrorReset THEN
				
				gDATA.Owijarka.MT10_09RolkowyOwijarka.Command.resetFault := TRUE;
				gDATA.Owijarka.MT9_08RolkowyDuzy.Command.resetFault := TRUE;
				alarm_OwijarkaPelna := FALSE;
				
				//procedura resetu error�w 
				gOwijarka.Sts.Error := FALSE;
				gOwijarka.Cmd.ErrorReset := FALSE;
			END_IF
			
			
			(********************************************)
			(*		PO_ZATRZYMANIU - Stan mini_resetu	*)
			(********************************************)
		
		OWIJARKA_PO_ZATRZYMANIU:
		
			IF gOwijarka.Cmd.Stop THEN 
				gOwijarka.Sts.Step := OWIJARKA_ZATRZYMYWANIE;
			END_IF
		
			gDATA.Owijarka.MT9_08RolkowyDuzy.Command.forward 		:= gRetain.Owijarka.RuchRolkiMagazyn;
			gDATA.Owijarka.MT10_09RolkowyOwijarka.Command.reverse 	:= gRetain.Owijarka.RuchRolkiOwijarka;
			
			Praca_step := gRetain.Owijarka.Praca_step_Owijarka;
			
			gOwijarka.Sts.Step := OWIJARKA_URUCHAMIANIE;				
	
	END_CASE
	
	(****************Przypisywanie zmiennych do zmiennych z poprzedniego cyklu**********************)
	cmdStart_old := gOwijarka.Cmd.Start;
	cmdReset_old := gOwijarka.Cmd.Reset;
	
	di_S40_Przycisk_Paleta_Old := gCM.Owijarka.di_S40_Przycisk_Paleta;
	
	(****************Tabele Alarm�w**********************)

	IF gDATA.Owijarka.MT10_09RolkowyOwijarka.Status.errorCode <> 0 THEN
		Alarmy_Owijarka[0] := TRUE;
	ELSE
		Alarmy_Owijarka[0] := FALSE;
	END_IF
	
	IF gDATA.Owijarka.MT9_08RolkowyDuzy.Status.errorCode <> 0 THEN
		Alarmy_Owijarka[1] := TRUE;
	ELSE
		Alarmy_Owijarka[1] := FALSE;
	END_IF
	
	IF alarm_OwijarkaPelna THEN
		Alarmy_Owijarka[2] := TRUE;
	ELSE
		Alarmy_Owijarka[2] := FALSE;
	END_IF	
	
END_PROGRAM
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

