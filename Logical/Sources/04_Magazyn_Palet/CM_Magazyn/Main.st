
PROGRAM _INIT
	//w przypadku pary silownikow Z6 i Z7 sa na nie trzy czujniki. S27, S28, S29
	//Trzeba sprawdzic jak to docelowo bedzie dzialac i wtedy zdecydowac czy podpiac pod konkretny bloczek CM
	
	//dwa symulowane czujniki
	gDATA.Magazyn.Z6_Z7_Silownik_MagPal.PAR.HasCloseFB := FALSE;
	gDATA.Magazyn.Z6_Z7_Silownik_MagPal.PAR.HasOpenFB := FALSE;
	gDATA.Magazyn.Z6_Z7_Silownik_MagPal.PAR.TimerStsClose_Preset := T#3s;
	gDATA.Magazyn.Z6_Z7_Silownik_MagPal.PAR.TimerStsOpen_Preset := T#3s;
	gDATA.Magazyn.Z6_Z7_Silownik_MagPal.PAR.TimerAlm_Preset := T#4s;
	 
	//dwa symulowane czujniki
	gDATA.Magazyn.Z7_Silownik_MagPal_2.PAR.HasCloseFB := FALSE;
	gDATA.Magazyn.Z7_Silownik_MagPal_2.PAR.HasOpenFB := FALSE;
	gDATA.Magazyn.Z7_Silownik_MagPal_2.PAR.TimerStsClose_Preset := T#3s;
	gDATA.Magazyn.Z7_Silownik_MagPal_2.PAR.TimerStsOpen_Preset := T#3s;
	gDATA.Magazyn.Z7_Silownik_MagPal_2.PAR.TimerAlm_Preset := T#4s;
	 
	//dwa rzeczywiste czujniki
	gDATA.Magazyn.Z8_Silownik_BlokPal.PAR.HasCloseFB := TRUE;
	gDATA.Magazyn.Z8_Silownik_BlokPal.PAR.HasOpenFB := TRUE;
//	gDATA.Magazyn.Z8_Silownik_BlokPal.PAR.TimerStsClose_Preset := T#3s;
//	gDATA.Magazyn.Z8_Silownik_BlokPal.PAR.TimerStsOpen_Preset := T#3s;
	gDATA.Magazyn.Z8_Silownik_BlokPal.PAR.TimerAlm_Preset := T#4s;
	 
	//dwa rzeczywiste czujniki
	gDATA.Magazyn.Z9_Silownik_Lancuchowy1.PAR.HasCloseFB := TRUE;
	gDATA.Magazyn.Z9_Silownik_Lancuchowy1.PAR.HasOpenFB := TRUE;
//	gDATA.Magazyn.Z9_Silownik_Lancuchowy1.PAR.TimerStsClose_Preset := T#3s;
//	gDATA.Magazyn.Z9_Silownik_Lancuchowy1.PAR.TimerStsOpen_Preset := T#3s;
	gDATA.Magazyn.Z9_Silownik_Lancuchowy1.PAR.TimerAlm_Preset := T#4s;
	 
END_PROGRAM

PROGRAM _CYCLIC
	gCM.Magazyn_Palet.Z6_Z7_Silownik_MagPal(DATA := gDATA.Magazyn.Z6_Z7_Silownik_MagPal);
	gCM.Magazyn_Palet.Z7_Silownik_MagPal_2(DATA := gDATA.Magazyn.Z7_Silownik_MagPal_2);
	gCM.Magazyn_Palet.Z8_Silownik_BlokPal(DATA := gDATA.Magazyn.Z8_Silownik_BlokPal);
	gCM.Magazyn_Palet.Z9_Silownik_Lancuchowy1(DATA := gDATA.Magazyn.Z9_Silownik_Lancuchowy1); 
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

