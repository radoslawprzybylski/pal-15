
TYPE
	Magazyn_typ : 	STRUCT 
		Cmd : Magazyn_Cmd_typ;
		Par : Magazyn_Par_typ;
		Sts : Magazyn_Sts_typ;
	END_STRUCT;
	Magazyn_Cmd_typ : 	STRUCT 
		Start : BOOL;
		Stop : BOOL;
		Wstrzymaj : BOOL;
		Reset : BOOL;
		ErrorReset : BOOL;
		KoniecPalety : BOOL;
	END_STRUCT;
	Magazyn_Par_typ : 	STRUCT 
		New_Member : USINT;
	END_STRUCT;
	Magazyn_Sts_typ : 	STRUCT 
		Step : MagazynStep_enum;
		Gotowy : BOOL;
		Error : BOOL;
		Zresetowano : BOOL;
		PaletyZaladowane : BOOL;
		NowaPaleta : BOOL;
		PaletaZabrana : BOOL;
		PoZatrzymaniu : BOOL;
	END_STRUCT;
	MagazynStep_enum : 
		(
		MAGAZYN_CZEKANIE := 0,
		MAGAZYN_URUCHAMIANIE := 1,
		MAGAZYN_PRACA := 2,
		MAGAZYN_WSTRZYMYWANIE,
		MAGAZYN_WSTRZYMANY,
		MAGAZYN_ZATRZYMYWANIE,
		MAGAZYN_ZATRZYMANY,
		MAGAZYN_RESETOWANIE,
		MAGAZYN_ERROR,
		MAGAZYN_PO_ZATRZYMANIU
		);
END_TYPE
