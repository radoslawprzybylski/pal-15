
PROGRAM _INIT
	//DO SPRAWDZENIA:
	// gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down/Up - czy komendy up i down odpowiadaja rzeczywistosci, czy nie trzeba zamienic w iomap
	// gDATA.Magazyn.Z8_Silownik_BlokPal.CS.Control := FALSE - to zamykanie palcow chwytaka

END_PROGRAM

PROGRAM _CYCLIC
	(****************************************************)
	(*				SPRAWDZENIE STATUSOW				*)
	(****************************************************)
	
	IF gMagazyn.Cmd.ErrorReset AND gMagazyn.Sts.Step <> MAGAZYN_ERROR THEN
		gMagazyn.Cmd.ErrorReset := FALSE;
	END_IF
	
	(******************Status bledu**********************)
	//wywolanie bledu od napedow MAGAZYNU *****************************************************************
	IF  gDATA.Magazyn.Z6_Z7_Silownik_MagPal.STS.ALM.InAlm   OR
		gDATA.Magazyn.Z8_Silownik_BlokPal.STS.ALM.InAlm     OR
		gDATA.Magazyn.Z9_Silownik_Lancuchowy1.STS.ALM.InAlm OR
		alarm_BrakPalety 
		THEN
		gMagazyn.Sts.Error := TRUE;
	END_IF
	
	// Zapisanie ostatniego stanu pracy przed bledem
	IF Praca_step <> 99 AND gMagazyn.Sts.Step = MAGAZYN_PRACA THEN
		gRetain.Magazyn.Praca_step_Magazyn := Praca_step;
		gRetain.Magazyn.RuchLancucha := gCM.Magazyn_Palet.K4_RuchLancucha;
		gRetain.Magazyn.RuchSilownikDown := gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down;
		gRetain.Magazyn.RuchSilownikUp := gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up;
		gRetain.Magazyn.Silownik_BlokPal := gDATA.Magazyn.Z8_Silownik_BlokPal.CS.Control;
		gRetain.Magazyn.Silownik_Lanuchowy1 := gDATA.Magazyn.Z9_Silownik_Lancuchowy1.CS.Control;
	END_IF
	
	//wejscie w stan bledu 
	IF gMagazyn.Sts.Error THEN 
		gMagazyn.Sts.Step := MAGAZYN_ERROR;
	END_IF

	(****************Status gotowosci********************)
	
	gMagazyn.Sts.Gotowy := NOT gMagazyn.Sts.Error;

	(****************Reset statusow**********************)
	IF (gMagazyn.Cmd.Reset > cmdReset_old) OR (gMagazyn.Sts.Step = MAGAZYN_PRACA) THEN
		gMagazyn.Sts.Zresetowano := FALSE;
	END_IF
	
	(****************************************************)
	(*					MASZYNA STANOW					*)
	(****************************************************)
	CASE gMagazyn.Sts.Step OF
		(********************************************)
		(*			WAIT - Czekaj na komendy		*)
		(********************************************)
		MAGAZYN_CZEKANIE:
			//Przejscie do procedury startu, pierwszys start zaczunamy od resetu
			IF (gMagazyn.Cmd.Reset AND NOT gMagazyn.Sts.Zresetowano) THEN
				gMagazyn.Sts.Step := MAGAZYN_RESETOWANIE;
			ELSIF gMagazyn.Cmd.Start > cmdStart_old AND gMagazyn.Sts.PoZatrzymaniu THEN
				gMagazyn.Sts.Step := MAGAZYN_PO_ZATRZYMANIU;
			ELSIF (gMagazyn.Cmd.Start > cmdStart_old AND NOT(gMagazyn.Sts.PoZatrzymaniu)) THEN
				gMagazyn.Sts.Step := MAGAZYN_PO_ZATRZYMANIU;
			END_IF
			
			Reset_step := 0;
			
			//Nacisniecie Stopu
			IF gMagazyn.Cmd.Stop THEN 
				gMagazyn.Sts.Step := MAGAZYN_ZATRZYMYWANIE;
			END_IF
			
			(********************************************)
			(*			 STARTING - Stan startu			*)
			(********************************************)
		MAGAZYN_URUCHAMIANIE:
			//zatrzymanie maszyny 
			IF NOT gMagazyn.Cmd.Start OR gMagazyn.Cmd.Stop THEN
				gMagazyn.Sts.Step := MAGAZYN_ZATRZYMYWANIE;
			END_IF
			
			IF gMagazyn.Cmd.Wstrzymaj THEN
				gMagazyn.Sts.Step := MAGAZYN_WSTRZYMYWANIE;
			END_IF
			
			// Przy uruchomieniu ustawiamy ten sygnal na TRUE zeby automatycznie zostala podana pierwsza paleta
			// Pozniej ten sygnal wysylany jest dopiero gdy pelna paleta przejdzie do owijarki
			
			gMagazyn.Sts.Step := MAGAZYN_PRACA;

			//uruchomienie potrzebnych rzeczy (wystawienie odpowiedniej predkosci na falowniki)
			//jezeli z sukcesem to przejdz to MAGAZYN_PRACA
			
			(********************************************)
			(*			   RUN - Stan pracy				*)
			(********************************************)
		MAGAZYN_PRACA:
			gMagazyn.Sts.PoZatrzymaniu := FALSE;
			
			//zatrzymanie maszyny 
			IF NOT gMagazyn.Cmd.Start OR gMagazyn.Cmd.Stop THEN
				gMagazyn.Sts.Step := MAGAZYN_ZATRZYMYWANIE;
				Praca_step := 99;
			END_IF
			
			CASE Praca_step OF
				
				0:
					IF gMagazyn.Cmd.Wstrzymaj THEN
						gMagazyn.Sts.Step := MAGAZYN_WSTRZYMYWANIE;
					ELSE
						Praca_step := 1;
					END_IF
					
				1:	// Kolejna paleta jest przygotowana do przekazania na podajniku lancuchowym
					// Opuszczamy magazyn na dol i otwieramy palce chwytaka
					gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := TRUE;
					gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := FALSE;
					
					IF gCM.Magazyn_Palet.di_S27_MagPal_1 THEN
						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := FALSE;
						gDATA.Magazyn.Z8_Silownik_BlokPal.CS.Control := TRUE; // (0 = Close, 1 = Open)
						Praca_step := 2;
					END_IF
				2:
					IF gMagazyn.Cmd.Wstrzymaj AND gCM.Magazyn_Palet.di_S36_BrakPalety_rolkowy THEN
						gMagazyn.Sts.Step := MAGAZYN_WSTRZYMYWANIE;
						gDATA.Magazyn.Z9_Silownik_Lancuchowy1.CS.Control := FALSE;
						
					ELSE
						IF gCM.Magazyn_Palet.di_S36_BrakPalety_rolkowy > Mag_diS36_BrakPalety_rolkowy_Old AND gBalkon.Sts.PaletaGotowa THEN
				
							// Wlaczenie lampki ostrzegajacej o wykonywaniu ruchow przez magazyn
							gMagazyn.Sts.PaletaZabrana := TRUE;
							Praca_step := 3;
							//ELSE
							// Wylaczenie lampki ostrzegajacej o wykonywaniu ruchow przez magazyn
						END_IF
					END_IF
				3:	
					// Jesli palce chwytaka sa otwarte to rozpoczynamy ruch do gory
					IF gCM.Magazyn_Palet.di_S32_ObecnoscPalety THEN
						IF gDATA.Magazyn.Z8_Silownik_BlokPal.STS.IsOpened THEN
							gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := FALSE;
							gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := TRUE;
							Praca_step := 4;
						END_IF
					ELSE
						alarm_BrakPalety := TRUE;
					END_IF
				4:
					// Jesli wjedziemy na pozycje 2 - srodkowa to zamykamy palce chwytaka
					IF gCM.Magazyn_Palet.di_S28_MagPal_2 > Magazyn_di_S28_MagPal_2_Old THEN
						gDATA.Magazyn.Z8_Silownik_BlokPal.CS.Control := FALSE; // (0 = Close, 1 = Open)
						Praca_step := 5;
					END_IF
				5:
					// Jesli palce sie zamknely i dojechalismy na gore to idziemy dalej
					IF gDATA.Magazyn.Z8_Silownik_BlokPal.STS.IsClosed AND gCM.Magazyn_Palet.di_S29_MagPal_3 THEN
						Praca_step := 6;
					END_IF
					
				6:
					// Sprawdzamy czy na przenosniku lancuchowym znajduje sie paleta, jesli tak to podnosimy przenosnik
					IF gCM.Magazyn_Palet.di_S32_ObecnoscPalety AND NOT gCM.Magazyn_Palet.di_S35_ObecnoscPalety_rolkowy  THEN
						gDATA.Magazyn.Z9_Silownik_Lancuchowy1.CS.Control := TRUE; //(0 = na gorze, 1 = na dole)
						Praca_step := 7;	
					ELSE
						alarm_BrakPalety := TRUE;
					END_IF
				7:	
					// Jesli silownik sie podniosl to zalaczamy ruch przenosnika
					IF gDATA.Magazyn.Z9_Silownik_Lancuchowy1.STS.IsOpened THEN
						gCM.Magazyn_Palet.K4_RuchLancucha := TRUE;
						Praca_step := 8;
					END_IF
				8:
					// Jesli paleta zostala wykryta na przenosniku rolkowym (reakcja na zbocze) to wylaczamy przenosnik lancuchowy i go opuszczamy
					IF gCM.Magazyn_Palet.di_S35_ObecnoscPalety_rolkowy > diS35_ObecnoscPalety_rolkowy_Old THEN
						gCM.Magazyn_Palet.K4_RuchLancucha := FALSE;
						gDATA.Magazyn.Z9_Silownik_Lancuchowy1.CS.Control := FALSE; //(0 = na gorze, 1 = na dole)
						Praca_step := 9;
					END_IF
				9:
					// Jesli przenosnik lanuchowy sie opuscil to zaczynamy proces od nowa
					// Zresetowanie statusu od zabrania palety
					IF gDATA.Magazyn.Z9_Silownik_Lancuchowy1.STS.IsClosed THEN
						gMagazyn.Sts.PaletaZabrana := FALSE;
						Praca_step := 0;
					END_IF
					
				99:
				//blad
						
			END_CASE
						
						
		
			(********************************************)
			(*		   WSTRZYMAJ - Stan wstrzymywania	*)
			(********************************************)	
		MAGAZYN_WSTRZYMYWANIE:
			
			//kod ktory pozwoli ten modul wstrzymac
			gMagazyn.Sts.Step := MAGAZYN_WSTRZYMANY;
			
			(********************************************)
			(*		   WSTRZYMANY - Stan wstrzymania	*)
			(********************************************)	
		MAGAZYN_WSTRZYMANY:
			
			IF NOT(gMagazyn.Cmd.Wstrzymaj) THEN
				gMagazyn.Sts.Step := MAGAZYN_PRACA;
			END_IF
			
			IF gMagazyn.Cmd.Stop THEN
				gMagazyn.Sts.Step := MAGAZYN_ZATRZYMYWANIE;
			END_IF


			(********************************************)
			(*		   STOPPING - Stan zatrzymywania	*)
			(********************************************)
		MAGAZYN_ZATRZYMYWANIE:
			//zatrzymywanie dzialania modulu 
			gCM.Magazyn_Palet.K4_RuchLancucha := FALSE;
			gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := FALSE;
			gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := FALSE;
			
			//przejscie do stanu oczekiwania w zatrzymaniu 
			gMagazyn.Sts.Step := MAGAZYN_ZATRZYMANY;
			
			(********************************************)
			(*		STOPPED - Stan modul zatrzymany		*)
			(********************************************)
		MAGAZYN_ZATRZYMANY:
			//o ile komenda stop nie jest aktywna to wchodzimy w aktywne czekanie na inna komende
			IF NOT gMagazyn.Cmd.Stop THEN 
				gMagazyn.Sts.Step := MAGAZYN_CZEKANIE;
			END_IF
			
			(********************************************)
			(*		  RESETTING - Stan resetowania		*)
			(********************************************)
		MAGAZYN_RESETOWANIE:
			
			Praca_step := 0;
			
			gMagazyn.Sts.PaletaZabrana := FALSE;
			
			gRetain.Magazyn.Praca_step_Magazyn := 0;
			gRetain.Magazyn.RuchLancucha := FALSE;
			gRetain.Magazyn.RuchSilownikDown := FALSE;
			gRetain.Magazyn.RuchSilownikUp := FALSE;
			gRetain.Magazyn.Silownik_Lanuchowy1 := FALSE;
			gRetain.Magazyn.Silownik_BlokPal := TRUE;
			CASE Reset_step OF
				0:
					// Sprawdzenie czy jestesmy na dolnej krancowce
					IF gCM.Magazyn_Palet.di_S27_MagPal_1 THEN
						Reset_step := 3;
						
					// jesli nie to czy jestesmy na gorze
					ELSIF gCM.Magazyn_Palet.di_S29_MagPal_3 OR gCM.Magazyn_Palet.di_S28_MagPal_2 THEN
						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := TRUE;
						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := FALSE;
						Reset_step := 2;
					// jesli nie jestesmy na zandej krancowce to jedziemy do gory zeby znalezc ktoras krancowke
					ELSE
						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := FALSE;
						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := TRUE;
						Reset_step := 1;
					END_IF
				1:
					IF gCM.Magazyn_Palet.di_S29_MagPal_3 OR gCM.Magazyn_Palet.di_S28_MagPal_2 THEN
						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := TRUE;
						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := FALSE;
						Reset_step := 2;
					ELSIF gCM.Magazyn_Palet.di_S27_MagPal_1 THEN
						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := FALSE;
						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := FALSE;
						Reset_step := 3;
					END_IF
				2:
					IF NOT gCM.Magazyn_Palet.di_S27_MagPal_1 THEN // test
						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := FALSE;
						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := FALSE;
						Reset_step := 3;
					END_IF
				3:
					// Na dole otwieramy palce chwytaka
					gDATA.Magazyn.Z8_Silownik_BlokPal.CS.Control := TRUE; // (0 = Close, 1 = Open)
					IF gDATA.Magazyn.Z8_Silownik_BlokPal.STS.IsOpened THEN
						Reset_step := 4;
					END_IF
				4:
					// Czekamy na potwierdzenie zaladowania palet
					IF gMagazyn.Sts.PaletyZaladowane THEN
						Reset_step := 5;
					END_IF
				5:
					// Jesli otwarte to rozpoczynamy ruch do gory
					IF gDATA.Magazyn.Z8_Silownik_BlokPal.STS.IsOpened THEN
						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := FALSE;
						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := TRUE;
						Reset_step := 6;
					END_IF
				6:
					// Jesli wjedziemy na pozycje 2 - srodkowa to zamykamy palce chwytaka
					IF gCM.Magazyn_Palet.di_S28_MagPal_2 > Magazyn_di_S28_MagPal_2_Old THEN
						gDATA.Magazyn.Z8_Silownik_BlokPal.CS.Control := FALSE; // (0 = Close, 1 = Open)
						Reset_step := 7;
					END_IF
				7:
					// Jesli palce sie zamknely i dojechalismy na gore to idziemy dalej
					IF gDATA.Magazyn.Z8_Silownik_BlokPal.STS.IsClosed AND gCM.Magazyn_Palet.di_S29_MagPal_3 THEN
						Reset_step := 8;
					END_IF
				8:
					// Sprawdzamy czy na przenosniku lancuchowym znajduje sie paleta, jesli tak to podnosimy przenosnik
					IF gCM.Magazyn_Palet.di_S32_ObecnoscPalety THEN
						gDATA.Magazyn.Z9_Silownik_Lancuchowy1.CS.Control := TRUE; // (0 = na gorze, 1 = na dole)
						Reset_step := 9;	
					ELSE
						alarm_BrakPalety := TRUE;
					END_IF
				9:
					// Jesli silownik sie podniosl do zalaczamy ruch przenosnika
					IF gDATA.Magazyn.Z9_Silownik_Lancuchowy1.STS.IsOpened THEN
						gCM.Magazyn_Palet.K4_RuchLancucha := TRUE;
						Reset_step := 10;
					END_IF
				10:
					// Jesli paleta zostala wykryta na przenosniku rolkowym (reakcja na zbocze) to wylaczamy przenosnik lancuchowy i go opuszczamy
					IF gCM.Magazyn_Palet.di_S35_ObecnoscPalety_rolkowy > diS35_ObecnoscPalety_rolkowy_Old THEN
						gCM.Magazyn_Palet.K4_RuchLancucha := FALSE;
						gDATA.Magazyn.Z9_Silownik_Lancuchowy1.CS.Control := FALSE; // (0 = na gorze, 1 = na dole)
						Reset_step := 11;
					END_IF	
				11:
					// Jesli przenosnik zostal opuszczony - koniec resetu. Pusta paleta znajduje sie teraz na przenosniku rolkowym.
					IF gDATA.Magazyn.Z9_Silownik_Lancuchowy1.STS.IsClosed THEN
						Reset_step := 12;
					END_IF
				12: 
					gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := TRUE;
					gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := FALSE;
					
					IF gCM.Magazyn_Palet.di_S27_MagPal_1 THEN
						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := FALSE;
						gDATA.Magazyn.Z8_Silownik_BlokPal.CS.Control := TRUE; // (0 = Close, 1 = Open)
						Reset_step := 13;
					END_IF
				13:
					gMagazyn.Sts.Zresetowano := TRUE;
				
			END_CASE

//			CASE Reset_step OF
//				0:
//					// Zamkniecie blokady/palcow chwytaka oraz opuszczenie przenosnika lancuchowego
//					gDATA.Magazyn.Z8_Silownik_BlokPal.CS.Control := FALSE; // (0 = Close, 1 = Open)
//					gDATA.Magazyn.Z9_Silownik_Lancuchowy1.CS.Control := FALSE; // (0 = na gorze, 1 = na dole)
//					IF gDATA.Magazyn.Z8_Silownik_BlokPal.STS.IsClosed AND gDATA.Magazyn.Z9_Silownik_Lancuchowy1.STS.IsClosed THEN
//						Reset_step := 1;
//					END_IF
//				1:
//					// Sprawdzamy czy jestesmy na g�rze. Jesli tak to idziemy dalej, jesli nie to wystawiamy wyjscie na silownik - jedziemy do gory.
//					IF gCM.Magazyn_Palet.di_S29_MagPal_3 THEN
//						Reset_step := 3;
//					ELSE
//						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := FALSE;
//						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := TRUE;
//						Reset_step := 2;
//					END_IF
//				2:
//					// Jesli dojechalismy na gore to idziemy dalej.
//					IF gCM.Magazyn_Palet.di_S29_MagPal_3 > Magazyn_di_S29_MagPal_3_Old THEN
//						Reset_step := 3;
//					END_IF
//				3:	
//					// Kiedy jestesmy na gorze to czekamy na potwierdzenie zaladowania palet
//					IF gMagazyn.Sts.PaletyZaladowane THEN
//						Reset_step := 4;
//					END_IF
//				4:	
//					// Jedziemy na dol i jak dojedziemy na dol to otwieramy palce chwytaka
//					gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := TRUE;
//					gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := FALSE;
//					
//					IF gCM.Magazyn_Palet.di_S27_MagPal_1 THEN
//						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := FALSE;
//						gDATA.Magazyn.Z8_Silownik_BlokPal.CS.Control := TRUE; // (0 = Close, 1 = Open)
//						Reset_step := 5;
//					END_IF
//				5:
//					// Jesli otwarte to rozpoczynamy ruch do gory
//					IF gDATA.Magazyn.Z8_Silownik_BlokPal.STS.IsOpened THEN
//						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := FALSE;
//						gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := TRUE;
//						Reset_step := 6;
//					END_IF
//				6:
//					// Jesli wjedziemy na pozycje 2 - srodkowa to zamykamy palce chwytaka
//					IF gCM.Magazyn_Palet.di_S28_MagPal_2 > Magazyn_di_S28_MagPal_2_Old THEN
//						gDATA.Magazyn.Z8_Silownik_BlokPal.CS.Control := FALSE; // (0 = Close, 1 = Open)
//						Reset_step := 7;
//					END_IF
//				7:
//					// Jesli palce sie zamknely i dojechalismy na gore to idziemy dalej
//					IF gDATA.Magazyn.Z8_Silownik_BlokPal.STS.IsClosed AND gCM.Magazyn_Palet.di_S29_MagPal_3 THEN
//						Reset_step := 8;
//					END_IF
//				8:
//					// Sprawdzamy czy na przenosniku lancuchowym znajduje sie paleta, jesli tak to podnosimy przenosnik
//					IF gCM.Magazyn_Palet.di_S32_ObecnoscPalety THEN
//						gDATA.Magazyn.Z9_Silownik_Lancuchowy1.CS.Control := TRUE; // (0 = na gorze, 1 = na dole)
//						Reset_step := 9;	
//					ELSE
//						alarm_BrakPalety := TRUE;
//					END_IF
//				9:
//					// Jesli silownik sie podniosl do zalaczamy ruch przenosnika
//					IF gDATA.Magazyn.Z9_Silownik_Lancuchowy1.STS.IsOpened THEN
//						gCM.Magazyn_Palet.K4_RuchLancucha := TRUE;
//						Reset_step := 10;
//					END_IF
//				10:
//					// Jesli paleta zostala wykryta na przenosniku rolkowym (reakcja na zbocze) to wylaczamy przenosnik lancuchowy i go opuszczamy
//					IF gCM.Magazyn_Palet.di_S35_ObecnoscPalety_rolkowy > diS35_ObecnoscPalety_rolkowy_Old THEN
//						gCM.Magazyn_Palet.K4_RuchLancucha := FALSE;
//						gDATA.Magazyn.Z9_Silownik_Lancuchowy1.CS.Control := FALSE; // (0 = na gorze, 1 = na dole)
//						Reset_step := 11;
//					END_IF	
//				11:
//					// Jesli przenosnik zostal opuszczony - koniec resetu. Pusta paleta znajduje sie teraz na przenosniku rolkowym.
//					IF gDATA.Magazyn.Z9_Silownik_Lancuchowy1.STS.IsClosed THEN
//						gMagazyn.Sts.Zresetowano := TRUE;
//						Reset_step := 0;
//					END_IF
//				
//			END_CASE
			
			//Stop lub zdjecie Startu
			IF gMagazyn.Cmd.Stop THEN 
				gMagazyn.Sts.Step := MAGAZYN_ZATRZYMYWANIE;
			END_IF
			
			//Warunek akcji konczacej, czy start czy reset
			IF gMagazyn.Sts.Zresetowano AND gMagazyn.Cmd.Start THEN 
				gMagazyn.Sts.Step := MAGAZYN_URUCHAMIANIE;
			ELSIF gMagazyn.Sts.Zresetowano THEN
				gMagazyn.Sts.Step := MAGAZYN_CZEKANIE;
			END_IF
					
			(********************************************)
			(*				ERROR - Stan bledu			*)
			(********************************************)
		MAGAZYN_ERROR:
			//Wylaczenie wszystkich naped�w
			gCM.Magazyn_Palet.K4_RuchLancucha := FALSE;
			gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := FALSE;
			gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := FALSE;
			gDATA.Magazyn.Z9_Silownik_Lancuchowy1.CS.Control := FALSE;
			//wyjscie z bledu 
			IF NOT gMagazyn.Sts.Error THEN 
				//przejscie do opowiedniego kroku
				gMagazyn.Sts.Step := MAGAZYN_CZEKANIE;
			END_IF
			
			IF gMagazyn.Cmd.ErrorReset THEN
				gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Reset := TRUE;
				gDATA.Magazyn.Z8_Silownik_BlokPal.CS.Reset := TRUE;
				gDATA.Magazyn.Z9_Silownik_Lancuchowy1.CS.Reset := TRUE;
				
				// reset alarmu od braku palety. Po wystapieniu tego bledu operator musi zrobic najpierw reset calego modulu zeby zlapac palety
				alarm_BrakPalety := FALSE;
				
				//procedura resetu error�w 
				gMagazyn.Sts.Error := FALSE;
				gMagazyn.Cmd.ErrorReset := FALSE;
			END_IF
			
			
			(********************************************)
			(*		PO_ZATRZYMANIU - Stan mini_resetu	*)
			(********************************************)
		
		MAGAZYN_PO_ZATRZYMANIU:
		
			IF gMagazyn.Cmd.Stop THEN 
				gMagazyn.Sts.Step := MAGAZYN_ZATRZYMYWANIE;
			END_IF
			
			Praca_step := gRetain.Magazyn.Praca_step_Magazyn;
			
			gCM.Magazyn_Palet.K4_RuchLancucha := gRetain.Magazyn.RuchLancucha;
			gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := gRetain.Magazyn.RuchSilownikDown;
			gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := gRetain.Magazyn.RuchSilownikUp;
			gDATA.Magazyn.Z8_Silownik_BlokPal.CS.Control := gRetain.Magazyn.Silownik_BlokPal;
			gDATA.Magazyn.Z9_Silownik_Lancuchowy1.CS.Control := gRetain.Magazyn.Silownik_Lanuchowy1;
			
			gMagazyn.Sts.Step := MAGAZYN_URUCHAMIANIE;
	
	END_CASE
	
	(****************Przypisywanie zmiennych do zmiennych z poprzedniego cyklu**********************)
	cmdStart_old := gMagazyn.Cmd.Start;
	cmdReset_old := gMagazyn.Cmd.Reset;
						
	Magazyn_di_S27_MagPal_1_Old := gCM.Magazyn_Palet.di_S27_MagPal_1;
	Magazyn_di_S28_MagPal_2_Old := gCM.Magazyn_Palet.di_S28_MagPal_2;
	Magazyn_di_S29_MagPal_3_Old := gCM.Magazyn_Palet.di_S29_MagPal_3;
	
	diS35_ObecnoscPalety_rolkowy_Old := gCM.Magazyn_Palet.di_S35_ObecnoscPalety_rolkowy;
	Mag_diS36_BrakPalety_rolkowy_Old := gCM.Magazyn_Palet.di_S36_BrakPalety_rolkowy;
						
	(****************Tabele Alarm�w**********************)
	
	IF gDATA.Magazyn.Z6_Z7_Silownik_MagPal.STS.ALM.InAlm THEN
		Alarmy_Magazyn[1] := TRUE;
	ELSE
		Alarmy_Magazyn[1] := FALSE;
	END_IF
	
	IF gDATA.Magazyn.Z8_Silownik_BlokPal.STS.ALM.InAlm THEN
		Alarmy_Magazyn[2] := TRUE;
	ELSE
		Alarmy_Magazyn[2] := FALSE;
	END_IF
	
	IF gDATA.Magazyn.Z9_Silownik_Lancuchowy1.STS.ALM.InAlm THEN
		Alarmy_Magazyn[3] := TRUE;
	ELSE
		Alarmy_Magazyn[3] := FALSE;
	END_IF

	IF alarm_BrakPalety THEN
		Alarmy_Magazyn[4] := TRUE;
	ELSE
		Alarmy_Magazyn[4] := FALSE;
	END_IF
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

