
PROGRAM _INIT
	(* Insert code here *)
	WysokoscWarstwy_1 := 145;
//	WysokoscWarstwy_2 := 310;
//	WysokoscWarstwy_3 := 450;
	
	OknoHaslo := 1;
	AktualnyPoziomDostepu := 0;
	Hasla_dostepu[0] := '';
	Hasla_dostepu[1] := '1234';
	Hasla_dostepu[2] := 'masterbr';
	
	OknoNowyProdukt := 1;
	OknoEdytujProdukt := 1;
	
	i := 0;
	
	ButtonPredkosciDomyslne := TRUE;
	ButtonCzasyDomyslne := TRUE;
	
END_PROGRAM

PROGRAM _CYCLIC
	(* Insert code here *)
	
	//resetowanie partii
	IF ZresetujLicznikPartia THEN
		Statystyki.Partia.Palet 	:= 0;
		Statystyki.Partia.Warstw 	:= 0;
		Statystyki.Partia.Zgrzewek 	:= 0;
		ZresetujLicznikPartia 		:= FALSE;
	END_IF
	
	//aktualny poziom dostepu
	IF AktualnyPoziomDostepu = 0 THEN
		ZabezpieczenieFirstLevel := 1;
		ZabezpieczenieSecondLevel := 1;
	ELSIF AktualnyPoziomDostepu = 1 THEN
		ZabezpieczenieFirstLevel := 0;
		ZabezpieczenieSecondLevel := 1;
	ELSIF AktualnyPoziomDostepu = 2 THEN
		ZabezpieczenieFirstLevel := 0;
		ZabezpieczenieSecondLevel := 0;
	END_IF
	
	(******************************************************************************************)
	(***************** PRZEPISANIE WARTOSCI DOMYSLNYCH PREDKOSCI I CZASOW *********************)
	(******************************************************************************************)
	IF ButtonPredkosciDomyslne THEN
		gRetain.Predkosci := gDomyslne.Predkosci;
		// Transport
		gTransport.Par.Predkosci := gRetain.Predkosci.Transport;
		// Balkon
		gBalkon.Par.PredkoscBalkon := gRetain.Predkosci.Balkon;
		gBalkon.Par.PredkoscSlupy := gRetain.Predkosci.Slupy;
		// Owijarka
		gOwijarka.Par.Predkosci := gRetain.Predkosci.Owijarka;
		// Jak przepisano to zresetuj przycisk
		ButtonPredkosciDomyslne := FALSE;
	END_IF
	
	IF ButtonCzasyDomyslne THEN
		gRetain.Czasy := gDomyslne.Czasy;
		gTransport.Par.Czasy := gRetain.Czasy;
		// Jak przepisano to zresetuj przycisk
		ButtonCzasyDomyslne := FALSE;
	END_IF
	
	(******************************************************************************************)
	(******************* GENERATOR PRZEBIEGU PROSTOKATNEGO NA WYJSCIE *************************)
	(******************************************************************************************)
	IF Start_gen THEN
		IF index = 1 THEN
			gDATA.Transport.Z1_Silownik_Spych1.CS.Control := Wyjscia[1];
		ELSIF index = 2 THEN
			gDATA.Transport.Z2_Silownik_Spych2.CS.Control := Wyjscia[2];
		ELSIF index = 3 THEN
			gDATA.Transport.Z3_BlokadaZgrzewki.CS.Control := Wyjscia[3];
		ELSIF index = 4 THEN
			gDATA.Balkon.Z4_Silownik_Warstwa.CS.Control := Wyjscia[4];
		ELSIF index = 5 THEN
			gDATA.Balkon.Z5_Silownik_Zderzak.CS.Control := Wyjscia[5];
		ELSIF index = 6 THEN
			gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Up := Wyjscia[6];
		ELSIF index = 7 THEN
			gDATA.Magazyn.Z6_Z7_Silownik_MagPal.CS.Down := Wyjscia[7];
		ELSIF index = 8 THEN
			gDATA.Magazyn.Z8_Silownik_BlokPal.CS.Control := Wyjscia[8];
		ELSIF index = 9 THEN	
			gDATA.Magazyn.Z9_Silownik_Lancuchowy1.CS.Control := Wyjscia[9];
		ELSIF index = 10 THEN	
			gCM.Owijarka.do_BlokadaOwijarki_przekaznik := Wyjscia[10];
		ELSIF index = 11 THEN	
			gCM.Transport.K3_SpychNaPlatforme := Wyjscia[11];
		ELSIF index = 12 THEN	
			gCM.Magazyn_Palet.K4_RuchLancucha := Wyjscia[12];
		ELSIF index = 13 THEN	
			gDATA.Transport.Z10_Obrotnica.CS.Control := Wyjscia[13];
		END_IF
	END_IF
	
	IF ResetWyjscia THEN
		gen_step := 0;
		Wyjscia[index] := FALSE;
	END_IF
	
	TP_gen();
	TP_gen.PT := DINT_TO_TIME(ZadanyCzas*1000);
	CASE gen_step OF
		0:
			Wyjscia[index] := TP_gen.Q;
			IF Start_gen THEN
				gen_step := 1;
			END_IF
		1:
			TP_gen.IN := TRUE;
			IF TP_gen.Q < TP_genQ_old THEN
				TP_gen.IN := FALSE;
				gen_step := 2;
			END_IF
			Wyjscia[index] := TP_gen.Q;
		2:
			TP_gen.IN := TRUE;
			IF TP_gen.Q < TP_genQ_old THEN
				TP_gen.IN := FALSE;
				gen_step := 0;
			END_IF
			Wyjscia[index] := NOT(TP_gen.Q);
	END_CASE
	
	LampkaWyjscia := Wyjscia[index];
	
	TP_genQ_old := TP_gen.Q;
	
	(******************************************************************************************)
	(************** PRZEPISANIE ZMIENNYCH Z WIZUALIZACJI DO TABLICY PROGRAMU ******************)
	(******************************************************************************************)
	IF ZmianaProduktu THEN
		WysokoscWarstwy_1 := WysokosciWarstw[0];
		WysokoscWarstwy_2 := WysokosciWarstw[1];
		WysokoscWarstwy_3 := WysokosciWarstw[2];
		WysokoscWarstwy_4 := WysokosciWarstw[3];
		WysokoscWarstwy_5 := WysokosciWarstw[4];
		WysokoscWarstwy_6 := WysokosciWarstw[5];
		WysokoscWarstwy_7 := WysokosciWarstw[6];
		WysokoscWarstwy_8 := WysokosciWarstw[7];
		WysokoscWarstwy_9 := WysokosciWarstw[8];
		WysokoscWarstwy_10 := WysokosciWarstw[9];
		WysokoscWarstwy_11 := WysokosciWarstw[10];
		WysokoscWarstwy_12 := WysokosciWarstw[11];
		WysokoscWarstwy_13 := WysokosciWarstw[12];
		WysokoscWarstwy_14 := WysokosciWarstw[13];
	ELSE
		WysokosciWarstw[0] := WysokoscWarstwy_1;
		WysokosciWarstw[1] := WysokoscWarstwy_2;
		WysokosciWarstw[2] := WysokoscWarstwy_3;
		WysokosciWarstw[3] := WysokoscWarstwy_4;
		WysokosciWarstw[4] := WysokoscWarstwy_5;
		WysokosciWarstw[5] := WysokoscWarstwy_6;
		WysokosciWarstw[6] := WysokoscWarstwy_7;
		WysokosciWarstw[7] := WysokoscWarstwy_8;
		WysokosciWarstw[8] := WysokoscWarstwy_9;
		WysokosciWarstw[9] := WysokoscWarstwy_10;
		WysokosciWarstw[10] := WysokoscWarstwy_11;
		WysokosciWarstw[11] := WysokoscWarstwy_12;
		WysokosciWarstw[12] := WysokoscWarstwy_13;
		WysokosciWarstw[13] := WysokoscWarstwy_14;
	END_IF
	
	IF ButtonNowyProdukt THEN
		IF gRetain.Produkty.ListaProduktow[i] = '' AND gRetain.Produkty.WysokosciProduktow[i] = 0 THEN
			gRetain.Produkty.ListaProduktow[i] := NowyProduktNazwa;
			gRetain.Produkty.WysokosciProduktow[i] := NowyProduktWysokosc;
			i := 0;
			ButtonNowyProdukt := FALSE;
		ELSE
			i := i + 1;
		END_IF
	END_IF
	
	IF ButtonEdytujProdukt THEN
		gRetain.Produkty.ListaProduktow[ID_Produkt] := NowyProduktNazwa;
		gRetain.Produkty.WysokosciProduktow[ID_Produkt] := NowyProduktWysokosc;
	END_IF
	
	IF ButtonZapiszPredkosci THEN
		gRetain.Predkosci.Balkon	:= gBalkon.Par.PredkoscBalkon;
		gRetain.Predkosci.Slupy 	:= gBalkon.Par.PredkoscSlupy;
		gRetain.Predkosci.Transport := gTransport.Par.Predkosci;
		gRetain.Predkosci.Owijarka := gOwijarka.Par.Predkosci;
		// Jak przepisano to zresetuj przycisk
		ButtonZapiszPredkosci := FALSE;
	END_IF
	
	IF ButtonZapiszCzasy THEN
		gRetain.Czasy := gTransport.Par.Czasy;
		// Jak przepisano to zresetuj przycisk
		ButtonZapiszCzasy := FALSE;
	END_IF
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

