
(* Control module: Motor Direct Controlled 1 Direction 1 Position *)
FUNCTION_BLOCK CM_MDC1D1P
	
	(********************************************)
	(*		 			CONTROL					*)
	(********************************************)
	(************SingleCycle************)
	//Switch on control if the SingleCycle command is on
	IF DATA.CS.SingleCycle > Internal.SingleCycle THEN
		DATA.STS.IsRun := TRUE;
	END_IF

	//Set status SingleCycleDone if motor is in position
	IF DATA.CS.SingleCycle AND DI_Sens > Internal.DI_Sens THEN
		//If NextCycle command is active, deactivate it
		IF DATA.CS.NextCycle THEN
			//If NextCycle command is active, deactivate it
			DATA.CS.NextCycle := FALSE;
		ELSE
			//If NextCycle command is inactive, finish single cycle (Set SingleCycleDone)
			DATA.STS.SingleCycleDone := TRUE;
		END_IF
	END_IF
	
	//Switch off control if SingleCycle command is off or single cycle is done and Control command is not on
	IF (DATA.CS.SingleCycle < Internal.SingleCycle OR DATA.STS.SingleCycleDone) AND NOT DATA.CS.Control THEN
		DATA.STS.IsRun := FALSE;
	END_IF
	//Reset status SingleCycleDone and next cycle
	IF NOT DATA.CS.SingleCycle THEN
		DATA.STS.SingleCycleDone := FALSE;
		DATA.CS.NextCycle := FALSE;
	END_IF
	(************Control************)
	//Switch on control if Control command is on
	IF DATA.CS.Control > Internal.Control THEN
		DATA.STS.IsRun := TRUE;
	END_IF
	//Switch off control if Control command is off and single cycle is not on
	IF DATA.CS.Control < Internal.Control AND NOT DATA.CS.SingleCycle THEN
		DATA.STS.IsRun := FALSE;
	END_IF
	
	(********************************************)
	(*		 		  INTERLOCK					*)
	(********************************************)
	IF DATA.CS.Interlock OR DATA.STS.ALM.InAlm THEN
		DATA.STS.IsRun := FALSE;
	END_IF
	DATA.STS.IsInterlocked := DATA.CS.Interlock;
	
	(********************************************)
	(*	 	     DIGITAL OUTPUT CONTROL			*)
	(********************************************)
	DO_Control := DATA.STS.IsRun;

	(********************************************)
	(*		 		     ALARM 					*)
	(********************************************)
	//Reset Alarm
	IF DATA.CS.Reset THEN
		DATA.CS.Reset := FALSE;
		
		DATA.STS.ALM.InAlm 					:= FALSE;
		DATA.STS.ALM.AlarmFeedback 			:= FALSE;
		DATA.STS.ALM.AlmPosSensorActive 	:= FALSE;
		DATA.STS.ALM.AlmPosSensorInActive 	:= FALSE;
	END_IF
	
	//Check the position sensor if motor is in run and has position feedback
	IF DATA.STS.IsRun AND DATA.PAR.HasPosFeedback THEN
		Internal.TimerAlm.IN := TRUE;
		//If there is edge on the position sensor deactivate timer
		IF DI_Sens <> Internal.DI_Sens THEN
			Internal.TimerAlm.IN := FALSE;
		END_IF
		//If the timer is active set the alarm (position sensor did not change in time)
		IF Internal.TimerAlm.Q THEN
			IF DI_Sens THEN
				//position sensor is constantly active
				DATA.STS.ALM.AlmPosSensorActive := TRUE;
			ELSE
				//position sensor is constantly inactive
				DATA.STS.ALM.AlmPosSensorInActive := TRUE;
			END_IF
		END_IF
	ELSE
		Internal.TimerAlm.IN := FALSE;
	END_IF
	
	//Alarm feedback - Digital input feedback alarm is active
	IF DI_Alarm THEN
		DATA.STS.ALM.AlarmFeedback := TRUE;	
	END_IF
	//In Alarm
	IF DATA.STS.ALM.AlarmFeedback OR DATA.STS.ALM.AlmPosSensorActive OR DATA.STS.ALM.AlmPosSensorInActive THEN
		DATA.STS.ALM.InAlm 	:= TRUE;
	END_IF
	
	Internal.TimerAlm(PT := DATA.PAR.TimerAlm_Preset);
	
	(********************************************)
	(*		 	MAINTENANCE COUNTER 			*)
	(********************************************)
	//Calculate the cycle time
	IF Internal.CycleTime = 0 THEN
		Internal.RTInfo_0(enable := 1);
		Internal.CycleTime := UDINT_TO_REAL(Internal.RTInfo_0.cycle_time) /(1000*1000); //Cycle time in seconds
	END_IF
	//Calculate maintenance counter
	IF DATA.STS.IsRun THEN
		MaintCounter := MaintCounter + Internal.CycleTime;
	END_IF
	
	//Reset counter
	IF DATA.CS.ResetMaintCounter THEN
		MaintCounter := 0;
	END_IF
	
	(********************************************)
	(*		 		VC4 VISUALIZATION			*)
	(********************************************)
	DATA.STS.VIS.InAlm 		:= NOT DATA.STS.ALM.InAlm;
	DATA.STS.VIS.Interlock 	:= NOT DATA.STS.IsInterlocked;
	
	//Visualization feedback status
	//Position sensor status
	IF DI_Sens THEN
		DATA.STS.VIS.PosSensorStatus := 1;
	ELSE
		DATA.STS.VIS.PosSensorStatus := 0;
	END_IF
	IF DATA.STS.ALM.AlmPosSensorActive THEN
		DATA.STS.VIS.PosSensorStatus := 2;
	ELSIF DATA.STS.ALM.AlmPosSensorInActive THEN
		DATA.STS.VIS.PosSensorStatus := 2;
	END_IF
	
	(********************************************)
	(* 					INTERNAL 				*)
	(********************************************)
	Internal.DI_Sens 		:= DI_Sens;
	Internal.SingleCycle 	:= DATA.CS.SingleCycle;
	Internal.Control 		:= DATA.CS.Control;
	
END_FUNCTION_BLOCK
