
(* Set valve feedback *)
FUNCTION_BLOCK V_FEEDBACK
	
	IF HasOpenFB AND HasCloseFB THEN
		//Valve has open and close feedback
		//Status IsOpened can be read directly from digital input DI_Open
		IsOpened := DI_Open;
		//Status IsClosed can be read directly from digital input DI_Close
		IsClosed := DI_Close;
	ELSIF NOT HasOpenFB AND HasCloseFB THEN
		//Valve does not have open feedback but has close feedback
		
		//Status IsClosed can be read directly from digital input DI_Close
		IsClosed := DI_Close;
		
		//Staus IsOpened can be read from inverse digital input DI_Close
		IF DI_Close THEN
			//If DI_Close is active reset IsOpened status
			IsOpened := FALSE;
		ELSE
			//If DI_Close is not active, wait set time and set IsOpened status
			TimerStsOpen.IN := TRUE;
			IF TimerStsOpen.Q THEN
				IsOpened := TRUE;
			END_IF
		END_IF
	ELSIF HasOpenFB AND NOT HasCloseFB THEN
		//Valve has open feedback but does not have close feedback
		//Status IsOpened can be read directly from digital input DI_Open
		IsOpened := DI_Open;
		//Staus IsClosed can be read from inverse digital input DI_Open
		IF DI_Open THEN
			//If DI_Open is active reset IsClosed status
			IsClosed := FALSE;
		ELSE
			//If DI_Open is not active, wait set time and set IsClosed status
			TimerStsClose.IN := TRUE;
			IF TimerStsClose.Q THEN
				IsClosed := TRUE;
			END_IF
		END_IF
	ELSIF NOT HasOpenFB AND NOT HasCloseFB THEN
		//Valve does not have open and close feedback 
		//Status IsOpened and IsClosed can be read from DO_Open status
		IF DO_Open THEN
			//DO_Open is active
			//Status IsOpened can be set after set time
			TimerStsOpen.IN := TRUE;
			IF TimerStsOpen.Q THEN
				IsOpened := TRUE;
			END_IF
			//Status IsClosed can be set
			IsClosed := FALSE;
		ELSE
			//Status IsOpened can be set
			IsOpened := FALSE;
			//Status IsClosed can be set after set time
			TimerStsClose.IN := TRUE;
			IF TimerStsClose.Q THEN
				IsClosed := TRUE;
			END_IF
		END_IF
	END_IF
	//Execute TimerSts FB
	TimerStsOpen(PT := TimerStsOpen_Preset);
	TimerStsClose(PT := TimerStsClose_Preset);
	//Reset TimerSts inputs
	TimerStsOpen.IN := FALSE;
	TimerStsClose.IN := FALSE;
	
END_FUNCTION_BLOCK
