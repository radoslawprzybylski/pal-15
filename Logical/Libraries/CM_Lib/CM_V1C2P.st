
(* Control module: Valve - 1 Control, 2 Positions *)
FUNCTION_BLOCK CM_V1C2P	
	(********************************************)
	(*		 		CHECK PARAMETERS			*)
	(********************************************)
	IF DATA.PAR.TimerStsClose_Preset > DATA.PAR.TimerAlm_Preset THEN
		DATA.PAR.TimerStsClose_Preset := DATA.PAR.TimerAlm_Preset;
	END_IF
	IF DATA.PAR.TimerStsOpen_Preset > DATA.PAR.TimerAlm_Preset THEN
		DATA.PAR.TimerStsOpen_Preset := DATA.PAR.TimerAlm_Preset;
	END_IF
	(********************************************)
	(*		 			CONTROL					*)
	(********************************************)
	//Output open
	IF DATA.CS.Control > Internal.Control THEN
		DO_Open := TRUE;
	ELSIF DATA.CS.Control < Internal.Control THEN
		DO_Open := FALSE;
	END_IF		
	(******Single cycle state machine******)
	//SingleCycle command reseted
	IF DATA.CS.SingleCycle < Internal.SingleCycle THEN
		Internal.SinglCycleStep := 4;
	END_IF
	CASE Internal.SinglCycleStep OF
		//WAIT Step - Wait for single cycle command
		0:
			DATA.STS.SingleCycleDone := FALSE;
			IF DATA.CS.SingleCycle THEN
				Internal.SinglCycleStep := 1;
			END_IF
		//OPEN Step - Open valve and wait for status IsOpened
		1:
			//Open valve
			DO_Open := TRUE;
			//Wait for status IsOpen
			IF DATA.STS.IsOpened THEN
				Internal.SinglCycleStep := 2;
			END_IF
		//Close Step - Close valve and wait for status IsClosed
		2:
			//Valve can be closed only if control command is not active
			IF NOT DATA.CS.Control THEN
				DO_Open := FALSE;
			END_IF
			//Wait for status IsClosed
			IF DATA.STS.IsClosed THEN
				Internal.SinglCycleStep := 3;
			END_IF
		//Done Step - Single cycle done	
		3:
			DATA.STS.SingleCycleDone := TRUE;
		//Reset Step - Close valve after reset of command SingleCycle	
		4:
			//Valve can be closed only if control command is not active
			IF NOT DATA.CS.Control THEN
				DO_Open := FALSE;
			END_IF
			Internal.SinglCycleStep := 0;
	END_CASE
	
	(********************************************)
	(*		 		  INTERLOCK					*)
	(********************************************)
	IF DATA.CS.Interlock THEN
		DO_Open := FALSE;
		//Set status IsInterlocked
		DATA.STS.IsInterlocked := TRUE;
	ELSE
		//Reset status IsInterlocked
		DATA.STS.IsInterlocked := FALSE;
	END_IF
	
	(********************************************)
	(*		 		    FEEDBACK 				*)
	(********************************************)
	Internal.Feedback.DI_Close 		:= DI_Close;
	Internal.Feedback.DI_Open 		:= DI_Open;
	Internal.Feedback.DO_Open 		:= DO_Open;
	
	Internal.Feedback.HasCloseFB 	:= DATA.PAR.HasCloseFB;
	Internal.Feedback.HasOpenFB 	:= DATA.PAR.HasOpenFB;
	
	Internal.Feedback.TimerStsClose_Preset 	:= DATA.PAR.TimerStsClose_Preset;
	Internal.Feedback.TimerStsOpen_Preset 	:= DATA.PAR.TimerStsOpen_Preset;
	
	Internal.Feedback();
	
	DATA.STS.IsClosed := Internal.Feedback.IsClosed;
	DATA.STS.IsOpened := Internal.Feedback.IsOpened;
	(********************************************)
	(*		 		     ALARM 					*)
	(********************************************)
	//Reset Alarm
	IF DATA.CS.Reset THEN
		DATA.CS.Reset := FALSE;
		memset(ADR(DATA.STS.ALM), 0, SIZEOF(DATA.STS.ALM));
	END_IF
	
	Internal.TimerAlm.IN := FALSE;
	//Alarm Open
	IF DO_Open AND NOT DATA.STS.IsOpened THEN
		Internal.TimerAlm.IN := TRUE;
		IF Internal.TimerAlm.Q THEN
			DATA.STS.ALM.Open 	:= TRUE;
		END_IF
	END_IF
	//Alarm Close
	IF NOT DO_Open AND NOT DATA.STS.IsClosed THEN
		Internal.TimerAlm.IN := TRUE;
		IF Internal.TimerAlm.Q THEN
			DATA.STS.ALM.Close := TRUE;
		END_IF
	END_IF
	//Alarm Open and Close
	IF DATA.STS.IsClosed AND DATA.STS.IsOpened THEN
		Internal.TimerAlm.IN := TRUE;
		IF Internal.TimerAlm.Q THEN
			IF DO_Open THEN
				DATA.STS.ALM.Close := TRUE;
			ELSE
				DATA.STS.ALM.Open  := TRUE;
			END_IF
		END_IF
	END_IF
	//In Alarm
	IF DATA.STS.ALM.Close OR DATA.STS.ALM.Open THEN
		DATA.STS.ALM.InAlm 	:= TRUE;
	END_IF
	//Changing control output resets the timer
	IF DO_Open <> Internal.DO_Open THEN
		Internal.TimerAlm.IN := FALSE;
	END_IF
	//Execute TimerAlm FB
	Internal.TimerAlm(PT := DATA.PAR.TimerAlm_Preset);
	
	(********************************************)
	(*		 	  MAINTENANCE COUNTER 			*)
	(********************************************)
	//Count up
	IF Internal.DO_Open > DO_Open THEN
		MaintCounter := MaintCounter + 1;
	END_IF
	//Reset counter
	IF DATA.CS.ResetMaintCounter THEN
		MaintCounter := 0;
	END_IF
		
	(********************************************)
	(*		 		VC4 VISUALIZATION			*)
	(********************************************)
	DATA.STS.VIS.InAlm 		:= NOT DATA.STS.ALM.InAlm;
	DATA.STS.VIS.Interlock 	:= NOT DATA.STS.IsInterlocked;
	
	//Visualization feedback status
	//Open feedback
	IF DATA.STS.IsOpened THEN
		DATA.STS.VIS.OpenFBStatus := 1;
	ELSE
		DATA.STS.VIS.OpenFBStatus := 0;
	END_IF
	IF DATA.STS.ALM.Open THEN
		IF DATA.STS.IsOpened THEN
			DATA.STS.VIS.OpenFBStatus := 3;
		ELSE
			DATA.STS.VIS.OpenFBStatus := 2;
		END_IF
	END_IF

	//Close feedback
	IF DATA.STS.IsClosed THEN
		DATA.STS.VIS.CloseFBStatus := 1;
	ELSE
		DATA.STS.VIS.CloseFBStatus := 0;
	END_IF
	IF DATA.STS.ALM.Close THEN
		IF DATA.STS.IsClosed THEN
			DATA.STS.VIS.CloseFBStatus := 3;
		ELSE
			DATA.STS.VIS.CloseFBStatus := 2;
		END_IF
	END_IF

	(********************************************)
	(*		 		   INTERNAL		 			*)
	(********************************************)
	Internal.DO_Open		:= DO_Open;
	Internal.Control 		:= DATA.CS.Control;
	Internal.SingleCycle 	:= DATA.CS.SingleCycle;
END_FUNCTION_BLOCK
