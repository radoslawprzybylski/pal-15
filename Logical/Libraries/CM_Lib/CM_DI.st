
(* Control module: Digital Input *)
FUNCTION_BLOCK CM_DI
	(********************************************)
	(*		 		     ALARM 					*)
	(********************************************)
	//Reset Alarm
	IF DATA.CS.Reset THEN
		DATA.CS.Reset := FALSE;
		DATA.STS.ALM.InAlm 		:= FALSE;
	END_IF
	
	TimerAlm.IN := NOT DATA.CS.IgnoreAlm AND ((DATA.PAR.AlarmCondition AND DI_Input) OR (NOT DATA.PAR.AlarmCondition AND NOT DI_Input));

	IF TimerAlm.Q THEN
		DATA.STS.ALM.InAlm := TRUE;
	END_IF
	
	TimerAlm(PT := DATA.PAR.TimerAlm_Preset);
END_FUNCTION_BLOCK
