(*UDT- User Defined Data Type*)
(************************************************************************************************************************************************************)
(*                                                                                       UDT_V2C2P*)
(************************************************************************************************************************************************************)

TYPE
	UDT_V2C2P : 	STRUCT  (*Valve 2 Controls 2 Positions*)
		CS : UDT_V2C2P_CS; (*Control Signal PLC*)
		STS : UDT_V2C2P_STS; (*Status*)
		PAR : UDT_V2C2P_PAR; (*Parameters*)
	END_STRUCT;
	UDT_V2C2P_CS : 	STRUCT  (*Control Signal PLC*)
		Control : BOOL; (*Control of the valve (0=Close, 1=Open)*)
		SingleCycle : BOOL; (*Perform single cycle (Open valve, wait for feedback open, close valve)*)
		Interlock : BOOL; (*Interlock the valve (0=No interlock, 1=Interlock)*)
		Reset : BOOL; (*Reset all pending alarms (0=No reset, 1 = Reset)*)
		ResetMaintCounter : BOOL; (*Reset Maintenance Counter*)
	END_STRUCT;
	UDT_V2C2P_STS : 	STRUCT  (*Status*)
		IsOpened : BOOL; (*Valve is opened (0=Not opened, 1= Opened)*)
		IsClosed : BOOL; (*Valve is closed (0=Not closed, 1=Closed)*)
		IsInterlocked : BOOL; (*Valve is interlocked (0=Not interlocked, 1=Interlocked)*)
		SingleCycleDone : BOOL; (*Single cycle was done*)
		ALM : UDT_V2C2P_ALM; (*Alarms*)
	END_STRUCT;
	UDT_V2C2P_ALM : 	STRUCT  (*Alarms*)
		InAlm : BOOL; (*Valve has one or more critical alarms active (0=No alarm, 1=Alarm)*)
		Open : BOOL; (*Alarm time out feedback open*)
		Close : BOOL; (*Alarm time out feedback closed*)
	END_STRUCT;
	UDT_V2C2P_PAR : 	STRUCT  (*Parameters*)
		TimerAlm_Preset : TIME := T#3s; (*Set time for alarm events*)
		TimerStsOpen_Preset : TIME := T#1s; (*Set time for setting IsOpen status (if there is no open feedback)*)
		TimerStsClose_Preset : TIME := T#1s; (*Set time for setting IsClose staus (if there is no close feedback)*)
		HasCloseFB : BOOL := TRUE; (*Configuration of the close feedback (0=no close feedback, 1=close feedback)*)
		HasOpenFB : BOOL := TRUE; (*Configuration of the open feedback (0=no open feedback, 1=open feedback)*)
	END_STRUCT;
	UDT_V2C2P_INTERNAL : 	STRUCT  (*Internal structure*)
		TimerAlm : TON; (*Alarm timer*)
		Feedback : V_FEEDBACK; (*FB that sets valve feedback*)
		DO_Open : BOOL; (*Digiral Output - Open (Memory)*)
		Control : BOOL; (*DATA.CS.Control (Memory)*)
		SingleCycle : BOOL; (*DATA.CS.SingleCycle (Memory)*)
		SinglCycleStep : USINT; (*Single cycle step*)
	END_STRUCT;
END_TYPE

(************************************************************************************************************************************************************)
(*                                                                                       UDT_V2C2P_stop*)
(************************************************************************************************************************************************************)

TYPE
	UDT_V2C2P_stop : 	STRUCT  (*Valve 2 Controls 2 Positions*)
		CS : UDT_V2C2P_stop_CS; (*Control Signal PLC*)
		STS : UDT_V2C2P_stop_STS; (*Status*)
		PAR : UDT_V2C2P_stop_PAR; (*Parameters*)
	END_STRUCT;
	UDT_V2C2P_stop_CS : 	STRUCT  (*Control Signal PLC*)
		Down : BOOL;
		Up : BOOL;
		SingleCycle : BOOL; (*Perform single cycle (Open valve, wait for feedback open, close valve)*)
		Interlock : BOOL; (*Interlock the valve (0=No interlock, 1=Interlock)*)
		Reset : BOOL; (*Reset all pending alarms (0=No reset, 1 = Reset)*)
		ResetMaintCounter : BOOL; (*Reset Maintenance Counter*)
	END_STRUCT;
	UDT_V2C2P_stop_STS : 	STRUCT  (*Status*)
		IsOpened : BOOL; (*Valve is opened (0=Not opened, 1= Opened)*)
		IsClosed : BOOL; (*Valve is closed (0=Not closed, 1=Closed)*)
		IsInterlocked : BOOL; (*Valve is interlocked (0=Not interlocked, 1=Interlocked)*)
		SingleCycleDone : BOOL; (*Single cycle was done*)
		ALM : UDT_V2C2P_stop_ALM; (*Alarms*)
	END_STRUCT;
	UDT_V2C2P_stop_ALM : 	STRUCT  (*Alarms*)
		InAlm : BOOL; (*Valve has one or more critical alarms active (0=No alarm, 1=Alarm)*)
		Open : BOOL; (*Alarm time out feedback open*)
		Close : BOOL; (*Alarm time out feedback closed*)
	END_STRUCT;
	UDT_V2C2P_stop_PAR : 	STRUCT  (*Parameters*)
		TimerAlm_Preset : TIME := T#3s; (*Set time for alarm events*)
		TimerStsOpen_Preset : TIME := T#1s; (*Set time for setting IsOpen status (if there is no open feedback)*)
		TimerStsClose_Preset : TIME := T#1s; (*Set time for setting IsClose staus (if there is no close feedback)*)
		HasCloseFB : BOOL := TRUE; (*Configuration of the close feedback (0=no close feedback, 1=close feedback)*)
		HasOpenFB : BOOL := TRUE; (*Configuration of the open feedback (0=no open feedback, 1=open feedback)*)
	END_STRUCT;
	UDT_V2C2P_stop_INTERNAL : 	STRUCT  (*Internal structure*)
		TimerAlm : TON; (*Alarm timer*)
		Feedback : V_FEEDBACK_stop; (*FB that sets valve feedback*)
		DO_Close : BOOL;
		DO_Open : BOOL; (*Digiral Output - Open (Memory)*)
		Control : BOOL; (*DATA.CS.Control (Memory)*)
		SingleCycle : BOOL; (*DATA.CS.SingleCycle (Memory)*)
		SinglCycleStep : USINT; (*Single cycle step*)
	END_STRUCT;
END_TYPE

(************************************************************************************************************************************************************)
(*                                                                                       UDT_V1C2P*)
(************************************************************************************************************************************************************)

TYPE
	UDT_V1C2P : 	STRUCT  (*Valve 1 Control 2 Positions*)
		CS : UDT_V1C2P_CS; (*Control Signal PLC*)
		STS : UDT_V1C2P_STS; (*Status*)
		PAR : UDT_V1C2P_PAR; (*Parameters*)
	END_STRUCT;
	UDT_V1C2P_CS : 	STRUCT  (*Control Signal PLC*)
		Control : BOOL; (*Control of the valve (0=Close, 1=Open)*)
		SingleCycle : BOOL; (*Perform single cycle (Open valve, wait for feedback open, close valve)*)
		Interlock : BOOL; (*Interlock the valve (0=No interlock, 1=Interlock)*)
		Reset : BOOL; (*Reset all pending alarms (0=No reset, 1 = Reset)*)
		ResetMaintCounter : BOOL; (*Reset Maintenance Counter*)
	END_STRUCT;
	UDT_V1C2P_STS : 	STRUCT  (*Status*)
		IsOpened : BOOL; (*Valve is opened (0=Not opened, 1= Opened)*)
		IsClosed : BOOL; (*Valve is closed (0=Not closed, 1=Closed)*)
		IsInterlocked : BOOL; (*Valve is interlocked (0=Not interlocked, 1=Interlocked)*)
		SingleCycleDone : BOOL; (*Single cycle was done*)
		ALM : UDT_V1C2P_ALM; (*Alarms*)
		VIS : UDT_V1C2P_VIS; (*VC4 Visualization structure*)
	END_STRUCT;
	UDT_V1C2P_ALM : 	STRUCT  (*Alarms*)
		InAlm : BOOL; (*Valve has one or more critical alarms active (0=No alarm, 1=Alarm)*)
		Open : BOOL; (*Alarm time out feedback open*)
		Close : BOOL; (*Alarm time out feedback closed*)
	END_STRUCT;
	UDT_V1C2P_VIS : 	STRUCT  (*VC4 Visualization structure*)
		Interlock : USINT; (*Shows IsInterlocked status*)
		InAlm : USINT; (*Shows In alarm status*)
		CloseFBStatus : USINT; (*Close feedback status*)
		OpenFBStatus : USINT; (*Open feedback status*)
	END_STRUCT;
	UDT_V1C2P_PAR : 	STRUCT  (*Parameters*)
		TimerAlm_Preset : TIME := T#3s; (*Positions Setpoint time out timer alarm events*)
		TimerStsOpen_Preset : TIME := T#1s; (*Set time for setting IsOpen status (if there is no open feedback)*)
		TimerStsClose_Preset : TIME := T#1s; (*Set time for setting IsClose staus (if there is no close feedback)*)
		HasCloseFB : BOOL := TRUE; (*Configuration of the close feedback (0=no close feedback, 1=close feedback)*)
		HasOpenFB : BOOL := TRUE; (*Configuration of the open feedback (0=no open feedback, 1=open feedback)*)
	END_STRUCT;
	UDT_V1C2P_INTERNAL : 	STRUCT  (*Internal structure*)
		TimerAlm : TON; (*Alarm timer*)
		Feedback : V_FEEDBACK; (*FB that sets valve feedback*)
		DO_Open : BOOL; (*Digiral Output - Open (Memory)*)
		Control : BOOL; (*DATA.CS.Control (Memory)*)
		SingleCycle : BOOL; (*DATA.CS.SingleCycle (Memory)*)
		SinglCycleStep : USINT; (*Single cycle step*)
	END_STRUCT;
END_TYPE

(************************************************************************************************************************************************************)
(*                                                                                       UDT_V1C2PnS*)
(************************************************************************************************************************************************************)

TYPE
	UDT_V1C2PnS : 	STRUCT  (*Valve 1 Control 2 Positions*)
		CS : UDT_V1C2PnS_CS; (*Control Signal PLC*)
		STS : UDT_V1C2PnS_STS; (*Status*)
		PAR : UDT_V1C2PnS_PAR; (*Parameters*)
	END_STRUCT;
	UDT_V1C2PnS_CS : 	STRUCT  (*Control Signal PLC*)
		Control : BOOL; (*Control of the valve (0=Close, 1=Open)*)
		SingleCycle : BOOL; (*Perform single cycle (Open valve, wait for feedback open, close valve)*)
		Interlock : BOOL; (*Interlock the valve (0=No interlock, 1=Interlock)*)
		Reset : BOOL; (*Reset all pending alarms (0=No reset, 1 = Reset)*)
		ResetMaintCounter : BOOL; (*Reset Maintenance Counter*)
	END_STRUCT;
	UDT_V1C2PnS_STS : 	STRUCT  (*Status*)
		IsOpened : BOOL; (*Valve is opened (0=Not opened, 1= Opened)*)
		IsClosed : BOOL; (*Valve is closed (0=Not closed, 1=Closed)*)
		IsInterlocked : BOOL; (*Valve is interlocked (0=Not interlocked, 1=Interlocked)*)
		SingleCycleDone : BOOL; (*Single cycle was done*)
		ALM : UDT_V1C2PnS_ALM; (*Alarms*)
		VIS : UDT_V1C2PnS_VIS; (*VC4 Visualization structure*)
	END_STRUCT;
	UDT_V1C2PnS_ALM : 	STRUCT  (*Alarms*)
		InAlm : BOOL; (*Valve has one or more critical alarms active (0=No alarm, 1=Alarm)*)
		Open : ARRAY[0..MAX_POS_SENSORS]OF BOOL; (*Alarm time out feedback open*)
		Close : ARRAY[0..MAX_POS_SENSORS]OF BOOL; (*Alarm time out feedback closed*)
	END_STRUCT;
	UDT_V1C2PnS_VIS : 	STRUCT  (*VC4 Visualization structure*)
		Interlock : USINT; (*Shows IsInterlocked status*)
		InAlm : USINT; (*Shows In alarm status*)
		CloseFBStatus : USINT; (*Close feedback status*)
		OpenFBStatus : USINT; (*Open feedback status*)
	END_STRUCT;
	UDT_V1C2PnS_PAR : 	STRUCT  (*Parameters*)
		TimerAlm_Preset : TIME := T#3s; (*Positions Setpoint time out timer alarm events*)
		TimerStsOpen_Preset : TIME := T#1s; (*Set time for setting IsOpen status (if there is no open feedback)*)
		TimerStsClose_Preset : TIME := T#1s; (*Set time for setting IsClose staus (if there is no close feedback)*)
		HasCloseFB : BOOL := TRUE; (*Configuration of the close feedback (0=no close feedback, 1=close feedback)*)
		HasOpenFB : BOOL := TRUE; (*Configuration of the open feedback (0=no open feedback, 1=open feedback)*)
		NumberOfPosSensors : USINT; (*Number of position sensors*)
	END_STRUCT;
	UDT_V1C2PnS_INTERNAL : 	STRUCT  (*Internal structure*)
		TimerAlm : ARRAY[0..MAX_POS_SENSORS]OF TON; (*Alarm timer*)
		Feedback : ARRAY[0..MAX_POS_SENSORS]OF V_FEEDBACK; (*FB that sets valve feedback*)
		DO_Open : BOOL; (*Digiral Output - Open (Memory)*)
		Control : BOOL; (*DATA.CS.Control (Memory)*)
		SingleCycle : BOOL; (*DATA.CS.SingleCycle (Memory)*)
		SinglCycleStep : USINT; (*Single cycle step*)
		i : USINT; (*Loop counter*)
	END_STRUCT;
END_TYPE

(************************************************************************************************************************************************************)
(*                                                                                       UDT_MDC1D1P*)
(************************************************************************************************************************************************************)

TYPE
	UDT_MDC1D1P : 	STRUCT  (*Motor Direct Controlled 1 Direction 1 Position*)
		CS : UDT_MDC1D1P_CS; (*Control Signals PLC*)
		STS : UDT_MDC1D1P_STS; (*Status*)
		PAR : UDT_MDC1D1P_PAR; (*Parameters*)
	END_STRUCT;
	UDT_MDC1D1P_CS : 	STRUCT  (*Control Signals PLC*)
		Control : BOOL; (*Control of the motor (0=Stop, 1=Start)*)
		SingleCycle : BOOL; (*Perform single cycle (run until positive edge of DI_Sens)*)
		NextCycle : BOOL; (*For single cycle command. If is TRUE single cycle is performed again without stopping the motor*)
		Interlock : BOOL; (*Interlock the motor (0=No interlock, 1=Interlock)*)
		Reset : BOOL; (*Reset all pending alarms (0=No reset, 1 = Reset)*)
		ResetMaintCounter : BOOL; (*Reset Maintenance Counter*)
	END_STRUCT;
	UDT_MDC1D1P_STS : 	STRUCT  (*Status*)
		IsRun : BOOL; (*Motor is running (0=Not running, 1= Running)*)
		IsInterlocked : BOOL; (*Motor is interlocked (0=Not interlocked, 1=Interlocked)*)
		SingleCycleDone : BOOL; (*Single cycle was done*)
		ALM : UDT_MDC1D1P_ALM; (*Alarms*)
		VIS : UDT_MDC1D1P_VIS; (*VC4 Visualization structure*)
	END_STRUCT;
	UDT_MDC1D1P_ALM : 	STRUCT  (*Alarms*)
		InAlm : BOOL; (*Motor has one or more critical alarm(s) active (0=No alarm, 1=Alarm)*)
		AlmPosSensorActive : BOOL; (*Alarm position, Motor is running and position sensor is constantly active*)
		AlmPosSensorInActive : BOOL; (*Alarm position, Motor is running and position sensor is constantly inactive*)
		AlarmFeedback : BOOL; (*Digital input feedback alarm is active*)
	END_STRUCT;
	UDT_MDC1D1P_VIS : 	STRUCT  (*VC4 Visualization structure*)
		Interlock : USINT; (*Shows IsInterlocked status*)
		InAlm : USINT; (*Shows In alarm status*)
		PosSensorStatus : USINT; (*Position sensor status*)
	END_STRUCT;
	UDT_MDC1D1P_PAR : 	STRUCT  (*Parameters*)
		HasPosFeedback : BOOL := TRUE; (*Configuration of the position feedback (0=no position feedback, 1= position feedback)*)
		TimerAlm_Preset : TIME := T#3s; (*Setpoint time out timer alarm events*)
	END_STRUCT;
	UDT_MDC1D1P_INTERNAL : 	STRUCT  (*Internal structure*)
		TimerAlm : TON; (*Alarm timer*)
		CycleTime : REAL; (*Cycle time of the task class that FB is used. Used to calculate MaintCounter*)
		RTInfo_0 : RTInfo; (*FB returns runtime information about the software object (calculate CycleTime)*)
		Control : BOOL; (*DATA.CS.Control (Memory)*)
		SingleCycle : BOOL; (*DATA.CS.SingleCycle (Memory)*)
		DI_Sens : BOOL; (*DI_Sens (Memory)*)
	END_STRUCT;
END_TYPE

(************************************************************************************************************************************************************)
(*                                                                                       UDT_MDC2D2P*)
(************************************************************************************************************************************************************)

TYPE
	UDT_MDC2D2P : 	STRUCT  (*Motor Direct Controlled 2 Direction 2 Position*)
		CS : UDT_MDC2D2P_CS; (*Control Signals PLC*)
		STS : UDT_MDC2D2P_STS; (*Status*)
		PAR : UDT_MDC2D2P_PAR; (*Parameters*)
	END_STRUCT;
	UDT_MDC2D2P_CS : 	STRUCT  (*Control Signals PLC*)
		Control : BOOL; (*Control of the motor (0=Stop, 1=Start)*)
		Move : BOOL; (*Perform single cycle (Move to positive or negative sensor)*)
		SingleCycle : BOOL; (*Perform single cycle (move to positive sensor then move back to negative sensor)*)
		NextCycle : BOOL; (*For single cycle command. If is TRUE single cycle is performed again without stopping the motor*)
		Interlock : BOOL; (*Interlock the motor (0=No interlock, 1=Interlock)*)
		Reset : BOOL; (*Reset all pending alarms (0=No reset, 1 = Reset)*)
		ResetMaintCounter : BOOL; (*Reset Maintenance Counter*)
	END_STRUCT;
	UDT_MDC2D2P_STS : 	STRUCT  (*Status*)
		IsRun : BOOL; (*Motor is running (0=Not running, 1= Running)*)
		IsInterlocked : BOOL; (*Motor is interlocked (0=Not interlocked, 1=Interlocked)*)
		SingleCycleDone : BOOL; (*Single cycle was done*)
		MoveDone : BOOL; (*Move was done*)
		ALM : UDT_MDC2D2P_ALM; (*Alarms*)
		VIS : UDT_MDC2D2P_VIS; (*VC4 Visualization structure*)
	END_STRUCT;
	UDT_MDC2D2P_ALM : 	STRUCT  (*Alarms*)
		InAlm : BOOL; (*Motor has one or more critical alarm(s) active (0=No alarm, 1=Alarm)*)
		AlmSensPosActive : BOOL; (*Alarm position, Motor is running and position sensor (direction positive) is constantly active*)
		AlmSensPosInactive : BOOL; (*Alarm position, Motor is running and position sensor (direction positive) is constantly inactive*)
		AlmSensNegActive : BOOL; (*Alarm position, Motor is running and position sensor (direction negative) is constantly active*)
		AlmSensNegInactive : BOOL; (*Alarm position, Motor is running and position sensor (direction negative) is constantly inactive*)
		AlarmFeedback : BOOL; (*Digital input feedback alarm is active*)
	END_STRUCT;
	UDT_MDC2D2P_VIS : 	STRUCT  (*VC4 Visualization structure*)
		Interlock : USINT; (*Shows IsInterlocked status*)
		InAlm : USINT; (*Shows In alarm status*)
		PosSensorStatus : USINT; (*Position sensor status - direction positive*)
		NegSensorStatus : USINT; (*Position sensor status - direction negative*)
	END_STRUCT;
	UDT_MDC2D2P_PAR : 	STRUCT  (*Parameters*)
		HasPosFeedback : BOOL := TRUE; (*Configuration of the position feedback (0=no position feedback, 1= position feedback)*)
		TimerAlm_Preset : TIME := T#3s; (*Setpoint time out timer alarm events*)
		Direction : UDT_MDC2D2P_DIR_Enum; (*Direction enumeration*)
	END_STRUCT;
	UDT_MDC2D2P_DIR_Enum : 
		( (*Direction enumeration*)
		DIR_POS, (*Direction positive*)
		DIR_NEG (*Direction negative*)
		);
	UDT_MDC2D2P_INTERNAL : 	STRUCT  (*Internal structure*)
		TimerAlm : ARRAY[0..1]OF TON; (*Alarm timers*)
		CycleTime : REAL; (*Cycle time of the task class that FB is used. Used to calculate MaintCounter*)
		RTInfo_0 : RTInfo; (*FB returns runtime information about the software object (calculate CycleTime)*)
		Control : BOOL; (*DATA.CS.Control (Memory)*)
		SingleCycle : BOOL; (*DATA.CS.SingleCycle (Memory)*)
		Move : BOOL; (*DATA.CS.Move (Memory)*)
		DI_Sens_Pos : BOOL; (*DI_Sens_Pos (Memory)*)
		DI_Sens_Neg : BOOL; (*DI_Sens_Neg (Memory)*)
		SinglCycleStep : USINT; (*Single cycle step*)
	END_STRUCT;
END_TYPE

(************************************************************************************************************************************************************)
(*                                                                                               UDT_DI*)
(************************************************************************************************************************************************************)

TYPE
	UDT_DI : 	STRUCT  (*Digital Input*)
		CS : UDT_DI_CS; (*Control Signals PLC*)
		STS : UDT_DI_STS; (*Status*)
		PAR : UDT_DI_PAR; (*Parameters*)
	END_STRUCT;
	UDT_DI_CS : 	STRUCT  (*Control Signals PLC*)
		IgnoreAlm : BOOL; (*Ignores defined alarm*)
		Reset : BOOL; (*Reset all pending alarms (0=No reset, 1 = Reset)*)
	END_STRUCT;
	UDT_DI_STS : 	STRUCT  (*Status*)
		ALM : UDT_DI_ALM; (*Alarms*)
	END_STRUCT;
	UDT_DI_ALM : 	STRUCT  (*Alarms*)
		InAlm : BOOL; (*One or more critical alarm(s) active (0=No alarm, 1=Alarm)*)
	END_STRUCT;
	UDT_DI_PAR : 	STRUCT  (*Parameters*)
		AlarmCondition : BOOL := TRUE; (*Determines Alarm condition (0=Alarm if input = False, 1=Alarm if input =True)*)
		TimerAlm_Preset : TIME := T#5s; (*Setpoint time out timer alarm events*)
	END_STRUCT;
END_TYPE

(************************************************************************************************************************************************************)
(*                                                                                               UDT_ES*)
(************************************************************************************************************************************************************)

TYPE
	UDT_ES : 	STRUCT  (*Digital Input*)
		CS : UDT_ES_CS; (*Control Signals PLC*)
		STS : UDT_ES_STS; (*Status*)
		PAR : UDT_ES_PAR; (*Parameters*)
	END_STRUCT;
	UDT_ES_CS : 	STRUCT  (*Control Signals PLC*)
		IgnoreAlm : BOOL; (*Ignores defined alarm*)
		Reset : BOOL; (*Reset all pending alarms (0=No reset, 1 = Reset)*)
	END_STRUCT;
	UDT_ES_STS : 	STRUCT  (*Status*)
		ALM : UDT_ES_ALM; (*Alarms*)
	END_STRUCT;
	UDT_ES_ALM : 	STRUCT  (*Alarms*)
		InAlm : BOOL; (*One or more critical alarm(s) active (0=No alarm, 1=Alarm)*)
		AlmMIN : BOOL; (*Alarm on MIN end switch*)
		AlmMAX : BOOL; (*Alarm on MAx end switch*)
	END_STRUCT;
	UDT_ES_PAR : 	STRUCT  (*Parameters*)
		TimerAlm_Preset : TIME := T#5s; (*Setpoint time out timer alarm events*)
	END_STRUCT;
END_TYPE
