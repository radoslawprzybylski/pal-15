
(* Control module: Valve - 1 Control, 2 Positions, n Position sensors *)
FUNCTION_BLOCK CM_V1C2PnS
	(********************************************)
	(*		 		CHECK PARAMETERS			*)
	(********************************************)
	IF DATA.PAR.TimerStsClose_Preset > DATA.PAR.TimerAlm_Preset THEN
		DATA.PAR.TimerStsClose_Preset := DATA.PAR.TimerAlm_Preset;
	END_IF
	IF DATA.PAR.TimerStsOpen_Preset > DATA.PAR.TimerAlm_Preset THEN
		DATA.PAR.TimerStsOpen_Preset := DATA.PAR.TimerAlm_Preset;
	END_IF
	IF DATA.PAR.NumberOfPosSensors + 1 > MAX_POS_SENSORS THEN
		//Number of postion sensors too high!!
		DATA.PAR.NumberOfPosSensors := MAX_POS_SENSORS + 1;
	END_IF
	
	(********************************************)
	(*		 			CONTROL					*)
	(********************************************)
	//Output open
	IF DATA.CS.Control > Internal.Control THEN
		DO_Open := TRUE;
	ELSIF DATA.CS.Control < Internal.Control THEN
		DO_Open := FALSE;
	END_IF		
	(******Single cycle state machine******)
	//SingleCycle command reseted
	IF DATA.CS.SingleCycle < Internal.SingleCycle THEN
		Internal.SinglCycleStep := 4;
	END_IF
	CASE Internal.SinglCycleStep OF
		//WAIT Step - Wait for single cycle command
		0:
			DATA.STS.SingleCycleDone := FALSE;
			IF DATA.CS.SingleCycle THEN
				Internal.SinglCycleStep := 1;
			END_IF
			//OPEN Step - Open valve and wait for status IsOpened
		1:
			//Open valve
			DO_Open := TRUE;
			//Wait for status IsOpen
			IF DATA.STS.IsOpened THEN
				Internal.SinglCycleStep := 2;
			END_IF
			//Close Step - Close valve and wait for status IsClosed
		2:
			//Valve can be closed only if control command is not active
			IF NOT DATA.CS.Control THEN
				DO_Open := FALSE;
			END_IF
			//Wait for status IsClosed
			IF DATA.STS.IsClosed THEN
				Internal.SinglCycleStep := 3;
			END_IF
		3:
			//Done Step - Single cycle done
			DATA.STS.SingleCycleDone := TRUE;
		4:
			//Reset Step - Close valve after reset of command SingleCycle
			//Valve can be closed only if control command is not active
			IF NOT DATA.CS.Control THEN
				DO_Open := FALSE;
			END_IF
			Internal.SinglCycleStep := 0;
	END_CASE
	
	(********************************************)
	(*		 		  INTERLOCK					*)
	(********************************************)
	IF DATA.CS.Interlock THEN
		DO_Open := FALSE;
		//Set status IsInterlocked
		DATA.STS.IsInterlocked := TRUE;
	ELSE
		//Reset status IsInterlocked
		DATA.STS.IsInterlocked := FALSE;
	END_IF
	
	(********************************************)
	(*		 		    FEEDBACK 				*)
	(********************************************)
	
	DATA.STS.IsClosed := TRUE;
	DATA.STS.IsOpened := TRUE;
	
	FOR Internal.i :=0 TO DATA.PAR.NumberOfPosSensors - 1 DO 	

		Internal.Feedback[Internal.i].DI_Close 		:= DI_Close[Internal.i];
		Internal.Feedback[Internal.i].DI_Open 		:= DI_Open[Internal.i];
		Internal.Feedback[Internal.i].DO_Open 		:= DO_Open;
	
		Internal.Feedback[Internal.i].HasCloseFB 	:= DATA.PAR.HasCloseFB;
		Internal.Feedback[Internal.i].HasOpenFB 	:= DATA.PAR.HasOpenFB;
	
		Internal.Feedback[Internal.i].TimerStsClose_Preset 	:= DATA.PAR.TimerStsClose_Preset;
		Internal.Feedback[Internal.i].TimerStsOpen_Preset 	:= DATA.PAR.TimerStsOpen_Preset;
	
		Internal.Feedback[Internal.i]();
		//if at least one vale is not closed set IsClosed status to false
		IF NOT Internal.Feedback[Internal.i].IsClosed THEN
			DATA.STS.IsClosed := FALSE;
		END_IF
		//if at least one valve is not opened set IsOpened status to false
		IF NOT Internal.Feedback[Internal.i].IsOpened THEN
			DATA.STS.IsOpened := FALSE;
		END_IF
		
	END_FOR
	
	
	
	(********************************************)
	(*		 		     ALARM 					*)
	(********************************************)
	//Reset Alarm
	IF DATA.CS.Reset THEN
		DATA.CS.Reset := FALSE;
		memset(ADR(DATA.STS.ALM), 0, SIZEOF(DATA.STS.ALM));
	END_IF
		
	FOR Internal.i:=0 TO DATA.PAR.NumberOfPosSensors - 1 DO
		Internal.TimerAlm[Internal.i].IN := FALSE;
		//Alarm Open
		IF DO_Open AND NOT Internal.Feedback[Internal.i].IsOpened THEN
			Internal.TimerAlm[Internal.i].IN := TRUE;
			IF Internal.TimerAlm[Internal.i].Q THEN
				DATA.STS.ALM.Open[Internal.i] := TRUE;
			END_IF
		END_IF
		//Alarm Close
		IF NOT DO_Open AND NOT Internal.Feedback[Internal.i].IsClosed THEN
			Internal.TimerAlm[Internal.i].IN := TRUE;
			IF Internal.TimerAlm[Internal.i].Q THEN
				DATA.STS.ALM.Close[Internal.i] := TRUE;
			END_IF
		END_IF
		//Alarm Open and Close
		IF Internal.Feedback[Internal.i].IsOpened AND Internal.Feedback[Internal.i].IsClosed THEN
			Internal.TimerAlm[Internal.i].IN := TRUE;
			IF Internal.TimerAlm[Internal.i].Q THEN
				IF DO_Open THEN
					DATA.STS.ALM.Close[Internal.i] := TRUE;
				ELSE
					DATA.STS.ALM.Open[Internal.i]  := TRUE;
				END_IF
			END_IF
		END_IF
		//In Alarm
		IF DATA.STS.ALM.Close[Internal.i] OR DATA.STS.ALM.Open[Internal.i] THEN
			DATA.STS.ALM.InAlm 	:= TRUE;
		END_IF
		
		Internal.TimerAlm[Internal.i](PT := DATA.PAR.TimerAlm_Preset);
	END_FOR
	
	(********************************************)
	(*		 	  MAINTENANCE COUNTER 			*)
	(********************************************)
	//Count up
	IF Internal.DO_Open > DO_Open THEN
		MaintCounter := MaintCounter + 1;
	END_IF
	//Reset counter
	IF DATA.CS.ResetMaintCounter THEN
		MaintCounter := 0;
	END_IF
		
	
	(********************************************)
	(*		 		VC4 VISUALIZATION			*)
	(********************************************)
	DATA.STS.VIS.InAlm 		:= NOT DATA.STS.ALM.InAlm;
	DATA.STS.VIS.Interlock 	:= NOT DATA.STS.IsInterlocked;
	
	//Visualization feedback status
	//Open feedback
	IF DATA.STS.IsOpened THEN
		DATA.STS.VIS.OpenFBStatus := 1;
	ELSE
		DATA.STS.VIS.OpenFBStatus := 0;
	END_IF
	IF DATA.STS.ALM.Open[0] OR DATA.STS.ALM.Open[1] OR DATA.STS.ALM.Open[2] OR DATA.STS.ALM.Open[3] OR DATA.STS.ALM.Open[4] OR DATA.STS.ALM.Open[5]THEN
		IF DATA.STS.IsOpened THEN
			DATA.STS.VIS.OpenFBStatus := 3;
		ELSE
			DATA.STS.VIS.OpenFBStatus := 2;
		END_IF
	END_IF
	//Close feedback
	IF DATA.STS.IsClosed THEN
		DATA.STS.VIS.CloseFBStatus := 1;
	ELSE
		DATA.STS.VIS.CloseFBStatus := 0;
	END_IF
	IF DATA.STS.ALM.Close[0] OR DATA.STS.ALM.Close[1] OR DATA.STS.ALM.Close[2] OR DATA.STS.ALM.Close[3] OR DATA.STS.ALM.Close[4] OR DATA.STS.ALM.Close[5] THEN
		IF DATA.STS.IsClosed THEN
			DATA.STS.VIS.CloseFBStatus := 3;
		ELSE
			DATA.STS.VIS.CloseFBStatus := 2;
		END_IF
	END_IF
	
	(********************************************)
	(*		 		   INTERNAL		 			*)
	(********************************************)
	Internal.DO_Open		:= DO_Open;
	Internal.Control 		:= DATA.CS.Control;
	Internal.SingleCycle 	:= DATA.CS.SingleCycle;	

END_FUNCTION_BLOCK
