
{REDUND_ERROR} FUNCTION_BLOCK CM_V2C2P (*Control module: Valve - 2 Controls, 2 Positions*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		DI_Open : BOOL; (*Digital Input - Open*)
		DI_Close : BOOL; (*Digital Input - Close*)
	END_VAR
	VAR_OUTPUT
		DO_Open : BOOL; (*Digital Output - Open*)
		DO_Close : BOOL; (*Digital Output - Close*)
		MaintCounter : UDINT; (*Maintenance counter*)
	END_VAR
	VAR_IN_OUT
		DATA : UDT_V2C2P; (*Data structure*)
	END_VAR
	VAR
		Internal : UDT_V2C2P_INTERNAL; (*Internal structure*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK CM_V2C2P_stop (*Control module: Valve - 2 Controls, 2 Positions*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		DI_Open : BOOL; (*Digital Input - Open*)
		DI_Close : BOOL; (*Digital Input - Close*)
	END_VAR
	VAR_OUTPUT
		DO_Open : BOOL; (*Digital Output - Open*)
		DO_Close : BOOL; (*Digital Output - Close*)
		MaintCounter : UDINT; (*Maintenance counter*)
	END_VAR
	VAR_IN_OUT
		DATA : UDT_V2C2P_stop; (*Data structure*)
	END_VAR
	VAR
		Internal : UDT_V2C2P_stop_INTERNAL; (*Internal structure*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK CM_V1C2P (*Control module: Valve - 1 Control, 2 Positions*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		DI_Open : BOOL; (*Digital Input - Open*)
		DI_Close : BOOL; (*Digital Input - Close*)
	END_VAR
	VAR_OUTPUT
		DO_Open : BOOL; (*Digital Output - Control*)
		MaintCounter : UDINT; (*Maintenance counter*)
	END_VAR
	VAR_IN_OUT
		DATA : UDT_V1C2P; (*Data structure*)
	END_VAR
	VAR
		Internal : UDT_V1C2P_INTERNAL; (*Internal structure*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK CM_V1C2PnS (*Control module: Valve - 1 Control, 2 Positions, n Position sensors*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		DI_Open : ARRAY[0..MAX_POS_SENSORS] OF BOOL; (*Digital Inputs - Open array*)
		DI_Close : ARRAY[0..MAX_POS_SENSORS] OF BOOL; (*Digital Inputs - Close array*)
	END_VAR
	VAR_OUTPUT
		DO_Open : BOOL; (*Digital Output - Control*)
		MaintCounter : UDINT; (*Maintenance counter*)
	END_VAR
	VAR_IN_OUT
		DATA : UDT_V1C2PnS; (*Data structure*)
	END_VAR
	VAR
		Internal : UDT_V1C2PnS_INTERNAL; (*Internal structure*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK V_FEEDBACK (*Set valve feedback*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		DI_Open : {REDUND_UNREPLICABLE} BOOL; (*Digital Inputs - Open*)
		DI_Close : BOOL; (*Digital Inputs - Close*)
		DO_Open : BOOL; (*Digital Output - Control*)
		HasOpenFB : BOOL; (*Parameter Has open feedback*)
		HasCloseFB : BOOL; (*Parameter Has close feedback*)
		TimerStsOpen_Preset : TIME; (*Parameter preset time for status IsOpened*)
		TimerStsClose_Preset : TIME; (*Parameter preset time for status IsCloseed*)
	END_VAR
	VAR_OUTPUT
		IsOpened : BOOL; (*IsOpened status*)
		IsClosed : BOOL; (*IsClosed status*)
	END_VAR
	VAR
		TimerStsOpen : TON; (*Timer for status IsOpened*)
		TimerStsClose : TON; (*Timer fot status IsClosed*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK CM_MDC1D1P (*Control module: Motor Direct Controlled 1 Direction 1 Position*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		DI_Sens : {REDUND_UNREPLICABLE} BOOL; (*Digital Input - Position sensor*)
		DI_Alarm : BOOL; (*Digital Input - Motor is in alarm*)
	END_VAR
	VAR_OUTPUT
		DO_Control : BOOL; (*Digital Output - Start the motor*)
		MaintCounter : REAL; (*Maintenance counter*)
	END_VAR
	VAR_IN_OUT
		DATA : UDT_MDC1D1P; (*Data structure*)
	END_VAR
	VAR
		Internal : UDT_MDC1D1P_INTERNAL; (*Internal structure*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK CM_MDC2D2P (*Control module: Motor Direct Controlled 2 Directiosn 2 Positions*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		DI_Sens_Pos : {REDUND_UNREPLICABLE} BOOL; (*Digital Input - Position sensor positive direction*)
		DI_Sens_Neg : {REDUND_UNREPLICABLE} BOOL; (*Digital Input - Position sensor positive direction*)
		DI_Alarm : BOOL; (*Digital Input - Motor is in alarm*)
	END_VAR
	VAR_OUTPUT
		DO_Control_Pos : BOOL; (*Digital Output - Start the motor positive direction*)
		DO_Control_Neg : BOOL; (*Digital Output - Start the motor negative direction*)
		MaintCounter : REAL; (*Maintenance counter*)
	END_VAR
	VAR_IN_OUT
		DATA : UDT_MDC2D2P; (*Data structure*)
	END_VAR
	VAR
		Internal : UDT_MDC2D2P_INTERNAL; (*Internal structure*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK CM_DI (*Control module: Digital Input*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		DI_Input : {REDUND_UNREPLICABLE} BOOL; (*Digital Input - Input is active*)
	END_VAR
	VAR_IN_OUT
		DATA : UDT_DI; (*Data structure*)
	END_VAR
	VAR
		TimerAlm : TON; (*Alarm timer*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK CM_ES (*Control module: Digital Input*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		DI_MIN : {REDUND_UNREPLICABLE} BOOL; (*Digital Input - Input MIN*)
		DI_MAX : {REDUND_UNREPLICABLE} BOOL; (*Digital Input - Input MAX*)
	END_VAR
	VAR_IN_OUT
		DATA : UDT_ES; (*Data structure*)
	END_VAR
	VAR
		TimerAlmMIN : TON; (*Alarm timer MIN*)
		TimerAlmMAX : TON; (*Alarm timer MAX*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK V_FEEDBACK_stop (*Set valve feedback*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		DI_Open : {REDUND_UNREPLICABLE} BOOL; (*Digital Inputs - Open*)
		DI_Close : BOOL; (*Digital Inputs - Close*)
		DO_Close : BOOL; (*Digital Output - Control*)
		DO_Open : BOOL; (*Digital Output - Control*)
		HasOpenFB : BOOL; (*Parameter Has open feedback*)
		HasCloseFB : BOOL; (*Parameter Has close feedback*)
		TimerStsOpen_Preset : TIME; (*Parameter preset time for status IsOpened*)
		TimerStsClose_Preset : TIME; (*Parameter preset time for status IsCloseed*)
	END_VAR
	VAR_OUTPUT
		IsOpened : BOOL; (*IsOpened status*)
		IsClosed : BOOL; (*IsClosed status*)
	END_VAR
	VAR
		TimerStsOpen : TON; (*Timer for status IsOpened*)
		TimerStsClose : TON; (*Timer fot status IsClosed*)
	END_VAR
END_FUNCTION_BLOCK
