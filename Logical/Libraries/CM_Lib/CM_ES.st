
(* Control module: Digital Input End Switch *)
FUNCTION_BLOCK CM_ES
	(********************************************)
	(*		 		     ALARM 					*)
	(********************************************)
	//Reset Alarm
	IF DATA.CS.Reset THEN
		DATA.CS.Reset := FALSE;
		DATA.STS.ALM.InAlm 	:= FALSE;
		DATA.STS.ALM.AlmMIN := FALSE;
		DATA.STS.ALM.AlmMAX := FALSE;
	END_IF
	//Alarm MIN
	TimerAlmMIN.IN := NOT DATA.CS.IgnoreAlm AND DI_MIN;

	IF TimerAlmMIN.Q THEN
		DATA.STS.ALM.AlmMIN := TRUE;
	END_IF
	TimerAlmMIN(PT := DATA.PAR.TimerAlm_Preset);
	//Alarm MAX
	TimerAlmMAX.IN := NOT DATA.CS.IgnoreAlm AND DI_MAX;

	IF TimerAlmMAX.Q THEN
		DATA.STS.ALM.AlmMAX := TRUE;
	END_IF
	TimerAlmMAX(PT := DATA.PAR.TimerAlm_Preset);
	//Alarm MIN and MAX
	IF DI_MIN AND DI_MAX THEN
		DATA.STS.ALM.AlmMIN := TRUE;
		DATA.STS.ALM.AlmMAX := TRUE;
	END_IF
	//InAlm
	DATA.STS.ALM.InAlm := DATA.STS.ALM.AlmMIN OR DATA.STS.ALM.AlmMAX;

END_FUNCTION_BLOCK
