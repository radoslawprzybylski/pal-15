
(* Control module: Valve - 2 Controls, Any Positions, External control variable*)
FUNCTION_BLOCK CM_V2C2P_stop
	(********************************************)
	(*		 		CHECK PARAMETERS			*)
	(********************************************)
	IF DATA.PAR.TimerStsClose_Preset > DATA.PAR.TimerAlm_Preset THEN
		DATA.PAR.TimerStsClose_Preset := DATA.PAR.TimerAlm_Preset;
	END_IF
	IF DATA.PAR.TimerStsOpen_Preset > DATA.PAR.TimerAlm_Preset THEN
		DATA.PAR.TimerStsOpen_Preset := DATA.PAR.TimerAlm_Preset;
	END_IF
	(********************************************)
	(*		 			CONTROL					*)
	(********************************************)
	//Output open
	IF DATA.CS.Down AND NOT DATA.CS.Up THEN 
		DO_Close := TRUE;
		DO_Open := FALSE;
	ELSIF DATA.CS.Up AND NOT DATA.CS.Down THEN
		DO_Close := FALSE;
		DO_Open := TRUE;	
	ELSE
		DO_Close := FALSE;
		DO_Open := FALSE;
	END_IF
		
	(********************************************)
	(*		 		  INTERLOCK					*)
	(********************************************)
	IF DATA.CS.Interlock THEN
		DO_Open := FALSE;
		DO_Close := TRUE;
		//Set status IsInterlocked
		DATA.STS.IsInterlocked := TRUE;
	ELSE
		//Reset status IsInterlocked
		DATA.STS.IsInterlocked := FALSE;
	END_IF
	
	(********************************************)
	(*		 		    FEEDBACK 				*)
	(********************************************)
	Internal.Feedback.DI_Close 		:= DI_Close;
	Internal.Feedback.DI_Open 		:= DI_Open;
	Internal.Feedback.DO_Open 		:= DO_Open;
	Internal.Feedback.DO_Close 		:= DO_Close;
	
	Internal.Feedback.HasCloseFB 	:= DATA.PAR.HasCloseFB;
	Internal.Feedback.HasOpenFB 	:= DATA.PAR.HasOpenFB;
	
	Internal.Feedback.TimerStsClose_Preset 	:= DATA.PAR.TimerStsClose_Preset;
	Internal.Feedback.TimerStsOpen_Preset 	:= DATA.PAR.TimerStsOpen_Preset;
	
	Internal.Feedback();
	
	DATA.STS.IsClosed := Internal.Feedback.IsClosed;
	DATA.STS.IsOpened := Internal.Feedback.IsOpened;
	
	(********************************************)
	(*		 		     ALARM 					*)
	(********************************************)
	//Reset Alarm
	IF DATA.CS.Reset THEN
		DATA.CS.Reset := FALSE;
		memset(ADR(DATA.STS.ALM), 0, SIZEOF(DATA.STS.ALM));
	END_IF
	
	Internal.TimerAlm.IN := FALSE;
	//Alarm Open
	IF DO_Open AND NOT DATA.STS.IsOpened THEN
		Internal.TimerAlm.IN := TRUE;
		IF Internal.TimerAlm.Q THEN
			DATA.STS.ALM.Open 	:= TRUE;
		END_IF
	END_IF
	//Alarm Close
	IF DO_Close AND NOT DATA.STS.IsClosed THEN
		Internal.TimerAlm.IN := TRUE;
		IF Internal.TimerAlm.Q THEN
			DATA.STS.ALM.Close := TRUE;
		END_IF
	END_IF
	//Alarm Open and Close
	IF DATA.STS.IsClosed AND DATA.STS.IsOpened THEN
		Internal.TimerAlm.IN := TRUE;
		IF Internal.TimerAlm.Q THEN
			IF DO_Open THEN
				DATA.STS.ALM.Close := TRUE;
			ELSE
				DATA.STS.ALM.Open  := TRUE;
			END_IF
		END_IF
	END_IF
	//In Alarm
	IF DATA.STS.ALM.Close OR DATA.STS.ALM.Open THEN
		DATA.STS.ALM.InAlm 	:= TRUE;
	END_IF
	//Changing control output resets the timer
	IF DO_Open <> Internal.DO_Open OR DO_Close <> Internal.DO_Close THEN
		Internal.TimerAlm.IN := FALSE;
	END_IF
	//Execute TimerAlm FB
	Internal.TimerAlm(PT := DATA.PAR.TimerAlm_Preset);
	
	(********************************************)
	(*		 	  MAINTENANCE COUNTER 			*)
	(********************************************)
	//Count up
	IF Internal.DO_Open > DO_Open THEN
		MaintCounter := MaintCounter + 1;
	END_IF
	//Reset counter
	IF DATA.CS.ResetMaintCounter THEN
		MaintCounter := 0;
	END_IF
		
	(********************************************)
	(*		 		   INTERNAL		 			*)
	(********************************************)
	Internal.DO_Open		:= DO_Open;
	Internal.DO_Close		:= DO_Close;
END_FUNCTION_BLOCK
