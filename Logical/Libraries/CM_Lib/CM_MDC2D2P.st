//TO DO
//ALARMY!!!!!!!!!!!!!!!!!!!!!!!


(* Control module: Motor Direct Controlled 2 Directions 2 Positions *)
FUNCTION_BLOCK CM_MDC2D2P
	
	(********************************************)
	(*		 			CONTROL					*)
	(********************************************)
	(************Move************)
	//Switch on control if the SingleCycle command is on
	IF DATA.CS.Move > Internal.Move THEN
		DATA.STS.IsRun := TRUE;
	END_IF

	//Set status MoveDone if motor is in position
	IF DATA.CS.Move THEN
		IF  (DATA.PAR.Direction = DIR_POS AND DI_Sens_Pos) OR
			(DATA.PAR.Direction = DIR_NEG AND DI_Sens_Neg)
		THEN
			DATA.STS.MoveDone := TRUE;
		END_IF
	END_IF
	
	//Switch off control if SingleCycle command is off or single cycle is done and Control command is not on
	IF (DATA.CS.Move < Internal.Move OR DATA.STS.MoveDone) AND NOT DATA.CS.SingleCycle AND NOT DATA.CS.Control THEN
		DATA.STS.IsRun := FALSE;
	END_IF
	//Reset status MoveDone
	IF NOT DATA.CS.Move THEN
		DATA.STS.MoveDone := FALSE;
	END_IF
	
	(******Single cycle state machine******)
	//SingleCycle command reseted
	IF DATA.CS.SingleCycle < Internal.SingleCycle THEN
		Internal.SinglCycleStep := 0;
	END_IF
	CASE Internal.SinglCycleStep OF
		//WAIT Step - Wait for single cycle command
		0:
			DATA.STS.SingleCycleDone := FALSE;
			IF DATA.CS.SingleCycle THEN
				DATA.PAR.Direction := DIR_POS;
				DATA.STS.IsRun := TRUE;
				Internal.SinglCycleStep := 1;
			END_IF
		//GO_POS Step - Go positive and wait for DI_Sens_Pos
		1:
			//Wait for DI_Sens_Pos
			IF DI_Sens_Pos THEN
				DATA.PAR.Direction := DIR_NEG;
				DATA.STS.IsRun := TRUE;
				Internal.SinglCycleStep := 2;
			END_IF
		//GO_NEG Step - Go negative and wait for DI_Sens_Neg
		2:
			//Wait for DI_Sens_Neg
			IF DI_Sens_Neg THEN
				DATA.STS.IsRun := FALSE;
				Internal.SinglCycleStep := 3;
			END_IF
		3:
			//Done Step - Single cycle done
			IF DATA.CS.NextCycle THEN
				DATA.CS.NextCycle := FALSE;
				DATA.PAR.Direction := DIR_POS;
				DATA.STS.IsRun := TRUE;
				Internal.SinglCycleStep := 1;
			ELSE
				DATA.STS.SingleCycleDone := TRUE;
			END_IF	
	END_CASE
	
	
	(************Control************)
	//Switch on control if Control command is on
	IF DATA.CS.Control > Internal.Control THEN
		DATA.STS.IsRun := TRUE;
	END_IF
	IF DATA.CS.Control THEN
		//Switch direction if motor reaches position sensor
		IF DI_Sens_Pos THEN
			DATA.PAR.Direction := DIR_NEG;
		ELSIF DI_Sens_Neg THEN
			DATA.PAR.Direction := DIR_POS;
		END_IF
	END_IF
	//Switch off control if Control command is off and single cycle is not on
	IF DATA.CS.Control < Internal.Control AND NOT DATA.CS.SingleCycle THEN
		DATA.STS.IsRun := FALSE;
	END_IF
	
	(********************************************)
	(*		 		  INTERLOCK					*)
	(********************************************)
	IF DATA.CS.Interlock OR DATA.STS.ALM.InAlm THEN
		DATA.STS.IsRun := FALSE;
	END_IF
	DATA.STS.IsInterlocked := DATA.CS.Interlock;
	
	(********************************************)
	(*	 	     DIGITAL OUTPUT CONTROL			*)
	(********************************************)
	IF DATA.PAR.Direction = DIR_POS THEN
		DO_Control_Pos := DATA.STS.IsRun;
		DO_Control_Neg := FALSE;
	ELSIF DATA.PAR.Direction = DIR_NEG THEN
		DO_Control_Pos := FALSE;
		DO_Control_Neg := DATA.STS.IsRun;
	END_IF
	
	(********************************************)
	(*		 		     ALARM 					*)
	(********************************************)
	//Reset Alarm
	IF DATA.CS.Reset THEN
		DATA.CS.Reset := FALSE;
		
		DATA.STS.ALM.InAlm 				:= FALSE;
		DATA.STS.ALM.AlarmFeedback 		:= FALSE;
		DATA.STS.ALM.AlmSensPosActive 	:= FALSE;
		DATA.STS.ALM.AlmSensPosInactive := FALSE;
		DATA.STS.ALM.AlmSensNegActive 	:= FALSE;
		DATA.STS.ALM.AlmSensNegInactive := FALSE;
	END_IF
	
	//Check the position sensor if motor is in run and has position feedback
	IF DATA.STS.IsRun AND DATA.PAR.HasPosFeedback THEN
		//Start timer if direction is negative and positive sensor is active or direction is positive and positive sensor is inactive
		IF (DATA.PAR.Direction = DIR_NEG AND DI_Sens_Pos) OR (DATA.PAR.Direction = DIR_POS AND NOT DI_Sens_Pos) THEN
			Internal.TimerAlm[0].IN := TRUE;
		ELSE
			Internal.TimerAlm[0].IN := FALSE;
		END_IF;
		IF DI_Sens_Pos <> Internal.DI_Sens_Pos THEN
			Internal.TimerAlm[0].IN := FALSE;
		END_IF
		//If the timer is active set the alarm (position sensor did not change in time)
		IF Internal.TimerAlm[0].Q THEN
			IF DI_Sens_Pos THEN
				//position sensor is constantly active
				DATA.STS.ALM.AlmSensPosActive := TRUE;
			ELSE
				//position sensor is constantly inactive
				DATA.STS.ALM.AlmSensPosInactive := TRUE;
			END_IF
		END_IF
		
		//Start timer if direction is positive and negative sensor is active or direction is negative and negative sensor is inactive
		IF (DATA.PAR.Direction = DIR_POS AND DI_Sens_Neg) OR (DATA.PAR.Direction = DIR_NEG AND NOT DI_Sens_Neg) THEN
			Internal.TimerAlm[1].IN := TRUE;
		ELSE
			Internal.TimerAlm[1].IN := FALSE;
		END_IF;
		IF DI_Sens_Neg <> Internal.DI_Sens_Neg THEN
			Internal.TimerAlm[1].IN := FALSE;
		END_IF
		//If the timer is active set the alarm (position sensor did not change in time)
		IF Internal.TimerAlm[1].Q THEN
			IF DI_Sens_Neg THEN
				//position sensor is constantly active
				DATA.STS.ALM.AlmSensNegActive := TRUE;
			ELSE
				//position sensor is constantly inactive
				DATA.STS.ALM.AlmSensNegInactive := TRUE;
			END_IF
		END_IF
	ELSE
		Internal.TimerAlm[0].IN := FALSE;
		Internal.TimerAlm[1].IN := FALSE;
	END_IF
	
	//Alarm feedback - Digital input feedback alarm is active
	IF DI_Alarm THEN
		DATA.STS.ALM.AlarmFeedback := TRUE;	
	END_IF
	//In Alarm
	IF DATA.STS.ALM.AlarmFeedback OR 
		DATA.STS.ALM.AlmSensPosActive OR DATA.STS.ALM.AlmSensPosInactive OR
		DATA.STS.ALM.AlmSensNegActive OR DATA.STS.ALM.AlmSensNegInactive 
	THEN
		DATA.STS.ALM.InAlm 	:= TRUE;
	END_IF
	
	Internal.TimerAlm[0](PT := DATA.PAR.TimerAlm_Preset);
	Internal.TimerAlm[1](PT := DATA.PAR.TimerAlm_Preset);
	
	(********************************************)
	(*		 	MAINTENANCE COUNTER 			*)
	(********************************************)
	//Calculate the cycle time
	IF Internal.CycleTime = 0 THEN
		Internal.RTInfo_0(enable := 1);
		Internal.CycleTime := UDINT_TO_REAL(Internal.RTInfo_0.cycle_time) /(1000*1000); //Cycle time in seconds
	END_IF
	//Calculate maintenance counter
	IF DATA.STS.IsRun THEN
		MaintCounter := MaintCounter + Internal.CycleTime;
	END_IF
	
	//Reset counter
	IF DATA.CS.ResetMaintCounter THEN
		MaintCounter := 0;
	END_IF
	
	(********************************************)
	(*		 		VC4 VISUALIZATION			*)
	(********************************************)
	DATA.STS.VIS.InAlm 		:= NOT DATA.STS.ALM.InAlm;
	DATA.STS.VIS.Interlock 	:= NOT DATA.STS.IsInterlocked;
	
	//Visualization feedback status
	//Position sensor status (direction positive)
	IF DI_Sens_Pos THEN
		DATA.STS.VIS.PosSensorStatus := 1;
	ELSE
		DATA.STS.VIS.PosSensorStatus := 0;
	END_IF
	IF DATA.STS.ALM.AlmSensPosActive THEN
		DATA.STS.VIS.PosSensorStatus := 2;
	ELSIF DATA.STS.ALM.AlmSensPosInactive THEN
		DATA.STS.VIS.PosSensorStatus := 3;	
	END_IF
	//Visualization feedback status
	//Position sensor status (direction negative)
	IF DI_Sens_Neg THEN
		DATA.STS.VIS.NegSensorStatus := 1;
	ELSE
		DATA.STS.VIS.NegSensorStatus := 0;
	END_IF
	IF DATA.STS.ALM.AlmSensNegActive THEN
		DATA.STS.VIS.NegSensorStatus := 2;
	ELSIF DATA.STS.ALM.AlmSensNegInactive THEN
		DATA.STS.VIS.NegSensorStatus := 2;	
	END_IF
	
	(********************************************)
	(* 					INTERNAL 				*)
	(********************************************)
	Internal.DI_Sens_Pos 	:= DI_Sens_Pos;
	Internal.DI_Sens_Neg 	:= DI_Sens_Neg;
	Internal.SingleCycle 	:= DATA.CS.SingleCycle;
	Internal.Control 		:= DATA.CS.Control;
	
END_FUNCTION_BLOCK
