(*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************)
(*CM*)

TYPE
	CM_Main_typ : 	STRUCT 
		Master : CM_Master_typ;
		Transport : CM_Transport_typ; (*CM dla Transport*)
		Balkon : CM_Balkon_typ; (*CM dla balkon*)
		Magazyn_Palet : CM_Magazyn_typ; (*CM dla Magazyn_Palet*)
		Owijarka : CM_Owijarka_typ; (*CM dla Owijarka*)
	END_STRUCT;
	CM_Master_typ : 	STRUCT 
		do_ResetSafety : BOOL; (*Lampka fizyczna na szafie*)
		do_StartAutomat : BOOL; (*Lampka fizyczna na szafie*)
		do_StopAutomat : BOOL; (*Lampka fizyczna na szafie*)
		do_Pauza : BOOL; (*Lampka fizyczna na szafie*)
		di_ResetSafety : BOOL; (*Konsola fizyczna na szafie*)
		di_StartAutomat : BOOL; (*Konsola fizyczna na szafie*)
		di_StopAutomat : BOOL; (*Konsola fizyczna na szafie*)
		di_Pauza : BOOL; (*Konsola fizyczna na szafie*)
		dis_Grzyb : BOOL; (*Sygnal z grzyba*)
		di_Paleta : BOOL; (*Konsola fizyczna na szafie -> do czego? (????????????????)*)
		di_CtrlFaz : BOOL; (*Czujnik kontroli fazy *)
		di_ZabezpieczenieSilnikow : BOOL; (*Zabezpieczenie silnikow F5,F6*)
		var_SafetyOK : BOOL;
		di_CzujnikPowietrza : BOOL;
	END_STRUCT;
	CM_Transport_typ : 	STRUCT 
		di_Potwierdzenie_Wtyczki : BOOL;
		di_S1_Zderzak_Spych1 : BOOL;
		di_S2_Spych1_Safety : BOOL; (*!*)
		Z1_Silownik_Spych1 : CM_V1C2P;
		di_S5_Zderzak_Spych2 : BOOL;
		di_S6_Spych2_Safety : BOOL; (*!*)
		di_S14_ZaDuzo_Zgrzewek : BOOL;
		Z2_Silownik_Spych2 : CM_V1C2P;
		Z3_BlokadaZgrzewki : CM_V1C2P;
		di_S11_ObecnoscZgrzewki : BOOL;
		Z10_Obrotnica : CM_V1C2P;
		di_S14_ZaDuzoZgrzewek : USINT;
		di_S14_PrzeladowanieZgrzewkami : BOOL;
		di_S15_Polozenie_SpychaczLan : BOOL; (*!*)
		K3_SpychNaPlatforme : BOOL; (*Stycznik. Spychacz gotowego rzedu zgrzewek na platform*)
	END_STRUCT;
	CM_Balkon_typ : 	STRUCT 
		di_S16_PozycjaBalkonu_1 : BOOL;
		di_S17_PozycjaBalkonu_2 : BOOL;
		di_S18_PozycjaBalkonu_3 : BOOL;
		di_S19_PozycjaBalkonu_4 : BOOL;
		Z4_Silownik_Warstwa : CM_V1C2P;
		Z5_Silownik_Zderzak : CM_V1C2P;
		di_S24_Balkon_nisko : BOOL;
		di_S25_Polozenie_Slup_1 : BOOL;
		di_S26_Polozenie_Slup_2 : BOOL;
		en_ENK1 : DINT;
		P2_Hamulec : BOOL;
	END_STRUCT;
	CM_Magazyn_typ : 	STRUCT 
		di_S27_MagPal_1 : BOOL;
		di_S28_MagPal_2 : BOOL;
		di_S29_MagPal_3 : BOOL;
		di_S35_ObecnoscPalety_rolkowy : BOOL;
		di_S36_BrakPalety_rolkowy : BOOL;
		Z6_Z7_Silownik_MagPal : CM_V2C2P_stop;
		Z7_Silownik_MagPal_2 : CM_V1C2P;
		Z8_Silownik_BlokPal : CM_V1C2P;
		di_S32_ObecnoscPalety : BOOL;
		Z9_Silownik_Lancuchowy1 : CM_V1C2P;
		K4_RuchLancucha : BOOL;
	END_STRUCT;
	CM_Owijarka_typ : 	STRUCT 
		di_S37_ObecnoscPalety_owijarka : BOOL;
		di_S38_BrakPalety_owijarka : BOOL;
		di_S39_StanOwijarki_ok : BOOL;
		di_S40_Przycisk_Paleta : BOOL;
		do_BlokadaOwijarki_przekaznik : BOOL; (*!!!!!!!!!!!!!!!!!!*)
	END_STRUCT;
END_TYPE

(****************************************************************************************************************************************************************************************************************************************************************************)
(*CM DATA*)

TYPE
	DATA_Main_typ : 	STRUCT 
		Transport : DATA_Transport_typ;
		Balkon : DATA_Balkon_typ;
		Magazyn : DATA_Magazyn_typ;
		Owijarka : DATA_Owijarka_typ;
	END_STRUCT;
	DATA_Transport_typ : 	STRUCT 
		Z1_Silownik_Spych1 : UDT_V1C2P;
		Z2_Silownik_Spych2 : UDT_V1C2P;
		Z3_BlokadaZgrzewki : UDT_V1C2P;
		Z10_Obrotnica : UDT_V1C2P;
		MT1_10 : inverter_ctrl_typ;
		MT2_11 : inverter_ctrl_typ;
		MT3_01 : inverter_ctrl_typ;
		MT4_02 : inverter_ctrl_typ;
		MT5_02RolkaKrotka : inverter_ctrl_typ;
		MT6_03RolkaDluga : inverter_ctrl_typ;
	END_STRUCT;
	DATA_Balkon_typ : 	STRUCT 
		Z4_Silownik_Warstwa : UDT_V1C2P;
		Z5_Silownik_Zderzak : UDT_V1C2P;
		MT7_04Balkon : inverter_ctrl_typ;
		MT8_05Slupy : inverter_ctrl_typ;
	END_STRUCT;
	DATA_Magazyn_typ : 	STRUCT 
		Z6_Z7_Silownik_MagPal : UDT_V2C2P_stop;
		Z7_Silownik_MagPal_2 : UDT_V1C2P;
		Z8_Silownik_BlokPal : UDT_V1C2P;
		Z9_Silownik_Lancuchowy1 : UDT_V1C2P;
	END_STRUCT;
	DATA_Owijarka_typ : 	STRUCT 
		MT10_09RolkowyOwijarka : inverter_ctrl_typ;
		MT9_08RolkowyDuzy : inverter_ctrl_typ;
	END_STRUCT;
END_TYPE

(****************************************************************************************************************************************************************************************************************************************************************************)
(*Obsuga falownik�w*)

TYPE
	inverter_ctrl_typ : 	STRUCT  (*struktura main dla invertera*)
		Command : inverter_cmd_typ; (*komendy*)
		Set : inverter_set_typ; (*ustawienia rampy*)
		Actual : inverter_act_typ; (*aktualne ustawienia rampy*)
		Status : inverter_stat_typ; (*status*)
		Config : inverter_cfg_typ; (*konfiguracja interfejsu komunikacyjnego*)
	END_STRUCT;
	inverter_cmd_typ : 	STRUCT  (*struktura komend*)
		forward : BOOL; (*ruch do przodu*)
		reverse : BOOL; (*ruch do ty�u*)
		stop : BOOL; (*zatrzymanie z ramp�*)
		quickStop : BOOL; (*szybkie zatrzymanie*)
		disableVoltage : BOOL; (*od��czenie zasilania z silnika*)
		resetFault : BOOL; (*kasowanie b��du*)
	END_STRUCT;
	inverter_act_typ : 	STRUCT  (*wartosci aktualne invertera*)
		outRPM : INT; (*aktualna pr�dko�� silnika [obr./min]*)
		outACC : UINT; (*czas rampy przyspieszania [0.1s]*)
		outDEC : UINT; (*czas rampy hamowania [0.1s]*)
	END_STRUCT;
	inverter_set_typ : 	STRUCT  (*wartosci zadane invertera*)
		setRPM : INT; (*zadana pr�dko�� silnika [obr./min]*)
		setACC : UINT; (*zadany czas rampy przyspieszania [0.1s]*)
		setDEC : UINT; (*zadany czas rampy hamowania [0.1s]*)
	END_STRUCT;
	inverter_stat_typ : 	STRUCT  (*struktura statusowa*)
		ready : BOOL; (*TRUE = falownik gotowy do pracy*)
		strState : STRING[50]; (*stan falownika (CiA402)*)
		errorCode : INT; (*kod b��du*)
		strError : STRING[80]; (*opis b��du*)
		acyclicStatus : USINT; (*status zapisu/odczytu parametr�w ACC/DEC, 0 = OK*)
	END_STRUCT;
	inverter_cfg_typ : 	STRUCT  (*struktura konfiguracyjna*)
		deviceName : STRING[80]; (*�cie�ka dost�pu do inerfejsu falownika*)
		stationNo : USINT; (*numer stacji*)
	END_STRUCT;
END_TYPE

(****************************************************************************************************************************************************************************************************************************************************************************)
(*Statystyki*)

TYPE
	StatystykiUlozonych_typ : 	STRUCT 
		Zgrzewek : INT;
		Warstw : INT;
		Palet : INT;
	END_STRUCT;
	Statystyki_typ : 	STRUCT 
		Zycie : StatystykiUlozonych_typ;
		Partia : StatystykiUlozonych_typ;
	END_STRUCT;
END_TYPE
